<?php

$num_columns	= 10;
$can_delete	= $this->auth->has_permission('Give.Settings.Delete');
$can_edit		= $this->auth->has_permission('Give.Settings.Edit');
$has_records	= isset($records) && is_array($records) && count($records);

if ($can_delete) {
    $num_columns++;
}
?>
<div class='admin-box'>
	<?php echo form_open($this->uri->uri_string()); ?>
		<table class='table table-striped'>
			<thead>
				<tr>
					<?php if ($can_delete && $has_records) : ?>
					<th class='column-check'><input class='check-all' type='checkbox' /></th>
					<?php endif;?>
					
					<th><?php echo lang('give_field_user_id'); ?></th>
					<th><?php echo lang('give_field_email'); ?></th>
					<th><?php echo lang('give_field_amount'); ?></th>
					<th><?php echo lang('give_field_status'); ?></th>
					<th><?php echo lang('give_field_donation_type'); ?></th>
					<th><?php echo lang('give_field_charge_created_at'); ?></th>
					<th><?php echo lang('give_field_stripe_customer_id'); ?></th>
					<th><?php echo lang('give_field_stripe_charge_id'); ?></th>
					<th><?php echo lang('give_field_stripe_balance_transaction_id'); ?></th>
					<th><?php echo lang('give_field_stripe_subscription_id'); ?></th>
				</tr>
			</thead>
			<?php if ($has_records) : ?>
			<tfoot>
				<?php if ($can_delete) : ?>
				<tr>
					<td colspan='<?php echo $num_columns; ?>'>
						<?php echo lang('bf_with_selected'); ?>
						<input type='submit' name='delete' id='delete-me' class='btn btn-danger' value="<?php echo lang('bf_action_delete'); ?>" onclick="return confirm('<?php e(js_escape(lang('give_delete_confirm'))); ?>')" />
					</td>
				</tr>
				<?php endif; ?>
			</tfoot>
			<?php endif; ?>
			<tbody>
				<?php
				if ($has_records) :
					foreach ($records as $record) :
				?>
				<tr>
					<?php if ($can_delete) : ?>
					<td class='column-check'><input type='checkbox' name='checked[]' value='<?php echo $record->id; ?>' /></td>
					<?php endif;?>
					
				<?php if ($can_edit) : ?>
					<td><?php echo anchor(SITE_AREA . '/settings/give/edit/' . $record->id, '<span class="icon-pencil"></span> ' .  $record->user_id); ?></td>
				<?php else : ?>
					<td><?php e($record->user_id); ?></td>
				<?php endif; ?>
					<td><?php e($record->email); ?></td>
					<td><?php e($record->amount); ?></td>
					<td><?php e($record->status); ?></td>
					<td><?php e($record->donation_type); ?></td>
					<td><?php e($record->charge_created_at); ?></td>
					<td><?php e($record->stripe_customer_id); ?></td>
					<td><?php e($record->stripe_charge_id); ?></td>
					<td><?php e($record->stripe_balance_transaction_id); ?></td>
					<td><?php e($record->stripe_subscription_id); ?></td>
				</tr>
				<?php
					endforeach;
				else:
				?>
				<tr>
					<td colspan='<?php echo $num_columns; ?>'><?php echo lang('give_records_empty'); ?></td>
				</tr>
				<?php endif; ?>
			</tbody>
		</table>
	<?php
    echo form_close();
    
    echo $this->pagination->create_links();
    ?>
</div>