<?php defined('BASEPATH') || exit('No direct script access allowed');


$lang['email_template_manage']      = 'Manage Email template';
$lang['email_template_edit']        = 'Edit';
$lang['email_template_true']        = 'True';
$lang['email_template_false']       = 'False';
$lang['email_template_create']      = 'Create';
$lang['email_template_list']        = 'List';
$lang['email_template_new']       = 'New';
$lang['email_template_edit_text']     = 'Edit this to suit your needs';
$lang['email_template_no_records']    = 'There are no email_template in the system.';
$lang['email_template_create_new']    = 'Create a new email_template.';
$lang['email_template_create_success']  = 'email_template successfully created.';
$lang['email_template_create_failure']  = 'There was a problem creating the email_template: ';
$lang['email_template_create_new_button'] = 'Create New email_template';
$lang['email_template_invalid_id']    = 'Invalid email_template ID.';
$lang['email_template_edit_success']    = 'email_template successfully saved.';
$lang['email_template_edit_failure']    = 'There was a problem saving the email_template: ';
$lang['email_template_delete_success']  = 'record(s) successfully deleted.';
$lang['email_template_delete_failure']  = 'We could not delete the record: ';
$lang['email_template_delete_error']    = 'You have not selected any records to delete.';
$lang['email_template_actions']     = 'Actions';
$lang['email_template_cancel']      = 'Cancel';
$lang['email_template_delete_record']   = 'Delete this email_template';
$lang['email_template_delete_confirm']  = 'Are you sure you want to delete this email_template?';
$lang['email_template_edit_heading']    = 'Edit Email template';

// Create/Edit Buttons
$lang['email_template_action_edit']   = 'Save Email template';
$lang['email_template_action_create']   = 'Create Email template';

// Activities
$lang['email_template_act_create_record'] = 'Created record with ID';
$lang['email_template_act_edit_record'] = 'Updated record with ID';
$lang['email_template_act_delete_record'] = 'Deleted record with ID';

//Listing Specifics
$lang['email_template_records_empty']    = 'No records found that match your selection.';
$lang['email_template_errors_message']    = 'Please fix the following errors:';

// Column Headings
$lang['email_template_column_created']  = 'Created';
$lang['email_template_column_deleted']  = 'Deleted';
$lang['email_template_column_modified'] = 'Modified';
$lang['email_template_column_deleted_by'] = 'Deleted By';
$lang['email_template_column_created_by'] = 'Created By';
$lang['email_template_column_modified_by'] = 'Modified By';

// Module Details
$lang['email_template_module_name'] = 'email_template';
$lang['email_template_module_description'] = 'Email Template module';
$lang['email_template_area_title'] = 'email_template';

// Fields
$lang['email_template_field_template_name'] = 'Email Template Name';
$lang['email_template_field_template_subject'] = 'Email Template Subject';
$lang['email_template_field_template_description'] = 'Email Template Description';
$lang['email_template_field_template_body'] = 'Email Template Body';
$lang['email_template_field_status'] = 'Email Template Status';