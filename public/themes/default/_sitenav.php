<header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <a class="navbar-brand" href="<?= base_url(); ?>"><?php e(class_exists('Settings_lib') ? settings_item('site.title') : 'MoviesIsland'); ?></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item active">
                    <a href="<?php echo site_url(); ?>"><i class="fas fa-home"></i> <?php e(lang('bf_home')); ?></a>
                </li>
            </ul>
            <div>
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                    <?php if (empty($current_user)) : ?>
                        <a href="<?php echo site_url(LOGIN_URL); ?>" class="nav-link"><i class="fas fa-sign-in-alt"></i></a>
                    <?php else : ?>
                        <a href="<?php echo site_url('logout'); ?>"><i class="fas fa-power-off"></i></a>
                    <?php endif; ?>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
<main role="main" class="flex-shrink-0" id="main">
    <div class="container">