<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BF
 *
 * An open source project to allow developers get a jumpstart their development of CodeIgniter applications
 *
 * @package   BASE
 * @author    BASE Dev Team
 * @copyright Copyright (c) 2011 - 2013, BASE Dev Team
 * @since     Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Content context controller
 *
 * The controller which displays the homepage of the Content context in BASE site.
 *
 * @package    BASE
 * @subpackage Controllers
 * @category   Controllers
 * @author     BASE Dev Team
 *
 */
class Content extends Admin_Controller
{


	/**
	 * Controller constructor sets the Title and Permissions
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		Template::set('toolbar_title', 'Content');

		$this->auth->restrict('Site.Content.View');
	}//end __construct()

	//--------------------------------------------------------------------

	/**
	 * Displays the initial page of the Content context
	 *
	 * @return void
	 */
	public function index()
	{
		$this->lang->load('admin');

		Template::set_view('admin/content/index');
		Template::render();
	}//end index()

	//--------------------------------------------------------------------


}//end class