<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['cms_manage']      = 'Gestisci CMS';
$lang['cms_edit']        = 'Modifica';
$lang['cms_true']        = 'Vero';
$lang['cms_false']       = 'Falso';
$lang['cms_create']      = 'Crea';
$lang['cms_list']        = 'Elenca';
$lang['cms_new']       = 'Nuovo';
$lang['cms_edit_text']     = 'Modifica questo secondo le tue necessità';
$lang['cms_no_records']    = 'Non ci sono cms nel sistema.';
$lang['cms_create_new']    = 'Crea un nuovo CMS.';
$lang['cms_create_success']  = 'CMS creato con successo.';
$lang['cms_create_failure']  = 'C\'è stato un problema nella creazione di cms: ';
$lang['cms_create_new_button'] = 'Crea nuovo CMS';
$lang['cms_invalid_id']    = 'ID CMS non valido.';
$lang['cms_edit_success']    = 'CMS creato con successo.';
$lang['cms_edit_failure']    = 'C\'è stato un errore nel salvataggio di cms: ';
$lang['cms_delete_success']  = 'record(s) creati con successo.';
$lang['cms_delete_failure']  = 'Non possiamo eliminare il record: ';
$lang['cms_delete_error']    = 'Non hai selezionato alcun record da eliminare.';
$lang['cms_actions']     = 'Azioni';
$lang['cms_cancel']      = 'Cancella';
$lang['cms_delete_record']   = 'Elimina questo CMS';
$lang['cms_delete_confirm']  = 'Sei sicuro di voler eliminare questo cms?';
$lang['cms_edit_heading']    = 'Modifica CMS';

// Create/Edit Buttons
$lang['cms_action_edit']   = 'Salva CMS';
$lang['cms_action_create']   = 'Crea CMS';

// Activities
$lang['cms_act_create_record'] = 'Creato il record con ID';
$lang['cms_act_edit_record'] = 'Aggiornato il record con ID';
$lang['cms_act_delete_record'] = 'Eliminato il record con ID';

// Listing Specifics
$lang['cms_records_empty']    = 'Nessun record trovato che corrisponda alla tua selezione.';
$lang['cms_errors_message']    = 'Per favore risolvi i seguenti errori:';

// Column Headings
$lang['cms_column_created']  = 'Creato';
$lang['cms_column_deleted']  = 'Eliminato';
$lang['cms_column_modified'] = 'Modificato';

// Module Details
$lang['cms_module_name'] = 'CMS';
$lang['cms_module_description'] = 'Your module description';
$lang['cms_area_title'] = 'CMS';

// Fields
$lang['cms_field_title'] = 'Page Title';
$lang['cms_field_slug_url'] = 'Page Slug URL';
$lang['cms_field_description'] = 'Page Content';
