<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['give_manage']      = 'Gestisci give';
$lang['give_edit']        = 'Modifica';
$lang['give_true']        = 'Vero';
$lang['give_false']       = 'Falso';
$lang['give_create']      = 'Crea';
$lang['give_list']        = 'Elenca';
$lang['give_new']       = 'Nuovo';
$lang['give_edit_text']     = 'Modifica questo secondo le tue necessità';
$lang['give_no_records']    = 'Non ci sono give nel sistema.';
$lang['give_create_new']    = 'Crea un nuovo give.';
$lang['give_create_success']  = 'give creato con successo.';
$lang['give_create_failure']  = 'C\'è stato un problema nella creazione di give: ';
$lang['give_create_new_button'] = 'Crea nuovo give';
$lang['give_invalid_id']    = 'ID give non valido.';
$lang['give_edit_success']    = 'give creato con successo.';
$lang['give_edit_failure']    = 'C\'è stato un errore nel salvataggio di give: ';
$lang['give_delete_success']  = 'record(s) creati con successo.';
$lang['give_delete_failure']  = 'Non possiamo eliminare il record: ';
$lang['give_delete_error']    = 'Non hai selezionato alcun record da eliminare.';
$lang['give_actions']     = 'Azioni';
$lang['give_cancel']      = 'Cancella';
$lang['give_delete_record']   = 'Elimina questo give';
$lang['give_delete_confirm']  = 'Sei sicuro di voler eliminare questo give?';
$lang['give_edit_heading']    = 'Modifica give';

// Create/Edit Buttons
$lang['give_action_edit']   = 'Salva give';
$lang['give_action_create']   = 'Crea give';

// Activities
$lang['give_act_create_record'] = 'Creato il record con ID';
$lang['give_act_edit_record'] = 'Aggiornato il record con ID';
$lang['give_act_delete_record'] = 'Eliminato il record con ID';

// Listing Specifics
$lang['give_records_empty']    = 'Nessun record trovato che corrisponda alla tua selezione.';
$lang['give_errors_message']    = 'Per favore risolvi i seguenti errori:';

// Column Headings
$lang['give_column_created']  = 'Creato';
$lang['give_column_deleted']  = 'Eliminato';
$lang['give_column_modified'] = 'Modificato';

// Module Details
$lang['give_module_name'] = 'give';
$lang['give_module_description'] = 'Donation Module';
$lang['give_area_title'] = 'give';

// Fields
$lang['give_field_user_id'] = 'fcc account ID';
$lang['give_field_email'] = 'Email';
$lang['give_field_amount'] = 'Amount';
$lang['give_field_status'] = 'Status';
$lang['give_field_donation_type'] = 'Donation type';
$lang['give_field_charge_created_at'] = 'Created at';
$lang['give_field_stripe_customer_id'] = 'Stripe Customer ID';
$lang['give_field_stripe_charge_id'] = 'Stripe Charge ID';
$lang['give_field_stripe_balance_transaction_id'] = 'Stripe Balance Transaction ID';
$lang['give_field_stripe_subscription_id'] = 'Stripe  Subscription ID';
