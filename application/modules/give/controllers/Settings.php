<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Settings controller
 */
class Settings extends Admin_Controller
{
    protected $permissionCreate = 'Give.Settings.Create';
    protected $permissionDelete = 'Give.Settings.Delete';
    protected $permissionEdit   = 'Give.Settings.Edit';
    protected $permissionView   = 'Give.Settings.View';

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->auth->restrict($this->permissionView);
        $this->load->model('give/give_model');
        $this->lang->load('give');
        
            Assets::add_css('flick/jquery-ui-1.8.13.custom.css');
            Assets::add_js('jquery-ui-1.8.13.min.js');
            Assets::add_css('jquery-ui-timepicker.css');
            Assets::add_js('jquery-ui-timepicker-addon.js');
            $this->form_validation->set_error_delimiters("<span class='error'>", "</span>");
        
        Template::set_block('sub_nav', 'settings/_sub_nav');

        Assets::add_module_js('give', 'give.js');
    }

    /**
     * Display a list of give data.
     *
     * @return void
     */
    public function index($offset = 0)
    {
        // Deleting anything?
        if (isset($_POST['delete'])) {
            $this->auth->restrict($this->permissionDelete);
            $checked = $this->input->post('checked');
            if (is_array($checked) && count($checked)) {

                // If any of the deletions fail, set the result to false, so
                // failure message is set if any of the attempts fail, not just
                // the last attempt

                $result = true;
                foreach ($checked as $pid) {
                    $deleted = $this->give_model->delete($pid);
                    if ($deleted == false) {
                        $result = false;
                    }
                }
                if ($result) {
                    Template::set_message(count($checked) . ' ' . lang('give_delete_success'), 'success');
                } else {
                    Template::set_message(lang('give_delete_failure') . $this->give_model->error, 'error');
                }
            }
        }
        $pagerUriSegment = 5;
        $pagerBaseUrl = site_url(SITE_AREA . '/settings/give/index') . '/';
        
        $limit  = $this->settings_lib->item('site.list_limit') ?: 15;

        $this->load->library('pagination');
        $this->pager['base_url']    = $pagerBaseUrl;
        $this->pager['total_rows']  = $this->give_model->count_all();
        $this->pager['per_page']    = $limit;
        $this->pager['uri_segment'] = $pagerUriSegment;

        $this->pagination->initialize($this->pager);
        $this->give_model->limit($limit, $offset);
        
        $records = $this->give_model->find_all();

        Template::set('records', $records);
        
        Template::set('toolbar_title', lang('give_manage'));

        Template::render();
    }
    
    /**
     * Create a give object.
     *
     * @return void
     */
    public function create()
    {
        $this->auth->restrict($this->permissionCreate);
        
        if (isset($_POST['save'])) {
            if ($insert_id = $this->save_give()) {
                log_activity($this->auth->user_id(), lang('give_act_create_record') . ': ' . $insert_id . ' : ' . $this->input->ip_address(), 'give');
                Template::set_message(lang('give_create_success'), 'success');

                redirect(SITE_AREA . '/settings/give');
            }

            // Not validation error
            if ( ! empty($this->give_model->error)) {
                Template::set_message(lang('give_create_failure') . $this->give_model->error, 'error');
            }
        }

        Template::set('toolbar_title', lang('give_action_create'));

        Template::render();
    }
    /**
     * Allows editing of give data.
     *
     * @return void
     */
    public function edit()
    {
        $id = $this->uri->segment(5);
        if (empty($id)) {
            Template::set_message(lang('give_invalid_id'), 'error');

            redirect(SITE_AREA . '/settings/give');
        }
        
        if (isset($_POST['save'])) {
            $this->auth->restrict($this->permissionEdit);

            if ($this->save_give('update', $id)) {
                log_activity($this->auth->user_id(), lang('give_act_edit_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'give');
                Template::set_message(lang('give_edit_success'), 'success');
                redirect(SITE_AREA . '/settings/give');
            }

            // Not validation error
            if ( ! empty($this->give_model->error)) {
                Template::set_message(lang('give_edit_failure') . $this->give_model->error, 'error');
            }
        }
        
        elseif (isset($_POST['delete'])) {
            $this->auth->restrict($this->permissionDelete);

            if ($this->give_model->delete($id)) {
                log_activity($this->auth->user_id(), lang('give_act_delete_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'give');
                Template::set_message(lang('give_delete_success'), 'success');

                redirect(SITE_AREA . '/settings/give');
            }

            Template::set_message(lang('give_delete_failure') . $this->give_model->error, 'error');
        }
        
        Template::set('give', $this->give_model->find($id));

        Template::set_view('create');
        Template::set('toolbar_title', lang('give_edit_heading'));
        Template::render();
    }

    //--------------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------------

    /**
     * Save the data.
     *
     * @param string $type Either 'insert' or 'update'.
     * @param int    $id   The ID of the record to update, ignored on inserts.
     *
     * @return boolean|integer An ID for successful inserts, true for successful
     * updates, else false.
     */
    private function save_give($type = 'insert', $id = 0)
    {
        if ($type == 'update') {
            $_POST['id'] = $id;
        }

        // Validate the data
        $this->form_validation->set_rules($this->give_model->get_validation_rules());
        if ($this->form_validation->run() === false) {
            return false;
        }

        // Make sure we only pass in the fields we want
        
        $data = $this->give_model->prep_data($this->input->post());

        // Additional handling for default values should be added below,
        // or in the model's prep_data() method
        
		$data['charge_created_at']	= $this->input->post('charge_created_at') ? $this->input->post('charge_created_at') : '0000-00-00 00:00:00';

        $return = false;
        if ($type == 'insert') {
            $id = $this->give_model->insert($data);

            if (is_numeric($id)) {
                $return = $id;
            }
        } elseif ($type == 'update') {
            $return = $this->give_model->update($id, $data);
        }

        return $return;
    }
}