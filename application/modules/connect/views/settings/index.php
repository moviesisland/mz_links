<?php

$num_columns	= 6;
$can_delete	= $this->auth->has_permission('Connect.Settings.Delete');
$can_edit		= $this->auth->has_permission('Connect.Settings.Edit');
$has_records	= isset($records) && is_array($records) && count($records);

if ($can_delete) {
    $num_columns++;
}
?>
<div class='admin-box'>
	<?php echo form_open($this->uri->uri_string()); ?>
		<table class='table table-striped'>
			<thead>
				<tr>
					<?php if ($can_delete && $has_records) : ?>
					<th class='column-check'><input class='check-all' type='checkbox' /></th>
					<?php endif;?>
					
					<th><?php echo lang('connect_field_option_title'); ?></th>
					<th><?php echo lang('connect_field_email'); ?></th>
					<th><?php echo lang('connect_field_workflow_name'); ?></th>
					<th><?php echo lang('connect_field_workflow_id'); ?></th>
					<th><?php echo lang('connect_column_created'); ?></th>
					<th><?php echo lang('connect_column_modified'); ?></th>
				</tr>
			</thead>
			<?php if ($has_records) : ?>
			<tfoot>
				<?php if ($can_delete) : ?>
				<tr>
					<td colspan='<?php echo $num_columns; ?>'>
						<?php echo lang('bf_with_selected'); ?>
						<input type='submit' name='delete' id='delete-me' class='btn btn-danger' value="<?php echo lang('bf_action_delete'); ?>" onclick="return confirm('<?php e(js_escape(lang('connect_delete_confirm'))); ?>')" />
					</td>
				</tr>
				<?php endif; ?>
			</tfoot>
			<?php endif; ?>
			<tbody>
				<?php
				if ($has_records) :
					foreach ($records as $record) :
				?>
				<tr>
					<?php if ($can_delete) : ?>
					<td class='column-check'><input type='checkbox' name='checked[]' value='<?php echo $record->id; ?>' /></td>
					<?php endif;?>
					
				<?php if ($can_edit) : ?>
					<td><?php echo anchor(SITE_AREA . '/settings/connect/edit/' . $record->id, '<span class="icon-pencil"></span> ' .  $record->option_title); ?></td>
				<?php else : ?>
					<td><?php e($record->option_title); ?></td>
				<?php endif; ?>
					<td><?php e($record->email); ?></td>
					<td><?php e($record->workflow_name); ?></td>
					<td><?php e($record->workflow_id); ?></td>
					<td><?php e(date(DATE_FORMAT,strtotime($record->created_on))); ?></td>
					<td><?php e(date(DATE_FORMAT,strtotime($record->modified_on))); ?></td>
				</tr>
				<?php
					endforeach;
				else:
				?>
				<tr>
					<td colspan='<?php echo $num_columns; ?>'><?php echo lang('connect_records_empty'); ?></td>
				</tr>
				<?php endif; ?>
			</tbody>
		</table>
	<?php
    echo form_close();
    
    echo $this->pagination->create_links();
    ?>
</div>