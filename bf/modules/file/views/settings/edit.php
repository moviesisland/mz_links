<?php

if (validation_errors()) :
?>
<div class='alert alert-block alert-error fade in'>
    <a class='close' data-dismiss='alert'>&times;</a>
    <h4 class='alert-heading'>
        <?php echo lang('file_errors_message'); ?>
    </h4>
    <?php echo validation_errors(); ?>
</div>
<?php
endif;

$id = isset($file->id) ? $file->id : '';
$currentMethod = $this->router->method;

?>
<div class='admin-box'>
    <?php echo form_open($this->uri->uri_string()); ?>
        

            <div class="form-group <?php echo form_error('name') ? ' error' : ''; ?>">
                <?php echo form_label(lang('file_field_name') . lang('bf_form_label_required'), 'name' ); ?>
                <input id='name' type='text' required='required' name='name' maxlength='255' value="<?php echo set_value('name', isset($file->name) ? $file->name : ''); ?>" class='form-control' />
                <span class='help-inline'><?php echo form_error('name'); ?></span>
            </div>

            <div class="form-group <?php echo form_error('size') ? ' error' : ''; ?>">
                <?php echo form_label(lang('file_field_size') . lang('bf_form_label_required'), 'size' ); ?>
                <input id='size' type='text' required='required' name='size' maxlength='100' value="<?php echo set_value('size', isset($file->size) ? $file->size : ''); ?>" class='form-control' />
                <span class='help-inline'><?php echo form_error('size'); ?></span>
            </div>

            <div class="form-group <?php echo form_error('slug') ? ' error' : ''; ?>">
                <?php echo form_label(lang('file_field_slug'), 'slug' ); ?>
                <input id='slug' type='text' name='slug' maxlength='100' value="<?php echo set_value('slug', isset($file->slug) ? $file->slug : ''); ?>" class='form-control' />
                <span class='help-inline'><?php echo form_error('slug'); ?></span>
            </div>
        <div class='form-group'>
            <?php $save_btnName = ($currentMethod == 'edit') ? lang('file_action_edit')  : lang('file_action_create'); ?>
            <input type='submit' name='save' class='btn btn-primary' value="<?php echo $save_btnName; ?>" />
            <?php echo lang('bf_or'); ?>
            <?php echo anchor(SITE_AREA . '/settings/file', lang('file_cancel'), 'class="btn btn-default"'); ?>
            
            <?php if ($this->auth->has_permission('File.Settings.Delete')) : ?>
                <?php echo lang('bf_or'); ?>
                <button type='submit' name='delete' formnovalidate class='btn btn-danger' id='delete-me' onclick="return confirm('<?php e(js_escape(lang('file_delete_confirm'))); ?>');">
                    <span class='icon-trash icon-white'></span>&nbsp;<?php echo lang('file_delete_record'); ?>
                </button>
            <?php endif; ?>
        </div>
    <?php echo form_close(); ?>
</div>