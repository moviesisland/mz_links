<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Hproviders controller
 */
class Hproviders extends Front_Controller
{
    protected $permissionCreate = 'Hproviders.Hproviders.Create';
    protected $permissionDelete = 'Hproviders.Hproviders.Delete';
    protected $permissionEdit   = 'Hproviders.Hproviders.Edit';
    protected $permissionView   = 'Hproviders.Hproviders.View';

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('hproviders/hproviders_model');
        $this->lang->load('hproviders');
        
        

        Assets::add_module_js('hproviders', 'hproviders.js');
    }

    /**
     * Display a list of hproviders data.
     *
     * @return void
     */
    public function index($offset = 0)
    {
        
        $pagerUriSegment = 3;
        $pagerBaseUrl = site_url('hproviders/index') . '/';
        
        $limit  = $this->settings_lib->item('site.list_limit') ?: 15;

        $this->load->library('pagination');
        $this->pager['base_url']    = $pagerBaseUrl;
        $this->pager['total_rows']  = $this->hproviders_model->count_all();
        $this->pager['per_page']    = $limit;
        $this->pager['uri_segment'] = $pagerUriSegment;

        $this->pagination->initialize($this->pager);
        $this->hproviders_model->limit($limit, $offset);
        

        // Don't display soft-deleted records
        $this->hproviders_model->where($this->hproviders_model->get_deleted_field(), 0);
        $records = $this->hproviders_model->find_all();

        Template::set('records', $records);
        

        Template::render();
    }
    
}