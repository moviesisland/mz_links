<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['cms_manage']      = 'Gestionar CMS';
$lang['cms_edit']        = 'Editar';
$lang['cms_true']        = 'Verdadero';
$lang['cms_false']       = 'Falso';
$lang['cms_create']      = 'Crear';
$lang['cms_list']        = 'Listar';
$lang['cms_new']       = 'Nuevo';
$lang['cms_edit_text']     = 'Editar esto para satisfacer sus necesidades';
$lang['cms_no_records']    = 'Hay ninguna cms en la sistema.';
$lang['cms_create_new']    = 'Crear nuevo(a) CMS.';
$lang['cms_create_success']  = 'CMS creado(a) con éxito.';
$lang['cms_create_failure']  = 'Hubo un problema al crear el(la) cms: ';
$lang['cms_create_new_button'] = 'Crear nuevo(a) CMS';
$lang['cms_invalid_id']    = 'ID de CMS inválido(a).';
$lang['cms_edit_success']    = 'CMS guardado correctamente.';
$lang['cms_edit_failure']    = 'Hubo un problema guardando el(la) cms: ';
$lang['cms_delete_success']  = 'Registro(s) eliminado con éxito.';
$lang['cms_delete_failure']  = 'No hemos podido eliminar el registro: ';
$lang['cms_delete_error']    = 'No ha seleccionado ning&#250;n registro que desea eliminar.';
$lang['cms_actions']     = 'Açciones';
$lang['cms_cancel']      = 'Cancelar';
$lang['cms_delete_record']   = 'Eliminar este(a) CMS';
$lang['cms_delete_confirm']  = '¿Esta seguro de que desea eliminar este(a) cms?';
$lang['cms_edit_heading']    = 'Editar CMS';

// Create/Edit Buttons
$lang['cms_action_edit']   = 'Guardar CMS';
$lang['cms_action_create']   = 'Crear CMS';

// Activities
$lang['cms_act_create_record'] = 'Creado registro con ID';
$lang['cms_act_edit_record'] = 'Actualizado registro con ID';
$lang['cms_act_delete_record'] = 'Eliminado registro con ID';

//Listing Specifics
$lang['cms_records_empty']    = 'No hay registros encontrados para su selección.';
$lang['cms_errors_message']    = 'Por favor corrija los siguientes errores:';

// Column Headings
$lang['cms_column_created']  = 'Creado';
$lang['cms_column_deleted']  = 'Elíminado';
$lang['cms_column_modified'] = 'Modificado';

// Module Details
$lang['cms_module_name'] = 'CMS';
$lang['cms_module_description'] = 'Your module description';
$lang['cms_area_title'] = 'CMS';

// Fields
$lang['cms_field_title'] = 'Page Title';
$lang['cms_field_slug_url'] = 'Page Slug URL';
$lang['cms_field_description'] = 'Page Content';
