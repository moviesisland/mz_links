<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Settings controller
 */
class Settings extends Admin_Controller
{
    protected $permissionCreate = 'File.Settings.Create';
    protected $permissionDelete = 'File.Settings.Delete';
    protected $permissionEdit   = 'File.Settings.Edit';
    protected $permissionView   = 'File.Settings.View';

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->auth->restrict($this->permissionView);
        $this->load->model('hproviders/hproviders_model');
        $this->load->model('file/file_model');
        $this->lang->load('file');
        
            $this->form_validation->set_error_delimiters("<span class='error'>", "</span>");
        
        Template::set_block('sub_nav', 'settings/_sub_nav');

        Assets::add_module_js('file', 'file.js');
    }

    /**
     * Display a list of file data.
     *
     * @return void
     */
    public function index($offset = 0)
    {
        // Deleting anything?
        if (isset($_POST['delete'])) {
            $this->auth->restrict($this->permissionDelete);
            $checked = $this->input->post('checked');
            if (is_array($checked) && count($checked)) {

                // If any of the deletions fail, set the result to false, so
                // failure message is set if any of the attempts fail, not just
                // the last attempt

                $result = true;
                foreach ($checked as $pid) {
                    $deleted = $this->file_model->delete($pid);
                    if ($deleted == false) {
                        $result = false;
                    }
                }
                if ($result) {
                    Template::set_message(count($checked) . ' ' . lang('file_delete_success'), 'success');
                } else {
                    Template::set_message(lang('file_delete_failure') . $this->file_model->error, 'error');
                }
            }
        }
        $pagerUriSegment = 5;
        $pagerBaseUrl = site_url(SITE_AREA . '/settings/file/index') . '/';
        
        $limit  = $this->settings_lib->item('site.list_limit') ?: 15;

        $this->load->library('pagination');
        $this->pager['base_url']    = $pagerBaseUrl;
        $this->pager['total_rows']  = $this->file_model->where('deleted', 0)->count_all();
        $this->pager['per_page']    = $limit;
        $this->pager['uri_segment'] = $pagerUriSegment;

        $this->pagination->initialize($this->pager);
        $this->file_model->limit($limit, $offset);
        
        $records = $this->file_model->where('deleted', 0)->find_all();

        Template::set('records', $records);
        
        Template::set('toolbar_title', lang('file_manage'));

        Template::render();
    }
    
    /**
     * Create a file object.
     *
     * @return void
     */
    public function create()
    {
        $this->auth->restrict($this->permissionCreate);
        
        if (isset($_POST['save'])) {
            if ($insert_id = $this->save_file()) {
                log_activity($this->auth->user_id(), lang('file_act_create_record') . ': ' . $insert_id . ' : ' . $this->input->ip_address(), 'file');
                Template::set_message(lang('file_create_success'), 'success');

                redirect(SITE_AREA . '/settings/file');
            }

            // Not validation error
            if ( ! empty($this->file_model->error)) {
                Template::set_message(lang('file_create_failure') . $this->file_model->error, 'error');
            }
        }

        Template::set('toolbar_title', lang('file_action_create'));

        Template::render();
    }
    /**
     * Allows editing of file data.
     *
     * @return void
     */
    public function edit()
    {
        $id = $this->uri->segment(5);
        if (empty($id)) {
            Template::set_message(lang('file_invalid_id'), 'error');

            redirect(SITE_AREA . '/settings/file');
        }
        
        if (isset($_POST['save'])) {
            $this->auth->restrict($this->permissionEdit);

            if ($this->save_file('update', $id)) {
                log_activity($this->auth->user_id(), lang('file_act_edit_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'file');
                Template::set_message(lang('file_edit_success'), 'success');
                redirect(SITE_AREA . '/settings/file');
            }

            // Not validation error
            if ( ! empty($this->file_model->error)) {
                Template::set_message(lang('file_edit_failure') . $this->file_model->error, 'error');
            }
        }
        
        elseif (isset($_POST['delete'])) {
            $this->auth->restrict($this->permissionDelete);

            if ($this->file_model->delete($id)) {
                log_activity($this->auth->user_id(), lang('file_act_delete_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'file');
                Template::set_message(lang('file_delete_success'), 'success');

                redirect(SITE_AREA . '/settings/file');
            }

            Template::set_message(lang('file_delete_failure') . $this->file_model->error, 'error');
        }

        // Get Hosing provider Details
        $Hrecords = $this->hproviders_model->find_all();
        Template::set('Hproviders', $Hrecords);

        // Get Links Details
        $links = $this->file_model->get_link_mapping($id);
        Template::set('links_data', $links);

        
        Template::set('file', $this->file_model->find($id));

        Template::set_view('create');
        Template::set('toolbar_title', lang('file_edit_heading'));
        Template::render();
    }

    //--------------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------------

    /**
     * Save the data.
     *
     * @param string $type Either 'insert' or 'update'.
     * @param int    $id   The ID of the record to update, ignored on inserts.
     *
     * @return boolean|integer An ID for successful inserts, true for successful
     * updates, else false.
     */
    private function save_file($type = 'insert', $id = 0)
    {
        if ($type == 'update') {
            $_POST['id'] = $id;
        }

        // Validate the data
        $this->form_validation->set_rules($this->file_model->get_validation_rules());
        if ($this->form_validation->run() === false) {
            return false;
        }

        // Make sure we only pass in the fields we want
        
        $data = $this->file_model->prep_data($this->input->post());
        // pre($this->input->post());

        // Custom Fileds
        $links = [];
        $direct_link = $this->input->post('direct_link') ?? '';
        $download_links = $this->input->post('download_link') ?? [];

        // Genrate New string while insert
        if ($type == 'insert') {

            if ( $data['slug'] == '' || empty($data['slug']) ) {
                
                // Create Random Alphnumeric String
                $DS = random_string_genrater(8);

                // Check if slug is alrady exits
                $is_available = $this->file_model->checkslug($DS);

                if ($is_available === TRUE) {
                    $data['slug'] = $DS;
                }
            }
        }
        
        // Extra Fileds save
        $links['direct_link'] = $direct_link;

        // Additional handling for default values should be added below,
        // or in the model's prep_data() method
        

        $return = false;
        if ($type == 'insert') {
            $id = $this->file_model->insert($data);
            $links['file_id'] = $id;
            // $this->file_model->add_record_download_link($links);

            if (is_numeric($id)) {
                $return = $id;
            }
        } elseif ($type == 'update') {
            $return = $this->file_model->update($id, $data);
            $this->file_model->add_record_link_mapping($download_links, $id);
        }

        return $return;
    }
}