<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['connect_manage']      = 'Gestisci connect';
$lang['connect_edit']        = 'Modifica';
$lang['connect_true']        = 'Vero';
$lang['connect_false']       = 'Falso';
$lang['connect_create']      = 'Crea';
$lang['connect_list']        = 'Elenca';
$lang['connect_new']       = 'Nuovo';
$lang['connect_edit_text']     = 'Modifica questo secondo le tue necessità';
$lang['connect_no_records']    = 'Non ci sono connect nel sistema.';
$lang['connect_create_new']    = 'Crea un nuovo connect.';
$lang['connect_create_success']  = 'connect creato con successo.';
$lang['connect_create_failure']  = 'C\'è stato un problema nella creazione di connect: ';
$lang['connect_create_new_button'] = 'Crea nuovo connect';
$lang['connect_invalid_id']    = 'ID connect non valido.';
$lang['connect_edit_success']    = 'connect creato con successo.';
$lang['connect_edit_failure']    = 'C\'è stato un errore nel salvataggio di connect: ';
$lang['connect_delete_success']  = 'record(s) creati con successo.';
$lang['connect_delete_failure']  = 'Non possiamo eliminare il record: ';
$lang['connect_delete_error']    = 'Non hai selezionato alcun record da eliminare.';
$lang['connect_actions']     = 'Azioni';
$lang['connect_cancel']      = 'Cancella';
$lang['connect_delete_record']   = 'Elimina questo connect';
$lang['connect_delete_confirm']  = 'Sei sicuro di voler eliminare questo connect?';
$lang['connect_edit_heading']    = 'Modifica connect';

// Create/Edit Buttons
$lang['connect_action_edit']   = 'Salva connect';
$lang['connect_action_create']   = 'Crea connect';

// Activities
$lang['connect_act_create_record'] = 'Creato il record con ID';
$lang['connect_act_edit_record'] = 'Aggiornato il record con ID';
$lang['connect_act_delete_record'] = 'Eliminato il record con ID';

// Listing Specifics
$lang['connect_records_empty']    = 'Nessun record trovato che corrisponda alla tua selezione.';
$lang['connect_errors_message']    = 'Per favore risolvi i seguenti errori:';

// Column Headings
$lang['connect_column_created']  = 'Creato';
$lang['connect_column_deleted']  = 'Eliminato';
$lang['connect_column_modified'] = 'Modificato';

// Module Details
$lang['connect_module_name'] = 'connect';
$lang['connect_module_description'] = 'Your module description';
$lang['connect_area_title'] = 'connect';

// Fields
$lang['connect_field_option_title'] = 'Option Title';
$lang['connect_field_email'] = 'Email';
$lang['connect_field_workflow_name'] = 'Workflow';
$lang['connect_field_workflow_id'] = 'Workflow ID';
