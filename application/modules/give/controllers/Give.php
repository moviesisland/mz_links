<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Give controller
 */
class Give extends Front_Controller
{
    protected $permissionCreate = 'Give.Give.Create';
    protected $permissionDelete = 'Give.Give.Delete';
    protected $permissionEdit   = 'Give.Give.Edit';
    protected $permissionView   = 'Give.Give.View';

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('give/give_model');
        $this->lang->load('give');
        
            Assets::add_css('flick/jquery-ui-1.8.13.custom.css');
            Assets::add_js('jquery-ui-1.8.13.min.js');
            Assets::add_css('jquery-ui-timepicker.css');
            Assets::add_js('jquery-ui-timepicker-addon.js');
        

        Assets::add_module_js('give', 'give.js');
    }

    /**
     * Display a list of give data.
     *
     * @return void
     */
    public function index($offset = 0)
    {
        
        $pagerUriSegment = 3;
        $pagerBaseUrl = site_url('give/index') . '/';
        
        $limit  = $this->settings_lib->item('site.list_limit') ?: 15;

        $this->load->library('pagination');
        $this->pager['base_url']    = $pagerBaseUrl;
        $this->pager['total_rows']  = $this->give_model->count_all();
        $this->pager['per_page']    = $limit;
        $this->pager['uri_segment'] = $pagerUriSegment;

        $this->pagination->initialize($this->pager);
        $this->give_model->limit($limit, $offset);
        

        // Don't display soft-deleted records
        $this->give_model->where($this->give_model->get_deleted_field(), 0);
        $records = $this->give_model->find_all();

        Template::set('records', $records);
        

        Template::render();
    }
    
}