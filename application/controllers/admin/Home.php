<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BF
 *
 * An open source project to allow developers get a jumpstart their development of CodeIgniter applications
 *
 * @package   BASE
 * @author    BASE Dev Team
 * @copyright Copyright (c) 2011 - 2013, BASE Dev Team
 /license.html
  * 
 * @since     Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Admin Home controller
 *
 * The base controller which handles visits to the admin area homepage in the BASE app.
 *
 * @package    BASE
 * @subpackage Controllers
 * @category   Controllers
 * @author     BASE Dev Team
 *
 */
class Home extends Admin_Controller
{


	/**
	 * Controller constructor sets the login restriction
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict();
	}//end __construct()

	//--------------------------------------------------------------------


	/**
	 * Redirects the user to the Content context
	 *
	 * @return void
	 */
    public function index()
    {
        $this->load->model('roles/role_model');

        $user_role = $this->role_model->find($this->current_user->role_id);
        $default_context = ($user_role !== false && isset($user_role->default_context)) ? $user_role->default_context : '';
        redirect(SITE_AREA .'/'.(isset($default_context) && !empty($default_context) ? $default_context : 'content'));
    }//end index()

	//--------------------------------------------------------------------


}//end class