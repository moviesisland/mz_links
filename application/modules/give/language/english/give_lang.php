<?php defined('BASEPATH') || exit('No direct script access allowed');


$lang['give_manage']      = 'Manage give';
$lang['give_edit']        = 'Edit';
$lang['give_true']        = 'True';
$lang['give_false']       = 'False';
$lang['give_create']      = 'Create';
$lang['give_list']        = 'List';
$lang['give_new']       = 'New';
$lang['give_edit_text']     = 'Edit this to suit your needs';
$lang['give_no_records']    = 'There are no give in the system.';
$lang['give_create_new']    = 'Create a new give.';
$lang['give_create_success']  = 'give successfully created.';
$lang['give_create_failure']  = 'There was a problem creating the give: ';
$lang['give_create_new_button'] = 'Create New give';
$lang['give_invalid_id']    = 'Invalid give ID.';
$lang['give_edit_success']    = 'give successfully saved.';
$lang['give_edit_failure']    = 'There was a problem saving the give: ';
$lang['give_delete_success']  = 'record(s) successfully deleted.';
$lang['give_delete_failure']  = 'We could not delete the record: ';
$lang['give_delete_error']    = 'You have not selected any records to delete.';
$lang['give_actions']     = 'Actions';
$lang['give_cancel']      = 'Cancel';
$lang['give_delete_record']   = 'Delete this give';
$lang['give_delete_confirm']  = 'Are you sure you want to delete this give?';
$lang['give_edit_heading']    = 'Edit give';

// Create/Edit Buttons
$lang['give_action_edit']   = 'Save give';
$lang['give_action_create']   = 'Create give';

// Activities
$lang['give_act_create_record'] = 'Created record with ID';
$lang['give_act_edit_record'] = 'Updated record with ID';
$lang['give_act_delete_record'] = 'Deleted record with ID';

//Listing Specifics
$lang['give_records_empty']    = 'No records found that match your selection.';
$lang['give_errors_message']    = 'Please fix the following errors:';

// Column Headings
$lang['give_column_created']  = 'Created';
$lang['give_column_deleted']  = 'Deleted';
$lang['give_column_modified'] = 'Modified';
$lang['give_column_deleted_by'] = 'Deleted By';
$lang['give_column_created_by'] = 'Created By';
$lang['give_column_modified_by'] = 'Modified By';

// Module Details
$lang['give_module_name'] = 'give';
$lang['give_module_description'] = 'Donation Module';
$lang['give_area_title'] = 'give';

// Fields
$lang['give_field_user_id'] = 'fcc account ID';
$lang['give_field_email'] = 'Email';
$lang['give_field_amount'] = 'Amount';
$lang['give_field_status'] = 'Status';
$lang['give_field_donation_type'] = 'Donation type';
$lang['give_field_charge_created_at'] = 'Created at';
$lang['give_field_stripe_customer_id'] = 'Stripe Customer ID';
$lang['give_field_stripe_charge_id'] = 'Stripe Charge ID';
$lang['give_field_stripe_balance_transaction_id'] = 'Stripe Balance Transaction ID';
$lang['give_field_stripe_subscription_id'] = 'Stripe  Subscription ID';