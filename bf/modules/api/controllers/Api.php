<?php defined('BASEPATH') || exit('No direct script access allowed');
/**
 * Utk_base
 *
 * An open source project to allow developers to jumpstart their development of
 * CodeIgniter applications.
 *
 * @package   UTKS_BASE
 * @author    Utk_base Dev Team
 * @copyright Copyright (c) 2011 - 2014, Utk_base Dev Team
 * @license   http://opensource.org/licenses/MIT
 * @link      http://cibonfire.com
 * @since     Version 1.0
 * @filesource
 */

/**
 * Users Controller.
 *
 * Provides front-end functions for users, including access to login and logout.
 *
 * @package utk_base\Modules\Users\Controllers\Users
 * @author     BASE Dev Team
 * @link    http://cibonfire.com/docs/developer
 */



require(APPPATH.'libraries/REST_Controller.php');


class Api extends  REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('users/auth');

        $this->lang->load('api');
        $this->output->enable_profiler(FALSE);
    }
    
    //This method to check login details.
    public function login_post()
    {
        // Get User name and Password
        $user_name = $this->input->post('user_name');
        $password = $this->input->post('password');

        // Check Login details
        if($this->auth->login($user_name,$password,0)) 
        {
            $message = array(
                'status' => 1,
                'message' => lang('us_loign_sucess')
            );

            // Add log activity
            log_activity(
                $this->auth->user_id(),
                lang('us_log_logged') . ': ' . $this->input->ip_address(),
                'users'
            );
        }
        else
        {
            $message = array(
                'status' => 0,
                'message' => $this->session->userdata('message')
            );

            // Add log activity
            log_activity(
                $this->auth->user_id(),
                lang('us_log_logged') . ': ' . $this->input->ip_address(),
                'users'
            );
        }

        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }
    public function forgotPassword_post()
    {
        // Load User Model
        $this->load->model('users/user_model');
        
        // Get user name from post
        $user_name = $this->input->post('user_name');

        // Find User name from DB
        $user = $this->user_model->find_by('username', $this->input->post('user_name'));

        //$user_id = 2;
        
        // No user found with the entered email address.
        if ($user === false) 
        {
            $message = array(
                'status' => 0,
                'message' => lang("us_invalid_user_name")
            );
        }
        else //Later on generate random password and send it via sms
        {
            $password = "12345678";
            $data =  array('password' =>  $password );
            if ($this->user_model->update($user->id, $data)) 
            {
                $message = array(
                    'status' => 1,
                    'message' => lang("us_password_updated")
                );
            }
        }

        // Set response
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
        
    }
    public function otpVerification_post()
    {
        // Load User Model
        $this->load->model('users/user_model');
        
        // Get OPT and user id
        $otp = $this->input->post('otp');
        $user_id = $this->input->post('user_id');

        if($this->user_model->check_otp($otp,$user_id))
        {
            $message = lang("us_otp_sucess");

            //make user active so they can login
            $data = array(
                "active" => 1
            );
            $this->user_model->update($user_id,$data);

            $status = 1;
        }
        else
        {
            $message = lang("us_otp_fail");
            $status = 0;
        }
        $message = array(
            'status' => $status,
            'message' => $message
        );

        // Send Response
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }
    public function listTable_post()
    {
        $this->load->model('api/table_model');
        $user_id = $this->input->post('user_id');

        $data = $this->table_model->list_all_table($user_id);

        $status =1;
        
        $message = [
            'status' => $status,
            'table_list' => $data
            ];  
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
        
    }
    public function checkUserStatus_post()
    {
        $this->load->model('user/user_model');
        $user_id = $this->input->post('user_id');
        $status = $this->user_model->check_user_active($user_id);

        $message = [ 'status' => $status ];  
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code

    }
    public function test()
    {
        $message = [
            'status' => 1,
            'message' => 'Hi There, This is Test Call'
            ];  
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }
}
