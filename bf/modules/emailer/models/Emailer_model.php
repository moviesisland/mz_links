<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * BF
 *
 * An open source project to allow developers to jumpstart their development of
 * CodeIgniter applications.
 *
 * @package   BASE
 * @author    BASE Dev Team
 * @copyright Copyright (c) 2011 - 2018, BASE Dev Team
 * @license   http://opensource.org/licenses/MIT The MIT License
  * 
 * @since     Version 1.0
 * @filesource
 */

/**
 * Emailer Model
 *
 * @package BF\Modules\Emailer\Models\Emailer_model
 * @author  BASE Dev Team
 * /docs
 */
class Emailer_model extends BF_Model
{
    /** @var string Name of the table. */
    protected $table_name = 'email_queue';

    /** @var string Name of the primary key. */
    protected $key = 'id';

    /** @var bool Whether to use soft deletes. */
    protected $soft_deletes = false;

    /** @var string The date format to use. */
    protected $date_format = 'datetime';

    /** @var bool Whether to set the created time automatically. */
    protected $set_created = false;

    /** @var bool Whether to set the modified time automatically. */
    protected $set_modified = false;
}
