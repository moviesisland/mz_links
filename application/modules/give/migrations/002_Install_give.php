<?php defined('BASEPATH') || exit('No direct script access allowed');

class Migration_Install_give extends Migration
{
	/**
	 * @var string The name of the database table
	 */
	private $table_name = 'give';

	/**
	 * @var array The table's fields
	 */
	private $fields = array(
		'id' => array(
			'type'       => 'INT',
			'constraint' => 11,
			'auto_increment' => true,
		),
        'user_id' => array(
            'type'       => 'BIGINT',
            'null'       => true,
        ),
        'email' => array(
            'type'       => 'VARCHAR',
            'constraint' => 255,
            'null'       => true,
        ),
        'amount' => array(
            'type'       => 'FLOAT',
            'constraint' => '20',
            'null'       => true,
        ),
        'status' => array(
            'type'       => 'TINYINT',
            'constraint' => 1,
            'null'       => true,
        ),
        'donation_type' => array(
            'type'       => 'TINYINT',
            'constraint' => 1,
            'null'       => true,
        ),
        'charge_created_at' => array(
            'type'       => 'DATETIME',
            'null'       => true,
            'default'    => '0000-00-00 00:00:00',
        ),
        'stripe_customer_id' => array(
            'type'       => 'VARCHAR',
            'constraint' => 255,
            'null'       => true,
        ),
        'stripe_charge_id' => array(
            'type'       => 'VARCHAR',
            'constraint' => 255,
            'null'       => true,
        ),
        'stripe_balance_transaction_id' => array(
            'type'       => 'VARCHAR',
            'constraint' => 255,
            'null'       => true,
        ),
        'stripe_subscription_id' => array(
            'type'       => 'VARCHAR',
            'constraint' => 255,
            'null'       => true,
        ),
        'deleted' => array(
            'type'       => 'TINYINT',
            'constraint' => 1,
            'default'    => '0',
        ),
        'deleted_by' => array(
            'type'       => 'BIGINT',
            'constraint' => 20,
            'null'       => true,
        ),
	);

	/**
	 * Install this version
	 *
	 * @return void
	 */
	public function up()
	{
		$this->dbforge->add_field($this->fields);
		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table($this->table_name);
	}

	/**
	 * Uninstall this version
	 *
	 * @return void
	 */
	public function down()
	{
		$this->dbforge->drop_table($this->table_name);
	}
}