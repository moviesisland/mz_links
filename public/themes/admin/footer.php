<!-- <footer class="main-footer"></footer> -->
    
<!-- Load JS Files -->
<?php echo Assets::js(null, true); ?>

<script type="text/javascript">
var current_ajax_req = null;
var ajaxCounter = 0;

// This function is used to get error message for all ajax calls
function getErrorMessage(jqXHR, exception) {
    var msg = '';
    if (jqXHR.status === 0) {
        msg = 'Not connect.\n Verify Network.';
    } else if (jqXHR.status == 404) {
        msg = 'Requested page not found. [404]';
    } else if (jqXHR.status == 500) {
        msg = 'Internal Server Error [500].';
    } else if (exception === 'parsererror') {
        msg = 'Requested JSON parse failed.';
    } else if (exception === 'timeout') {
        msg = 'Time out error.';
    } else if (exception === 'abort') {
        msg = 'Ajax request aborted.';
    } else {
        msg = 'Uncaught Error.\n' + jqXHR.responseText;
    }
    $('#ajaxContent').text(msg);
}

// Ajax Call
$('#ajax_test').click(function () {

    // Block UI
    $('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>').insertAfter( ".box-body" );

    // Ajax Call
    current_ajax_req = $.ajax({
        url: "<?php echo site_url().'admin/settings/users/test' ?>",
        type: 'POST',
        beforeSend : function() { if(current_ajax_req != null) current_ajax_req.abort(); },
        data: {
            <?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>',
            isAjax: 1,
            ajaxCounter: ajaxCounter,
        },
        error: function (jqXHR, exception) {
            console.log(jqXHR);
            getErrorMessage(jqXHR, exception);
        },
        success: function(result) {
            if (ajaxCounter%2 == 0) {
                $("pre#ajaxContent").css({"backgroundColor": "#27ae60", "color": "white"});
            }
            else
            {
                $("pre#ajaxContent").css({"backgroundColor": "#e67e22", "color": "white"});
            }
            $("#ajaxContent").text(result);
            $( ".overlay" ).remove(); /* Remove Block UI */
            ajaxCounter++;
        }
    });
});
</script>
</body>
</html>
