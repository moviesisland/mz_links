<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['give_manage']      = 'Gestionar give';
$lang['give_edit']        = 'Editar';
$lang['give_true']        = 'Verdadero';
$lang['give_false']       = 'Falso';
$lang['give_create']      = 'Crear';
$lang['give_list']        = 'Listar';
$lang['give_new']       = 'Nuevo';
$lang['give_edit_text']     = 'Editar esto para satisfacer sus necesidades';
$lang['give_no_records']    = 'Hay ninguna give en la sistema.';
$lang['give_create_new']    = 'Crear nuevo(a) give.';
$lang['give_create_success']  = 'give creado(a) con éxito.';
$lang['give_create_failure']  = 'Hubo un problema al crear el(la) give: ';
$lang['give_create_new_button'] = 'Crear nuevo(a) give';
$lang['give_invalid_id']    = 'ID de give inválido(a).';
$lang['give_edit_success']    = 'give guardado correctamente.';
$lang['give_edit_failure']    = 'Hubo un problema guardando el(la) give: ';
$lang['give_delete_success']  = 'Registro(s) eliminado con éxito.';
$lang['give_delete_failure']  = 'No hemos podido eliminar el registro: ';
$lang['give_delete_error']    = 'No ha seleccionado ning&#250;n registro que desea eliminar.';
$lang['give_actions']     = 'Açciones';
$lang['give_cancel']      = 'Cancelar';
$lang['give_delete_record']   = 'Eliminar este(a) give';
$lang['give_delete_confirm']  = '¿Esta seguro de que desea eliminar este(a) give?';
$lang['give_edit_heading']    = 'Editar give';

// Create/Edit Buttons
$lang['give_action_edit']   = 'Guardar give';
$lang['give_action_create']   = 'Crear give';

// Activities
$lang['give_act_create_record'] = 'Creado registro con ID';
$lang['give_act_edit_record'] = 'Actualizado registro con ID';
$lang['give_act_delete_record'] = 'Eliminado registro con ID';

//Listing Specifics
$lang['give_records_empty']    = 'No hay registros encontrados para su selección.';
$lang['give_errors_message']    = 'Por favor corrija los siguientes errores:';

// Column Headings
$lang['give_column_created']  = 'Creado';
$lang['give_column_deleted']  = 'Elíminado';
$lang['give_column_modified'] = 'Modificado';

// Module Details
$lang['give_module_name'] = 'give';
$lang['give_module_description'] = 'Donation Module';
$lang['give_area_title'] = 'give';

// Fields
$lang['give_field_user_id'] = 'fcc account ID';
$lang['give_field_email'] = 'Email';
$lang['give_field_amount'] = 'Amount';
$lang['give_field_status'] = 'Status';
$lang['give_field_donation_type'] = 'Donation type';
$lang['give_field_charge_created_at'] = 'Created at';
$lang['give_field_stripe_customer_id'] = 'Stripe Customer ID';
$lang['give_field_stripe_charge_id'] = 'Stripe Charge ID';
$lang['give_field_stripe_balance_transaction_id'] = 'Stripe Balance Transaction ID';
$lang['give_field_stripe_subscription_id'] = 'Stripe  Subscription ID';
