<?php

if (validation_errors()) :
?>
<div class='alert alert-block alert-error fade in'>
    <a class='close' data-dismiss='alert'>&times;</a>
    <h4 class='alert-heading'>
        <?php echo lang('cms_errors_message'); ?>
    </h4>
    <?php echo validation_errors(); ?>
</div>
<?php
endif;

$id = isset($cms->id) ? $cms->id : '';
$currentMethod = $this->router->method;

?>
<div class='admin-box'>
    <?php echo form_open($this->uri->uri_string()); ?>
        

            <div class="form-group <?php echo form_error('title') ? ' error' : ''; ?>">
                <?php echo form_label(lang('cms_field_title') . lang('bf_form_label_required'), 'title' ); ?>
                <input id='title' type='text' required='required' name='title' maxlength='255' value="<?php echo set_value('title', isset($cms->title) ? $cms->title : ''); ?>" class='form-control' />
                <span class='help-inline'><?php echo form_error('title'); ?></span>
            </div>

            <div class="form-group <?php echo form_error('slug_url') ? ' error' : ''; ?>">
                <?php echo form_label(lang('cms_field_slug_url'), 'slug_url' ); ?>
                <input id='slug_url' type='text' name='slug_url' maxlength='255' value="<?php echo set_value('slug_url', isset($cms->slug_url) ? $cms->slug_url : ''); ?>" class='form-control' />
                <span class='help-inline'><?php echo form_error('slug_url'); ?></span>
            </div>

            <div class="form-group <?php echo form_error('description') ? ' error' : ''; ?>">
                <?php echo form_label(lang('cms_field_description') . lang('bf_form_label_required'), 'description'); ?>
                    <?php echo form_textarea(array('name' => 'description', 'id' => 'description', 'class' => 'form-control', 'rows' => '5', 'cols' => '80', 'value' => set_value('description', isset($cms->description) ? $cms->description : ''), 'required' => 'required')); ?>
                    <span class='help-inline'><?php echo form_error('description'); ?></span>
            </div>
        <div class='form-group'>
            <?php $save_btnName = ($currentMethod == 'edit') ? lang('cms_action_edit')  : lang('cms_action_create'); ?>
            <input type='submit' name='save' class='btn btn-primary' value="<?php echo $save_btnName; ?>" />
            <?php echo lang('bf_or'); ?>
            <?php echo anchor(SITE_AREA . '/settings/cms', lang('cms_cancel'), 'class="btn btn-default"'); ?>
            
            <?php if ($this->auth->has_permission('CMS.Settings.Delete')) : ?>
                <?php echo lang('bf_or'); ?>
                <button type='submit' name='delete' formnovalidate class='btn btn-danger' id='delete-me' onclick="return confirm('<?php e(js_escape(lang('cms_delete_confirm'))); ?>');">
                    <span class='icon-trash icon-white'></span>&nbsp;<?php echo lang('cms_delete_record'); ?>
                </button>
            <?php endif; ?>
        </div>
    <?php echo form_close(); ?>
</div>