<div class="container">
	
	<?php if (!empty($data)): ?>
		<h1><?php echo $data->title; ?></h1>
		<br>
		<div id="page-container"><?php echo $data->description; ?></div>
	<?php endif ?>
</div>