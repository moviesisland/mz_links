<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['file_manage']      = 'Gestisci file';
$lang['file_edit']        = 'Modifica';
$lang['file_true']        = 'Vero';
$lang['file_false']       = 'Falso';
$lang['file_create']      = 'Crea';
$lang['file_list']        = 'Elenca';
$lang['file_new']       = 'Nuovo';
$lang['file_edit_text']     = 'Modifica questo secondo le tue necessità';
$lang['file_no_records']    = 'Non ci sono file nel sistema.';
$lang['file_create_new']    = 'Crea un nuovo file.';
$lang['file_create_success']  = 'file creato con successo.';
$lang['file_create_failure']  = 'C\'è stato un problema nella creazione di file: ';
$lang['file_create_new_button'] = 'Crea nuovo file';
$lang['file_invalid_id']    = 'ID file non valido.';
$lang['file_edit_success']    = 'file creato con successo.';
$lang['file_edit_failure']    = 'C\'è stato un errore nel salvataggio di file: ';
$lang['file_delete_success']  = 'record(s) creati con successo.';
$lang['file_delete_failure']  = 'Non possiamo eliminare il record: ';
$lang['file_delete_error']    = 'Non hai selezionato alcun record da eliminare.';
$lang['file_actions']     = 'Azioni';
$lang['file_cancel']      = 'Cancella';
$lang['file_delete_record']   = 'Elimina questo file';
$lang['file_delete_confirm']  = 'Sei sicuro di voler eliminare questo file?';
$lang['file_edit_heading']    = 'Modifica file';

// Create/Edit Buttons
$lang['file_action_edit']   = 'Salva file';
$lang['file_action_create']   = 'Crea file';

// Activities
$lang['file_act_create_record'] = 'Creato il record con ID';
$lang['file_act_edit_record'] = 'Aggiornato il record con ID';
$lang['file_act_delete_record'] = 'Eliminato il record con ID';

// Listing Specifics
$lang['file_records_empty']    = 'Nessun record trovato che corrisponda alla tua selezione.';
$lang['file_errors_message']    = 'Per favore risolvi i seguenti errori:';

// Column Headings
$lang['file_column_created']  = 'Creato';
$lang['file_column_deleted']  = 'Eliminato';
$lang['file_column_modified'] = 'Modificato';

// Module Details
$lang['file_module_name'] = 'file';
$lang['file_module_description'] = 'Host file information';
$lang['file_area_title'] = 'file';

// Fields
$lang['file_field_name'] = 'File Name';
$lang['file_field_size'] = 'File Size';
$lang['file_field_slug'] = 'URL Slug';
