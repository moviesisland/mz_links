-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 23, 2020 at 02:38 PM
-- Server version: 8.0.21
-- PHP Version: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mz_links`
--

-- --------------------------------------------------------

--
-- Table structure for table `bf_activities`
--

DROP TABLE IF EXISTS `bf_activities`;
CREATE TABLE IF NOT EXISTS `bf_activities` (
  `activity_id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL DEFAULT '0',
  `activity` varchar(255) NOT NULL,
  `module` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL,
  `deleted` tinyint NOT NULL DEFAULT '0',
  PRIMARY KEY (`activity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1536 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bf_activities`
--

INSERT INTO `bf_activities` (`activity_id`, `user_id`, `activity`, `module`, `created_on`, `deleted`) VALUES
(1506, 1, 'logged in from: 127.0.0.1', 'users', '2020-09-19 12:49:24', 0),
(1507, 1, 'logged in from: 127.0.0.1', 'users', '2020-09-19 15:48:25', 0),
(1508, 1, 'logged out from: 127.0.0.1', 'users', '2020-09-19 15:50:47', 0),
(1509, 1, 'logged in from: 127.0.0.1', 'users', '2020-09-19 17:23:00', 0),
(1510, 1, 'logged out from: 127.0.0.1', 'users', '2020-09-19 17:24:13', 0),
(1511, 1, 'logged in from: 127.0.0.1', 'users', '2020-09-19 17:25:16', 0),
(1512, 1, 'logged out from: 127.0.0.1', 'users', '2020-09-19 17:25:33', 0),
(1513, 1, 'logged in from: 127.0.0.1', 'users', '2020-09-19 17:25:50', 0),
(1514, 1, 'logged out from: 127.0.0.1', 'users', '2020-09-19 17:25:55', 0),
(1515, 1, 'logged in from: 127.0.0.1', 'users', '2020-09-19 17:27:22', 0),
(1516, 1, 'logged out from: 127.0.0.1', 'users', '2020-09-19 17:29:31', 0),
(1517, 1, 'logged in from: 127.0.0.1', 'users', '2020-09-19 17:29:36', 0),
(1518, 1, 'logged out from: 127.0.0.1', 'users', '2020-09-19 17:31:35', 0),
(1519, 1, 'logged in from: 127.0.0.1', 'users', '2020-09-19 17:31:38', 0),
(1520, 1, 'App settings saved from: 127.0.0.1', 'core', '2020-09-19 17:31:50', 0),
(1521, 1, 'logged in from: 127.0.0.1', 'users', '2020-09-22 10:41:34', 0),
(1522, 1, 'logged out from: 127.0.0.1', 'users', '2020-09-22 12:17:04', 0),
(1523, 1, 'logged in from: 127.0.0.1', 'users', '2020-09-22 12:18:04', 0),
(1524, 1, 'logged out from: 127.0.0.1', 'users', '2020-09-22 12:18:17', 0),
(1525, 1, 'logged in from: 127.0.0.1', 'users', '2020-09-22 14:21:09', 0),
(1526, 1, 'Created Module: file : 127.0.0.1', 'modulebuilder', '2020-09-22 14:41:09', 0),
(1527, 1, 'logged in from: 127.0.0.1', 'users', '2020-09-23 07:41:03', 0),
(1528, 1, 'Deleted Module: file : 127.0.0.1', 'builder', '2020-09-23 07:41:11', 0),
(1529, 1, 'Created Module: file : 127.0.0.1', 'modulebuilder', '2020-09-23 07:42:55', 0),
(1530, 1, 'logged in from: 127.0.0.1', 'users', '2020-09-23 12:35:39', 0),
(1531, 1, 'logged out from: 127.0.0.1', 'users', '2020-09-23 12:36:02', 0),
(1532, 1, 'logged in from: 127.0.0.1', 'users', '2020-09-23 12:36:23', 0),
(1533, 1, 'Created record with ID: 1 : 127.0.0.1', 'file', '2020-09-23 12:37:18', 0),
(1534, 1, 'Created record with ID: 2 : 127.0.0.1', 'file', '2020-09-23 13:09:11', 0),
(1535, 1, 'Created record with ID: 2 : 127.0.0.1', 'file', '2020-09-23 13:26:21', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bf_ci3_sessions`
--

DROP TABLE IF EXISTS `bf_ci3_sessions`;
CREATE TABLE IF NOT EXISTS `bf_ci3_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bf_ci3_sessions`
--

INSERT INTO `bf_ci3_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('053mamj2itped3897fe45sa63174rnhi', '127.0.0.1', 1600866349, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630303836363334393b7265717565737465645f706167657c733a34363a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f61646d696e2f73657474696e67732f66696c652f637265617465223b70726576696f75735f706167657c733a34363a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f61646d696e2f73657474696e67732f66696c652f637265617465223b757365725f69647c733a313a2231223b617574685f637573746f6d7c733a353a2261646d696e223b757365725f746f6b656e7c733a34303a2235383237646236343463313063326434353936623630363536396366353335303834396636333362223b6964656e746974797c733a31343a2261646d696e40626173652e636f6d223b726f6c655f69647c733a313a2231223b6c6f676765645f696e7c623a313b6c616e67756167657c733a373a22656e676c697368223b),
('0uv4tkj0ftvspn45jvn8sjfbldhfgs0v', '127.0.0.1', 1600594681, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630303539343638313b7265717565737465645f706167657c733a36313a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f646f63732f342e352f6173736574732f6272616e642f626f6f7473747261702d736f6c69642e737667223b70726576696f75735f706167657c733a36313a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f646f63732f342e352f6173736574732f6272616e642f626f6f7473747261702d736f6c69642e737667223b),
('1l426uq3g5icodnip3mcvf8i41ckkvpl', '127.0.0.1', 1600865185, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630303836353138353b7265717565737465645f706167657c733a34363a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f61646d696e2f73657474696e67732f66696c652f637265617465223b70726576696f75735f706167657c733a34363a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f61646d696e2f73657474696e67732f66696c652f637265617465223b757365725f69647c733a313a2231223b617574685f637573746f6d7c733a353a2261646d696e223b757365725f746f6b656e7c733a34303a2235383237646236343463313063326434353936623630363536396366353335303834396636333362223b6964656e746974797c733a31343a2261646d696e40626173652e636f6d223b726f6c655f69647c733a313a2231223b6c6f676765645f696e7c623a313b6c616e67756167657c733a373a22656e676c697368223b),
('2nkt4ljhfpbcpebhd7a1ihq4rrp24eg8', '127.0.0.1', 1600785669, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630303738353636393b7265717565737465645f706167657c733a35393a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f61646d696e2f646576656c6f7065722f6275696c6465722f6372656174655f6d6f64756c652f33223b70726576696f75735f706167657c733a35393a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f61646d696e2f646576656c6f7065722f6275696c6465722f6372656174655f6d6f64756c652f33223b757365725f69647c733a313a2231223b617574685f637573746f6d7c733a353a2261646d696e223b757365725f746f6b656e7c733a34303a2235383237646236343463313063326434353936623630363536396366353335303834396636333362223b6964656e746974797c733a31343a2261646d696e40626173652e636f6d223b726f6c655f69647c733a313a2231223b6c6f676765645f696e7c623a313b6c616e67756167657c733a373a22656e676c697368223b),
('368fsfppq3di3r9019cce19t9njfqkee', '127.0.0.1', 1600596217, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630303539363231373b7265717565737465645f706167657c733a32303a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f223b70726576696f75735f706167657c733a32303a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f223b6d6573736167657c733a303a22223b5f5f63695f766172737c613a313a7b733a373a226d657373616765223b733a333a226e6577223b7d),
('3bj568na9m7ndf3u723dc42casdfoaho', '127.0.0.1', 1600870499, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630303837303439393b7265717565737465645f706167657c733a34363a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f61646d696e2f73657474696e67732f66696c652f656469742f32223b70726576696f75735f706167657c733a34363a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f61646d696e2f73657474696e67732f66696c652f656469742f32223b757365725f69647c733a313a2231223b617574685f637573746f6d7c733a353a2261646d696e223b757365725f746f6b656e7c733a34303a2235383237646236343463313063326434353936623630363536396366353335303834396636333362223b6964656e746974797c733a31343a2261646d696e40626173652e636f6d223b726f6c655f69647c733a313a2231223b6c6f676765645f696e7c623a313b6c616e67756167657c733a373a22656e676c697368223b),
('3om1sfojv2mtj158q45mrdbgbcb76fnp', '127.0.0.1', 1600870184, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630303837303138343b7265717565737465645f706167657c733a34363a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f61646d696e2f73657474696e67732f66696c652f656469742f31223b70726576696f75735f706167657c733a34363a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f61646d696e2f73657474696e67732f66696c652f656469742f31223b757365725f69647c733a313a2231223b617574685f637573746f6d7c733a353a2261646d696e223b757365725f746f6b656e7c733a34303a2235383237646236343463313063326434353936623630363536396366353335303834396636333362223b6964656e746974797c733a31343a2261646d696e40626173652e636f6d223b726f6c655f69647c733a313a2231223b6c6f676765645f696e7c623a313b6c616e67756167657c733a373a22656e676c697368223b),
('4lr4gfh4q3d7ubbce28rpqm88adiqkc5', '127.0.0.1', 1600870807, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630303837303830373b7265717565737465645f706167657c733a34363a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f61646d696e2f73657474696e67732f66696c652f656469742f32223b70726576696f75735f706167657c733a34363a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f61646d696e2f73657474696e67732f66696c652f656469742f32223b757365725f69647c733a313a2231223b617574685f637573746f6d7c733a353a2261646d696e223b757365725f746f6b656e7c733a34303a2235383237646236343463313063326434353936623630363536396366353335303834396636333362223b6964656e746974797c733a31343a2261646d696e40626173652e636f6d223b726f6c655f69647c733a313a2231223b6c6f676765645f696e7c623a313b6c616e67756167657c733a373a22656e676c697368223b),
('6iso3igi5t9ujvbp11bmoduk6nurfqqg', '127.0.0.1', 1600595325, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630303539353332353b7265717565737465645f706167657c733a36313a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f646f63732f342e352f6173736574732f6272616e642f626f6f7473747261702d736f6c69642e737667223b70726576696f75735f706167657c733a36313a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f646f63732f342e352f6173736574732f6272616e642f626f6f7473747261702d736f6c69642e737667223b),
('7ljitv7bn389a8gkc90dk733808pcv5e', '127.0.0.1', 1600600471, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630303630303335313b7265717565737465645f706167657c733a32303a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f223b70726576696f75735f706167657c733a32303a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f223b6d6573736167657c733a303a22223b5f5f63695f766172737c613a313a7b733a373a226d657373616765223b733a333a226e6577223b7d),
('8mt6stt13k6021r08k4b5euehq52ejc5', '127.0.0.1', 1600777097, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630303737373039373b7265717565737465645f706167657c733a32303a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f223b70726576696f75735f706167657c733a32303a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f223b),
('926tk5uk7s40p6590ojsd91ofo1uoerm', '127.0.0.1', 1600596550, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630303539363535303b7265717565737465645f706167657c733a32303a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f223b70726576696f75735f706167657c733a32303a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f223b6d6573736167657c733a303a22223b5f5f63695f766172737c613a313a7b733a373a226d657373616765223b733a333a226e6577223b7d),
('9j34h0n4khbkh8lbvql7a1cob2uvmaca', '127.0.0.1', 1600785669, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630303738353636393b7265717565737465645f706167657c733a35393a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f61646d696e2f646576656c6f7065722f6275696c6465722f6372656174655f6d6f64756c652f33223b70726576696f75735f706167657c733a35393a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f61646d696e2f646576656c6f7065722f6275696c6465722f6372656174655f6d6f64756c652f33223b757365725f69647c733a313a2231223b617574685f637573746f6d7c733a353a2261646d696e223b757365725f746f6b656e7c733a34303a2235383237646236343463313063326434353936623630363536396366353335303834396636333362223b6964656e746974797c733a31343a2261646d696e40626173652e636f6d223b726f6c655f69647c733a313a2231223b6c6f676765645f696e7c623a313b6c616e67756167657c733a373a22656e676c697368223b),
('af1kdb87o7sbpnsu2cgo5mj6iunc39d2', '127.0.0.1', 1600769617, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630303736393631373b7265717565737465645f706167657c733a32303a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f223b70726576696f75735f706167657c733a32303a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f223b6d6573736167657c733a303a22223b5f5f63695f766172737c613a313a7b733a373a226d657373616765223b733a333a226f6c64223b7d),
('dga2e6cvmbc5httuqinsgpsmmaja64pi', '127.0.0.1', 1600777018, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630303737373031383b7265717565737465645f706167657c733a35373a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f61646d696e2f646576656c6f7065722f6275696c6465722f6372656174655f6d6f64756c65223b70726576696f75735f706167657c733a35373a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f61646d696e2f646576656c6f7065722f6275696c6465722f6372656174655f6d6f64756c65223b757365725f69647c733a313a2231223b617574685f637573746f6d7c733a353a2261646d696e223b757365725f746f6b656e7c733a34303a2235383237646236343463313063326434353936623630363536396366353335303834396636333362223b6964656e746974797c733a31343a2261646d696e40626173652e636f6d223b726f6c655f69647c733a313a2231223b6c6f676765645f696e7c623a313b6c616e67756167657c733a373a22656e676c697368223b),
('et6ukpl7mht7cpgog7pvlfl1f40bt7mu', '127.0.0.1', 1600771331, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630303737313333313b7265717565737465645f706167657c733a32303a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f223b70726576696f75735f706167657c733a32303a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f223b757365725f69647c733a313a2231223b617574685f637573746f6d7c733a353a2261646d696e223b757365725f746f6b656e7c733a34303a2235383237646236343463313063326434353936623630363536396366353335303834396636333362223b6964656e746974797c733a31343a2261646d696e40626173652e636f6d223b726f6c655f69647c733a313a2231223b6c6f676765645f696e7c623a313b6c616e67756167657c733a373a22656e676c697368223b),
('f540gfgc43nbaqaa3tp7kindrd5ktobm', '127.0.0.1', 1600770264, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630303737303236343b7265717565737465645f706167657c733a32303a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f223b70726576696f75735f706167657c733a32303a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f223b),
('j2ar8l4103k2ot0bdmjvfcqikul2je29', '127.0.0.1', 1600595003, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630303539353030333b7265717565737465645f706167657c733a36313a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f646f63732f342e352f6173736574732f6272616e642f626f6f7473747261702d736f6c69642e737667223b70726576696f75735f706167657c733a36313a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f646f63732f342e352f6173736574732f6272616e642f626f6f7473747261702d736f6c69642e737667223b),
('n4kt8nmstce4sg63meas3jqpis48n9qm', '127.0.0.1', 1600869686, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630303836393638363b7265717565737465645f706167657c733a34363a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f61646d696e2f73657474696e67732f66696c652f656469742f31223b70726576696f75735f706167657c733a34363a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f61646d696e2f73657474696e67732f66696c652f656469742f31223b757365725f69647c733a313a2231223b617574685f637573746f6d7c733a353a2261646d696e223b757365725f746f6b656e7c733a34303a2235383237646236343463313063326434353936623630363536396366353335303834396636333362223b6964656e746974797c733a31343a2261646d696e40626173652e636f6d223b726f6c655f69647c733a313a2231223b6c6f676765645f696e7c623a313b6c616e67756167657c733a373a22656e676c697368223b6d6573736167657c733a303a22223b5f5f63695f766172737c613a313a7b733a373a226d657373616765223b733a333a226f6c64223b7d),
('oef876nb2egkvfpsi4vk05q36fi2t79c', '127.0.0.1', 1600864870, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630303836343837303b7265717565737465645f706167657c733a34363a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f61646d696e2f73657474696e67732f66696c652f637265617465223b70726576696f75735f706167657c733a34363a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f61646d696e2f73657474696e67732f66696c652f637265617465223b757365725f69647c733a313a2231223b617574685f637573746f6d7c733a353a2261646d696e223b757365725f746f6b656e7c733a34303a2235383237646236343463313063326434353936623630363536396366353335303834396636333362223b6964656e746974797c733a31343a2261646d696e40626173652e636f6d223b726f6c655f69647c733a313a2231223b6c6f676765645f696e7c623a313b6c616e67756167657c733a373a22656e676c697368223b),
('p1sqb30pki54c2anpn1q36k1ub46bp4a', '127.0.0.1', 1600771030, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630303737313033303b7265717565737465645f706167657c733a32303a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f223b70726576696f75735f706167657c733a32303a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f223b),
('r47uoms61l4lj4f3vmraht0t8fffsh3u', '127.0.0.1', 1600596869, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630303539363836393b7265717565737465645f706167657c733a32303a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f223b70726576696f75735f706167657c733a32303a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f223b6d6573736167657c733a303a22223b5f5f63695f766172737c613a313a7b733a373a226d657373616765223b733a333a226e6577223b7d),
('rjmmh8l9nl1m2h4qcbueao805ikuhvds', '127.0.0.1', 1600599861, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630303539393836313b7265717565737465645f706167657c733a32303a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f223b70726576696f75735f706167657c733a32303a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f223b6d6573736167657c733a303a22223b5f5f63695f766172737c613a313a7b733a373a226d657373616765223b733a333a226e6577223b7d),
('sh9vdg8h86156ou7jistbcgcr7uprd33', '127.0.0.1', 1600600351, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630303630303335313b7265717565737465645f706167657c733a32303a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f223b70726576696f75735f706167657c733a32303a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f223b6d6573736167657c733a303a22223b5f5f63695f766172737c613a313a7b733a373a226d657373616765223b733a333a226e6577223b7d),
('tst0iqmqvuml4bie1qtu15r6jpjtcal5', '127.0.0.1', 1600867369, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630303836373336393b7265717565737465645f706167657c733a33393a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f61646d696e2f73657474696e67732f66696c65223b70726576696f75735f706167657c733a33393a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f61646d696e2f73657474696e67732f66696c65223b757365725f69647c733a313a2231223b617574685f637573746f6d7c733a353a2261646d696e223b757365725f746f6b656e7c733a34303a2235383237646236343463313063326434353936623630363536396366353335303834396636333362223b6964656e746974797c733a31343a2261646d696e40626173652e636f6d223b726f6c655f69647c733a313a2231223b6c6f676765645f696e7c623a313b6c616e67756167657c733a373a22656e676c697368223b),
('ug0i80lnn7hnonhdcrd5396gapcrnq7q', '127.0.0.1', 1600846989, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630303834363835383b7265717565737465645f706167657c733a34363a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f61646d696e2f73657474696e67732f66696c652f637265617465223b70726576696f75735f706167657c733a34363a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f61646d696e2f73657474696e67732f66696c652f637265617465223b757365725f69647c733a313a2231223b617574685f637573746f6d7c733a353a2261646d696e223b757365725f746f6b656e7c733a34303a2235383237646236343463313063326434353936623630363536396366353335303834396636333362223b6964656e746974797c733a31343a2261646d696e40626173652e636f6d223b726f6c655f69647c733a313a2231223b6c6f676765645f696e7c623a313b6c616e67756167657c733a373a22656e676c697368223b),
('v2nf5rn3vpdjkk0ldv127f32ob9f5lh9', '127.0.0.1', 1600866786, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630303836363738363b7265717565737465645f706167657c733a33393a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f61646d696e2f73657474696e67732f66696c65223b70726576696f75735f706167657c733a33393a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f61646d696e2f73657474696e67732f66696c65223b757365725f69647c733a313a2231223b617574685f637573746f6d7c733a353a2261646d696e223b757365725f746f6b656e7c733a34303a2235383237646236343463313063326434353936623630363536396366353335303834396636333362223b6964656e746974797c733a31343a2261646d696e40626173652e636f6d223b726f6c655f69647c733a313a2231223b6c6f676765645f696e7c623a313b6c616e67756167657c733a373a22656e676c697368223b),
('v5cpmnqnmr61baikt27po2qufvnagfhd', '127.0.0.1', 1600870936, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630303837303830373b7265717565737465645f706167657c733a34363a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f61646d696e2f73657474696e67732f66696c652f656469742f31223b70726576696f75735f706167657c733a34363a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f61646d696e2f73657474696e67732f66696c652f656469742f31223b757365725f69647c733a313a2231223b617574685f637573746f6d7c733a353a2261646d696e223b757365725f746f6b656e7c733a34303a2235383237646236343463313063326434353936623630363536396366353335303834396636333362223b6964656e746974797c733a31343a2261646d696e40626173652e636f6d223b726f6c655f69647c733a313a2231223b6c6f676765645f696e7c623a313b6c616e67756167657c733a373a22656e676c697368223b),
('vcphk1l8r6kq1b3r7acqo5sv7r2khe7l', '127.0.0.1', 1600595734, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630303539353733343b7265717565737465645f706167657c733a36313a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f646f63732f342e352f6173736574732f6272616e642f626f6f7473747261702d736f6c69642e737667223b70726576696f75735f706167657c733a36313a22687474703a2f2f6d7a5f6c696e6b732e636f6d2f646f63732f342e352f6173736574732f6272616e642f626f6f7473747261702d736f6c69642e737667223b);

-- --------------------------------------------------------

--
-- Table structure for table `bf_cms`
--

DROP TABLE IF EXISTS `bf_cms`;
CREATE TABLE IF NOT EXISTS `bf_cms` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug_url` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_by` bigint DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` bigint NOT NULL,
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` bigint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bf_email_queue`
--

DROP TABLE IF EXISTS `bf_email_queue`;
CREATE TABLE IF NOT EXISTS `bf_email_queue` (
  `id` int NOT NULL AUTO_INCREMENT,
  `to_email` varchar(254) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `alt_message` text,
  `max_attempts` int NOT NULL DEFAULT '3',
  `attempts` int NOT NULL DEFAULT '0',
  `success` tinyint(1) NOT NULL DEFAULT '0',
  `date_published` datetime DEFAULT NULL,
  `last_attempt` datetime DEFAULT NULL,
  `date_sent` datetime DEFAULT NULL,
  `csv_attachment` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bf_email_template`
--

DROP TABLE IF EXISTS `bf_email_template`;
CREATE TABLE IF NOT EXISTS `bf_email_template` (
  `id` int NOT NULL AUTO_INCREMENT,
  `template_name` varchar(150) NOT NULL,
  `template_subject` varchar(255) NOT NULL,
  `template_description` text,
  `template_body` text NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_by` bigint DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` bigint NOT NULL,
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` bigint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bf_email_template`
--

INSERT INTO `bf_email_template` (`id`, `template_name`, `template_subject`, `template_description`, `template_body`, `status`, `deleted`, `deleted_by`, `created_on`, `created_by`, `modified_on`, `modified_by`) VALUES
(1, 'test_template', 'Test Mail', 'sdf', '\r\nhii [NAME] This is body\r\n<h1>hiii</h1>', 1, 0, NULL, '2019-12-21 13:44:05', 1, '2019-12-25 11:05:49', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bf_files`
--

DROP TABLE IF EXISTS `bf_files`;
CREATE TABLE IF NOT EXISTS `bf_files` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `size` varchar(100) NOT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_by` bigint DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` bigint NOT NULL,
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` bigint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bf_files`
--

INSERT INTO `bf_files` (`id`, `name`, `size`, `slug`, `deleted`, `deleted_by`, `created_on`, `created_by`, `modified_on`, `modified_by`) VALUES
(1, 'Paatal Lok S01 E01 WebRip Hindi 480p ESub - Moviesisland', '112 MB', '7f84Xo1J', 0, NULL, '2020-09-23 12:37:18', 1, '0000-00-00 00:00:00', NULL),
(2, 'Paatal Lok S01 E02 WebRip Hindi 480p ESub - Moviesisland', '119 MB', 'PBeItdm0', 0, NULL, '2020-09-23 13:26:21', 1, '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bf_keys`
--

DROP TABLE IF EXISTS `bf_keys`;
CREATE TABLE IF NOT EXISTS `bf_keys` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `is_private_key` tinyint(1) NOT NULL DEFAULT '0',
  `ip_addresses` text,
  `date_created` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bf_keys`
--

INSERT INTO `bf_keys` (`id`, `user_id`, `key`, `level`, `ignore_limits`, `is_private_key`, `ip_addresses`, `date_created`) VALUES
(1, 1, '1234', 0, 0, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bf_login_attempts`
--

DROP TABLE IF EXISTS `bf_login_attempts`;
CREATE TABLE IF NOT EXISTS `bf_login_attempts` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bf_permissions`
--

DROP TABLE IF EXISTS `bf_permissions`;
CREATE TABLE IF NOT EXISTS `bf_permissions` (
  `permission_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(100) NOT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=234 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bf_permissions`
--

INSERT INTO `bf_permissions` (`permission_id`, `name`, `description`, `status`) VALUES
(2, 'Site.Content.View', 'Allow users to view the Content Context', 'active'),
(3, 'Site.Reports.View', 'Allow users to view the Reports Context', 'active'),
(4, 'Site.Settings.View', 'Allow users to view the Settings Context', 'active'),
(5, 'Site.Developer.View', 'Allow users to view the Developer Context', 'active'),
(6, 'Bonfire.Roles.Manage', 'Allow users to manage the user Roles', 'active'),
(7, 'Bonfire.Users.Manage', 'Allow users to manage the site Users', 'active'),
(8, 'Bonfire.Users.View', 'Allow users access to the User Settings', 'active'),
(9, 'Bonfire.Users.Add', 'Allow users to add new Users', 'active'),
(10, 'Bonfire.Database.Manage', 'Allow users to manage the Database settings', 'active'),
(11, 'Bonfire.Emailer.Manage', 'Allow users to manage the Emailer settings', 'active'),
(12, 'Bonfire.Logs.View', 'Allow users access to the Log details', 'active'),
(13, 'Bonfire.Logs.Manage', 'Allow users to manage the Log files', 'active'),
(14, 'Bonfire.Emailer.View', 'Allow users access to the Emailer settings', 'active'),
(15, 'Site.Signin.Offline', 'Allow users to login to the site when the site is offline', 'active'),
(16, 'Bonfire.Permissions.View', 'Allow access to view the Permissions menu unders Settings Context', 'active'),
(17, 'Bonfire.Permissions.Manage', 'Allow access to manage the Permissions in the system', 'active'),
(18, 'Bonfire.Modules.Add', 'Allow creation of modules with the builder.', 'active'),
(19, 'Bonfire.Modules.Delete', 'Allow deletion of modules.', 'active'),
(20, 'Permissions.Administrator.Manage', 'To manage the access control permissions for the Administrator role.', 'active'),
(21, 'Permissions.Editor.Manage', 'To manage the access control permissions for the Editor role.', 'active'),
(23, 'Permissions.User.Manage', 'To manage the access control permissions for the User role.', 'active'),
(24, 'Permissions.Developer.Manage', 'To manage the access control permissions for the Developer role.', 'active'),
(26, 'Activities.Own.View', 'To view the users own activity logs', 'active'),
(27, 'Activities.Own.Delete', 'To delete the users own activity logs', 'active'),
(28, 'Activities.User.View', 'To view the user activity logs', 'active'),
(29, 'Activities.User.Delete', 'To delete the user activity logs, except own', 'active'),
(30, 'Activities.Module.View', 'To view the module activity logs', 'active'),
(31, 'Activities.Module.Delete', 'To delete the module activity logs', 'active'),
(32, 'Activities.Date.View', 'To view the users own activity logs', 'active'),
(33, 'Activities.Date.Delete', 'To delete the dated activity logs', 'active'),
(34, 'Bonfire.UI.Manage', 'Manage the Bonfire UI settings', 'active'),
(35, 'Bonfire.Settings.View', 'To view the site settings page.', 'active'),
(36, 'Bonfire.Settings.Manage', 'To manage the site settings.', 'active'),
(37, 'Bonfire.Activities.View', 'To view the Activities menu.', 'active'),
(38, 'Bonfire.Database.View', 'To view the Database menu.', 'active'),
(39, 'Bonfire.Migrations.View', 'To view the Migrations menu.', 'active'),
(40, 'Bonfire.Builder.View', 'To view the Modulebuilder menu.', 'active'),
(41, 'Bonfire.Roles.View', 'To view the Roles menu.', 'active'),
(42, 'Bonfire.Sysinfo.View', 'To view the System Information page.', 'active'),
(43, 'Bonfire.Translate.Manage', 'To manage the Language Translation.', 'active'),
(44, 'Bonfire.Translate.View', 'To view the Language Translate menu.', 'active'),
(45, 'Bonfire.UI.View', 'To view the UI/Keyboard Shortcut menu.', 'active'),
(48, 'Bonfire.Profiler.View', 'To view the Console Profiler Bar.', 'active'),
(49, 'Bonfire.Roles.Add', 'To add New Roles', 'active'),
(50, 'Email_template.Settings.View', 'View Email_template Settings', 'active'),
(51, 'Email_template.Settings.Create', 'Create Email_template Settings', 'active'),
(52, 'Email_template.Settings.Edit', 'Edit Email_template Settings', 'active'),
(53, 'Email_template.Settings.Delete', 'Delete Email_template Settings', 'active'),
(230, 'File.Settings.View', 'View File Settings', 'active'),
(231, 'File.Settings.Create', 'Create File Settings', 'active'),
(232, 'File.Settings.Edit', 'Edit File Settings', 'active'),
(233, 'File.Settings.Delete', 'Delete File Settings', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `bf_roles`
--

DROP TABLE IF EXISTS `bf_roles`;
CREATE TABLE IF NOT EXISTS `bf_roles` (
  `role_id` int NOT NULL AUTO_INCREMENT,
  `role_name` varchar(60) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `can_delete` tinyint(1) NOT NULL DEFAULT '1',
  `login_destination` varchar(255) NOT NULL DEFAULT '/',
  `default_context` varchar(255) DEFAULT 'content',
  `deleted` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bf_roles`
--

INSERT INTO `bf_roles` (`role_id`, `role_name`, `description`, `default`, `can_delete`, `login_destination`, `default_context`, `deleted`) VALUES
(1, 'Administrator', 'Has full control over every aspect of the site.', 0, 0, 'admin/content', 'content', 0),
(2, 'Editor', 'Can handle day-to-day management, but does not have full power.', 0, 1, '', 'content', 0),
(4, 'User', 'This is the default user with access to login.', 1, 0, '', 'content', 0),
(6, 'Developer', 'Developers typically are the only ones that can access the developer tools. Otherwise identical to Administrators, at least until the site is handed off.', 0, 1, '', 'content', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bf_role_permissions`
--

DROP TABLE IF EXISTS `bf_role_permissions`;
CREATE TABLE IF NOT EXISTS `bf_role_permissions` (
  `role_id` int NOT NULL,
  `permission_id` int NOT NULL,
  PRIMARY KEY (`role_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bf_role_permissions`
--

INSERT INTO `bf_role_permissions` (`role_id`, `permission_id`) VALUES
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 23),
(1, 24),
(1, 26),
(1, 27),
(1, 28),
(1, 29),
(1, 30),
(1, 31),
(1, 32),
(1, 33),
(1, 34),
(1, 35),
(1, 36),
(1, 37),
(1, 38),
(1, 39),
(1, 40),
(1, 41),
(1, 42),
(1, 43),
(1, 44),
(1, 48),
(1, 49),
(1, 50),
(1, 51),
(1, 52),
(1, 53),
(1, 226),
(1, 227),
(1, 228),
(1, 229),
(1, 230),
(1, 231),
(1, 232),
(1, 233),
(2, 2),
(2, 3),
(6, 2),
(6, 3),
(6, 4),
(6, 5),
(6, 6),
(6, 7),
(6, 8),
(6, 9),
(6, 10),
(6, 11),
(6, 12),
(6, 13),
(6, 48);

-- --------------------------------------------------------

--
-- Table structure for table `bf_schema_version`
--

DROP TABLE IF EXISTS `bf_schema_version`;
CREATE TABLE IF NOT EXISTS `bf_schema_version` (
  `type` varchar(40) NOT NULL,
  `version` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bf_schema_version`
--

INSERT INTO `bf_schema_version` (`type`, `version`) VALUES
('core', 44),
('file_', 2);

-- --------------------------------------------------------

--
-- Table structure for table `bf_sessions`
--

DROP TABLE IF EXISTS `bf_sessions`;
CREATE TABLE IF NOT EXISTS `bf_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bf_settings`
--

DROP TABLE IF EXISTS `bf_settings`;
CREATE TABLE IF NOT EXISTS `bf_settings` (
  `name` varchar(30) NOT NULL,
  `module` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bf_settings`
--

INSERT INTO `bf_settings` (`name`, `module`, `value`) VALUES
('auth.allow_name_change', 'core', '1'),
('auth.allow_register', 'core', '0'),
('auth.allow_remember', 'core', '1'),
('auth.do_login_redirect', 'core', '1'),
('auth.login_type', 'core', 'email'),
('auth.name_change_frequency', 'core', '1'),
('auth.name_change_limit', 'core', '1'),
('auth.password_force_mixed_case', 'core', '0'),
('auth.password_force_numbers', 'core', '0'),
('auth.password_force_symbols', 'core', '0'),
('auth.password_min_length', 'core', '8'),
('auth.password_show_labels', 'core', '0'),
('auth.remember_length', 'core', '1209600'),
('auth.user_activation_method', 'core', '0'),
('auth.use_extended_profile', 'core', '0'),
('auth.use_usernames', 'core', '1'),
('ext.country', 'core', 'US'),
('ext.state', 'core', ''),
('ext.street_name', 'core', ''),
('ext.type', 'core', 'small'),
('form_save', 'core.ui', 'ctrl+s/⌘+s'),
('goto_content', 'core.ui', 'alt+c'),
('mailpath', 'email', '/usr/sbin/sendmail'),
('mailtype', 'email', 'text'),
('password_iterations', 'users', '8'),
('protocol', 'email', 'mail'),
('sender_email', 'email', 'info@base.com'),
('site.languages', 'core', 'a:1:{i:0;s:7:\"english\";}'),
('site.list_limit', 'core', '10'),
('site.offline_reason', 'core', ''),
('site.show_front_profiler', 'core', '1'),
('site.show_profiler', 'core', '1'),
('site.status', 'core', '1'),
('site.system_email', 'core', 'admin@base.com'),
('site.title', 'core', 'MZ Links'),
('smtp_host', 'email', ''),
('smtp_pass', 'email', ''),
('smtp_port', 'email', ''),
('smtp_timeout', 'email', ''),
('smtp_user', 'email', '');

-- --------------------------------------------------------

--
-- Table structure for table `bf_users`
--

DROP TABLE IF EXISTS `bf_users`;
CREATE TABLE IF NOT EXISTS `bf_users` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` int NOT NULL DEFAULT '4',
  `email` varchar(254) NOT NULL,
  `username` varchar(30) NOT NULL DEFAULT '',
  `password_hash` char(60) DEFAULT NULL,
  `reset_hash` varchar(40) DEFAULT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_ip` varchar(45) NOT NULL DEFAULT '',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `reset_by` int DEFAULT NULL,
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_message` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT '',
  `last_name` varchar(255) DEFAULT NULL,
  `address` text,
  `city` varchar(150) DEFAULT NULL,
  `state` char(2) DEFAULT NULL,
  `zip` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `display_name_changed` date DEFAULT NULL,
  `timezone` varchar(40) NOT NULL DEFAULT 'UM6',
  `language` varchar(20) NOT NULL DEFAULT 'english',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `activate_hash` varchar(40) NOT NULL DEFAULT '',
  `force_password_reset` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bf_users`
--

INSERT INTO `bf_users` (`id`, `role_id`, `email`, `username`, `password_hash`, `reset_hash`, `last_login`, `last_ip`, `created_on`, `deleted`, `reset_by`, `banned`, `ban_message`, `display_name`, `last_name`, `address`, `city`, `state`, `zip`, `phone`, `display_name_changed`, `timezone`, `language`, `active`, `activate_hash`, `force_password_reset`) VALUES
(1, 1, 'admin@base.com', 'admin', '$2a$08$z28xG14EjTOMZf2xIGXFEu3OgAvPHAxZGMMxAU3jHiN1xZB1X0SPK', NULL, '2020-09-23 12:36:23', '127.0.0.1', '2018-08-18 08:56:06', 0, NULL, 0, NULL, 'admin', '', '', '0', 'NY', '', '', NULL, 'UM6', 'english', 1, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bf_user_cookies`
--

DROP TABLE IF EXISTS `bf_user_cookies`;
CREATE TABLE IF NOT EXISTS `bf_user_cookies` (
  `user_id` bigint UNSIGNED NOT NULL,
  `token` varchar(128) NOT NULL,
  `created_on` datetime NOT NULL,
  KEY `token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
