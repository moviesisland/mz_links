<?php

if (validation_errors()) :
?>
<div class='alert alert-block alert-error fade in'>
    <a class='close' data-dismiss='alert'>&times;</a>
    <h4 class='alert-heading'>
        <?php echo lang('hproviders_errors_message'); ?>
    </h4>
    <?php echo validation_errors(); ?>
</div>
<?php
endif;

$id = isset($hproviders->id) ? $hproviders->id : '';
$currentMethod = $this->router->method;

?>
<div class='admin-box'>
    <?php echo form_open($this->uri->uri_string()); ?>
        

            <div class="form-group <?php echo form_error('name') ? ' error' : ''; ?>">
                <?php echo form_label(lang('hproviders_field_name') . lang('bf_form_label_required'), 'name' ); ?>
                <input id='name' type='text' required='required' name='name' maxlength='255' value="<?php echo set_value('name', isset($hproviders->name) ? $hproviders->name : ''); ?>" class='form-control' />
                <span class='help-inline'><?php echo form_error('name'); ?></span>
            </div>

            <div class="form-group <?php echo form_error('website_url') ? ' error' : ''; ?>">
                <?php echo form_label(lang('hproviders_field_website_url') . lang('bf_form_label_required'), 'website_url' ); ?>
                <input id='website_url' type='text' required='required' name='website_url' maxlength='255' value="<?php echo set_value('website_url', isset($hproviders->website_url) ? $hproviders->website_url : ''); ?>" class='form-control' />
                <span class='help-inline'><?php echo form_error('website_url'); ?></span>
            </div>

            <div class="form-group <?php echo form_error('desc') ? ' error' : ''; ?>">
                <?php echo form_label(lang('hproviders_field_desc'), 'desc'); ?>
                    <?php echo form_textarea(array('name' => 'desc', 'id' => 'desc', 'class' => 'form-control', 'rows' => '5', 'cols' => '80', 'value' => set_value('desc', isset($hproviders->desc) ? $hproviders->desc : ''))); ?>
                    <span class='help-inline'><?php echo form_error('desc'); ?></span>
            </div>
        <div class='form-group'>
            <?php $save_btnName = ($currentMethod == 'edit') ? lang('hproviders_action_edit')  : lang('hproviders_action_create'); ?>
            <input type='submit' name='save' class='btn btn-primary' value="<?php echo $save_btnName; ?>" />
            <?php echo lang('bf_or'); ?>
            <?php echo anchor(SITE_AREA . '/settings/hproviders', lang('hproviders_cancel'), 'class="btn btn-default"'); ?>
            
        </div>
    <?php echo form_close(); ?>
</div>