<?php

if (validation_errors()) :
?>
<div class='alert alert-block alert-error fade in'>
    <a class='close' data-dismiss='alert'>&times;</a>
    <h4 class='alert-heading'>
        <?php echo lang('connect_errors_message'); ?>
    </h4>
    <?php echo validation_errors(); ?>
</div>
<?php
endif;

$id = isset($connect->id) ? $connect->id : '';
$currentMethod = $this->router->method;

?>
<div class='admin-box'>
    <?php echo form_open($this->uri->uri_string()); ?>
        

            <div class="form-group <?php echo form_error('option_title') ? ' error' : ''; ?>">
                <?php echo form_label(lang('connect_field_option_title') . lang('bf_form_label_required'), 'option_title' ); ?>
                <input id='option_title' type='text' required='required' name='option_title' maxlength='255' value="<?php echo set_value('option_title', isset($connect->option_title) ? $connect->option_title : ''); ?>" class='form-control' />
                <span class='help-inline'><?php echo form_error('option_title'); ?></span>
            </div>

            <div class="form-group <?php echo form_error('email') ? ' error' : ''; ?>">
                <?php echo form_label(lang('connect_field_email') . lang('bf_form_label_required'), 'email' ); ?>
                <input id='email' type='text' required='required' name='email' maxlength='255' value="<?php echo set_value('email', isset($connect->email) ? $connect->email : ''); ?>" class='form-control' />
                <span class='help-inline'><?php echo form_error('email'); ?></span>
            </div>

            <div class="form-group">
                <?php // Change the values in this array to populate your dropdown as required
                    $options = array(
                        255 => 255,
                    );
                    echo form_dropdown(array('name' => 'workflow_name', 'class'=>'form-control', 'required' => 'required'), $options, set_value('workflow_name', isset($connect->workflow_name) ? $connect->workflow_name : ''), lang('connect_field_workflow_name') . lang('bf_form_label_required'));
                ?>
            </div>

            <div class="form-group <?php echo form_error('workflow_id') ? ' error' : ''; ?>">
                <?php echo form_label(lang('connect_field_workflow_id'), 'workflow_id' ); ?>
                <input id='workflow_id' type='text' name='workflow_id' maxlength='255' value="<?php echo set_value('workflow_id', isset($connect->workflow_id) ? $connect->workflow_id : ''); ?>" class='form-control' />
                <span class='help-inline'><?php echo form_error('workflow_id'); ?></span>
            </div>
        <div class='form-group'>
            <?php $save_btnName = ($currentMethod == 'edit') ? lang('connect_action_edit')  : lang('connect_action_create'); ?>
            <input type='submit' name='save' class='btn btn-primary' value="<?php echo $save_btnName; ?>" />
            <?php echo lang('bf_or'); ?>
            <?php echo anchor(SITE_AREA . '/settings/connect', lang('connect_cancel'), 'class="btn btn-default"'); ?>
            
        </div>
    <?php echo form_close(); ?>
</div>