<?php if (has_permission('Bonfire.Users.Manage')):?>
<ul class="nav nav-tabs">
	<li role="presentation" <?php echo ($this->uri->segment(4) == '' || $this->uri->segment(4) == 'index') ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(SITE_AREA .'/settings/users') ?>"><?php echo lang('bf_users'); ?></a>
	</li>
	<li role="presentation" <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(SITE_AREA .'/settings/users/create') ?>" id="create_new"><?php echo lang('bf_new') .' '. lang('bf_user'); ?></a>
	</li>
</ul>
<br>
<?php endif;?>