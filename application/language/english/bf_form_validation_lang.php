<?php defined('BASEPATH') || exit('No direct script access allowed');
/**
 * BF
 *
 * An open source project to allow developers to jumpstart their development of
 * CodeIgniter applications
 *
 * @package   BASE
 * @author    BASE Dev Team
 * @copyright Copyright (c) 2011 - 2018, BASE Dev Team
 * @license   http://opensource.org/licenses/MIT The MIT License
  * 
 * @since     Version 1.0
 */

/**
 * BF Form Validation language file (English)
 *
 * Localization strings used by Bonfire
 *
 * @package Bonfire\Application\Language\bf_form_validation
 * @author  BASE Dev Team
 * /docs/developer
 */

$lang['form_validation_is_array']    		= 'The {field} field must be an array.';

$lang['form_validation_bf_email']            	= 'Email';
$lang['form_validation_bf_language']         	= 'Language';
$lang['form_validation_bf_username']         	= 'Username';
$lang['form_validation_bf_password']         	= 'Password';
$lang['form_validation_bf_password_confirm'] 	= 'Password (again)';
$lang['form_validation_is_decimal'] 		= 'Please enter proper decimal value';

