<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['hproviders_manage']      = 'Gerenciar hproviders';
$lang['hproviders_edit']        = 'Editar';
$lang['hproviders_true']        = 'Verdadeiro';
$lang['hproviders_false']       = 'Falso';
$lang['hproviders_create']      = 'Criar';
$lang['hproviders_list']        = 'Listar';
$lang['hproviders_new']       = 'Novo';
$lang['hproviders_edit_text']     = 'Edite isto conforme sua necessidade';
$lang['hproviders_no_records']    = 'Não há hproviders no sistema.';
$lang['hproviders_create_new']    = 'Criar novo(a) hproviders.';
$lang['hproviders_create_success']  = 'hproviders Criado(a) com sucesso.';
$lang['hproviders_create_failure']  = 'Ocorreu um problema criando o(a) hproviders: ';
$lang['hproviders_create_new_button'] = 'Criar novo(a) hproviders';
$lang['hproviders_invalid_id']    = 'ID de hproviders inválida.';
$lang['hproviders_edit_success']    = 'hproviders salvo(a) com sucesso.';
$lang['hproviders_edit_failure']    = 'Ocorreu um problema salvando o(a) hproviders: ';
$lang['hproviders_delete_success']  = 'Registro(s) excluído(s) com sucesso.';
$lang['hproviders_delete_failure']  = 'Não foi possível excluir o registro: ';
$lang['hproviders_delete_error']    = 'Voc6e não selecionou nenhum registro para excluir.';
$lang['hproviders_actions']     = 'Ações';
$lang['hproviders_cancel']      = 'Cancelar';
$lang['hproviders_delete_record']   = 'Excluir este(a) hproviders';
$lang['hproviders_delete_confirm']  = 'Você tem certeza que deseja excluir este(a) hproviders?';
$lang['hproviders_edit_heading']    = 'Editar hproviders';

// Create/Edit Buttons
$lang['hproviders_action_edit']   = 'Salvar hproviders';
$lang['hproviders_action_create']   = 'Criar hproviders';

// Activities
$lang['hproviders_act_create_record'] = 'Criado registro com ID';
$lang['hproviders_act_edit_record'] = 'Atualizado registro com ID';
$lang['hproviders_act_delete_record'] = 'Excluído registro com ID';

//Listing Specifics
$lang['hproviders_records_empty']    = 'Nenhum registro encontrado.';
$lang['hproviders_errors_message']    = 'Por favor corrija os erros a seguir:';

// Column Headings
$lang['hproviders_column_created']  = 'Criado';
$lang['hproviders_column_deleted']  = 'Excluído';
$lang['hproviders_column_modified'] = 'Atualizado';

// Module Details
$lang['hproviders_module_name'] = 'hproviders';
$lang['hproviders_module_description'] = 'manage Hosting providers';
$lang['hproviders_area_title'] = 'hproviders';

// Fields
$lang['hproviders_field_name'] = 'Name';
$lang['hproviders_field_website_url'] = 'Website URL';
$lang['hproviders_field_desc'] = 'Description';
