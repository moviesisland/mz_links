<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Cms controller
 */
class Cms extends Front_Controller
{
    protected $permissionCreate = 'Cms.Cms.Create';
    protected $permissionDelete = 'Cms.Cms.Delete';
    protected $permissionEdit   = 'Cms.Cms.Edit';
    protected $permissionView   = 'Cms.Cms.View';

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('cms/cms_model');
        $this->lang->load('cms');
        
        

        Assets::add_module_js('cms', 'cms.js');
    }

    /**
     * Display a list of CMS data.
     *
     * @return void
     */
    public function index($offset = 0)
    {
        
        $pagerUriSegment = 3;
        $pagerBaseUrl = site_url('cms/index') . '/';
        
        $limit  = $this->settings_lib->item('site.list_limit') ?: 15;

        $this->load->library('pagination');
        $this->pager['base_url']    = $pagerBaseUrl;
        $this->pager['total_rows']  = $this->cms_model->count_all();
        $this->pager['per_page']    = $limit;
        $this->pager['uri_segment'] = $pagerUriSegment;

        $this->pagination->initialize($this->pager);
        $this->cms_model->limit($limit, $offset);
        

        // Don't display soft-deleted records
        $this->cms_model->where($this->cms_model->get_deleted_field(), 0);
        $records = $this->cms_model->find_all();

        Template::set('records', $records);
        

        Template::render();
    }

    /**
     * CMS page Render
     * @param  integer $id Page ID
     * @return Void
     */
    public function page($id)
    {
        // Initializing empty array
        $records = array();

        // Get record based on Id
        $this->cms_model->where($this->cms_model->get_deleted_field(), 0);
        $records = $this->cms_model->find($id);
        // pre($records);

        // Set into View
        Template::set('data', $records);

        // Render View
        Template::render();   
    }
    
}