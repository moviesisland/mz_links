<?php defined('BASEPATH') || exit('No direct script access allowed');
/**
 * Bonfire
 *
 * An open source project to allow developers to jumpstart their development of
 * CodeIgniter applications
 *
 * @package   Bonfire
 * @author    Bonfire Dev Team
 * @copyright Copyright (c) 2011 - 2018, Bonfire Dev Team
 * @license   http://opensource.org/licenses/MIT The MIT License
 * @link      http://cibonfire.com
 * @since     Version 1.0
 */

/**
 * Users language file (English).
 *
 * Localization strings used by Bonfire.
 *
 * @package Bonfire\Modules\Users\Language\English
 * @author  Bonfire Dev Team
 * @link    http://cibonfire.com/docs/developer
 */

$lang['us_loign_sucess']			= 'Login Sucess';
$lang['us_password_updated']			= 'Password Updated';
$lang['us_invalid_user_name']			= 'Invalid User Name or User Does not exit';


$lang['us_user_name_exit']			= 'This Mobile/User Name exist in our system, please enter some other Mobile/User name';

$lang['us_general_something_issue']			= 'Something Went Wrong Please try again';

$lang['us_general_sucess']			= 'Sucess';
$lang['us_general_fail']			= 'Fail';


$lang['us_otp_sucess']			= 'OTP Sucess';
$lang['us_otp_fail']			= 'OTP is Wrong';

$lang['us_user_not_exit']			= 'User Does not exit';

