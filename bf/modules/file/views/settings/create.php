<?php

if (validation_errors()) :
?>
<div class='alert alert-block alert-error fade in'>
    <a class='close' data-dismiss='alert'>&times;</a>
    <h4 class='alert-heading'>
        <?php echo lang('file_errors_message'); ?>
    </h4>
    <?php echo validation_errors(); ?>
</div>
<?php
endif;

$id = isset($file->id) ? $file->id : '';
$currentMethod = $this->router->method;

?>
<div class='admin-box'>

    <?php if ($currentMethod == 'edit'): ?>
        <div class="form-group">
                <?php echo form_label('FULL URL', 'slug' ); ?>
            <div class="input-group">
                <input type="text" class='form-control' id="copyFullUrl" value="<?php echo set_value('slug', isset($file->slug) ? base_url( 'file/' . $file->slug) : ''); ?>" placeholer="get File URL" readonly=''>
                <div class="input-group-addon" onClick="copyText()">
                    <i class="fa fa-copy"></i>
                </div>
            </div>
        </div>

    <?php endif ?>


    <?php echo form_open($this->uri->uri_string()); ?>
        

            <div class="form-group <?php echo form_error('name') ? ' error' : ''; ?>">
                <?php echo form_label(lang('file_field_name') . lang('bf_form_label_required'), 'name' ); ?>
                <input id='name' type='text' required='required' name='name' maxlength='255' value="<?php echo set_value('name', isset($file->name) ? $file->name : ''); ?>" class='form-control' autocomplete="off" />
                <span class='help-inline'><?php echo form_error('name'); ?></span>
            </div>

            <div class="form-group <?php echo form_error('size') ? ' error' : ''; ?>">
                <?php echo form_label(lang('file_field_size') . lang('bf_form_label_required'), 'size' ); ?>
                <input id='size' type='text' required='required' name='size' maxlength='100' value="<?php echo set_value('size', isset($file->size) ? $file->size : ''); ?>" class='form-control' autocomplete="off" />
                <span class='help-inline'><?php echo form_error('size'); ?></span>
            </div>

            

            <?php if ($currentMethod == 'create1'): ?>
                <div class="form-group <?php echo form_error('slug') ? ' error' : ''; ?>">
                    <?php echo form_label(lang('file_field_slug'), 'slug' ); ?>
                    <input id='slug' type='text' name='slug' maxlength='100' value="<?php echo set_value('slug', isset($file->slug) ? $file->slug : ''); ?>" class='form-control' autocomplete="off" />
                    <span class='help-inline'><?php echo form_error('slug'); ?></span>
                </div>
            <?php endif ?>
        
        <div class="box box-success box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Hosing Providers</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <!-- <div class="container"> -->
                    <?php /*pr($links_data);*/ foreach ($Hproviders as $k => $hp): ?>
                        
                        <div class="form-group">
                            <label for="direct_link"><?= $hp->name ?></label>
                            <input type='text' name='download_link[<?= $hp->id ?>]' value="<?php echo set_value('download_link', isset($links_data[$k]->link) ? $links_data[$k]->link : ''); ?>" class='form-control' />
                            <span class='help-inline'><?php echo form_error('download_link'); ?></span>
                        </div>

                    <?php endforeach ?>
                <!-- </div> -->
            </div>
        </div>

        <div class='form-group' style="    text-align: center;">
            <?php $save_btnName = ($currentMethod == 'edit') ? lang('file_action_edit')  : lang('file_action_create'); ?>
            <input type='submit' name='save' class='btn btn-primary' value="<?php echo $save_btnName; ?>" />
            <?php echo lang('bf_or'); ?>
            <?php echo anchor(SITE_AREA . '/settings/file', lang('file_cancel'), 'class="btn btn-default"'); ?>
            
        </div>

    <?php echo form_close(); ?>
</div>

<script type="text/javascript">
function copyText() {
    /* Get the text field */
      var copyText = document.getElementById("copyFullUrl");

      /* Select the text field */
      copyText.select();
      copyText.setSelectionRange(0, 99999); /*For mobile devices*/

      /* Copy the text inside the text field */
      document.execCommand("copy");

      /* Alert the copied text */
      // alert("Copied the text: " + copyText.value);
}
</script>