<?php

if (validation_errors()) :
?>
<div class='alert alert-block alert-error fade in'>
    <a class='close' data-dismiss='alert'>&times;</a>
    <h4 class='alert-heading'>
        <?php echo lang('give_errors_message'); ?>
    </h4>
    <?php echo validation_errors(); ?>
</div>
<?php
endif;

$id = isset($give->id) ? $give->id : '';
$currentMethod = $this->router->method;

?>
<div class='admin-box'>
    <?php echo form_open($this->uri->uri_string()); ?>
        

            <div class="form-group <?php echo form_error('user_id') ? ' error' : ''; ?>">
                <?php echo form_label(lang('give_field_user_id'), 'user_id' ); ?>
                <input id='user_id' type='text' name='user_id'  value="<?php echo set_value('user_id', isset($give->user_id) ? $give->user_id : ''); ?>" class='form-control' />
                <span class='help-inline'><?php echo form_error('user_id'); ?></span>
            </div>

            <div class="form-group <?php echo form_error('email') ? ' error' : ''; ?>">
                <?php echo form_label(lang('give_field_email'), 'email' ); ?>
                <input id='email' type='text' name='email' maxlength='255' value="<?php echo set_value('email', isset($give->email) ? $give->email : ''); ?>" class='form-control' />
                <span class='help-inline'><?php echo form_error('email'); ?></span>
            </div>

            <div class="form-group <?php echo form_error('amount') ? ' error' : ''; ?>">
                <?php echo form_label(lang('give_field_amount'), 'amount' ); ?>
                <input id='amount' type='text' name='amount' maxlength='20' value="<?php echo set_value('amount', isset($give->amount) ? $give->amount : ''); ?>" class='form-control' />
                <span class='help-inline'><?php echo form_error('amount'); ?></span>
            </div>

            <div class="form-group <?php echo form_error('status') ? ' error' : ''; ?>">
                <?php echo form_label(lang('give_field_status'), 'status' ); ?>
                <input id='status' type='text' name='status' maxlength='1' value="<?php echo set_value('status', isset($give->status) ? $give->status : ''); ?>" class='form-control' />
                <span class='help-inline'><?php echo form_error('status'); ?></span>
            </div>

            <div class="form-group <?php echo form_error('donation_type') ? ' error' : ''; ?>">
                <?php echo form_label(lang('give_field_donation_type'), 'donation_type' ); ?>
                <input id='donation_type' type='text' name='donation_type' maxlength='1' value="<?php echo set_value('donation_type', isset($give->donation_type) ? $give->donation_type : ''); ?>" class='form-control' />
                <span class='help-inline'><?php echo form_error('donation_type'); ?></span>
            </div>

            <div class="form-group <?php echo form_error('charge_created_at') ? ' error' : ''; ?>">
                <?php echo form_label(lang('give_field_charge_created_at'), 'charge_created_at' ); ?>
                <input id='charge_created_at' type='text' name='charge_created_at'  value="<?php echo set_value('charge_created_at', isset($give->charge_created_at) ? $give->charge_created_at : ''); ?>" class='form-control' />
                <span class='help-inline'><?php echo form_error('charge_created_at'); ?></span>
            </div>

            <div class="form-group <?php echo form_error('stripe_customer_id') ? ' error' : ''; ?>">
                <?php echo form_label(lang('give_field_stripe_customer_id'), 'stripe_customer_id' ); ?>
                <input id='stripe_customer_id' type='text' name='stripe_customer_id' maxlength='255' value="<?php echo set_value('stripe_customer_id', isset($give->stripe_customer_id) ? $give->stripe_customer_id : ''); ?>" class='form-control' />
                <span class='help-inline'><?php echo form_error('stripe_customer_id'); ?></span>
            </div>

            <div class="form-group <?php echo form_error('stripe_charge_id') ? ' error' : ''; ?>">
                <?php echo form_label(lang('give_field_stripe_charge_id'), 'stripe_charge_id' ); ?>
                <input id='stripe_charge_id' type='text' name='stripe_charge_id' maxlength='255' value="<?php echo set_value('stripe_charge_id', isset($give->stripe_charge_id) ? $give->stripe_charge_id : ''); ?>" class='form-control' />
                <span class='help-inline'><?php echo form_error('stripe_charge_id'); ?></span>
            </div>

            <div class="form-group <?php echo form_error('stripe_balance_transaction_id') ? ' error' : ''; ?>">
                <?php echo form_label(lang('give_field_stripe_balance_transaction_id'), 'stripe_balance_transaction_id' ); ?>
                <input id='stripe_balance_transaction_id' type='text' name='stripe_balance_transaction_id' maxlength='255' value="<?php echo set_value('stripe_balance_transaction_id', isset($give->stripe_balance_transaction_id) ? $give->stripe_balance_transaction_id : ''); ?>" class='form-control' />
                <span class='help-inline'><?php echo form_error('stripe_balance_transaction_id'); ?></span>
            </div>

            <div class="form-group <?php echo form_error('stripe_subscription_id') ? ' error' : ''; ?>">
                <?php echo form_label(lang('give_field_stripe_subscription_id'), 'stripe_subscription_id' ); ?>
                <input id='stripe_subscription_id' type='text' name='stripe_subscription_id' maxlength='255' value="<?php echo set_value('stripe_subscription_id', isset($give->stripe_subscription_id) ? $give->stripe_subscription_id : ''); ?>" class='form-control' />
                <span class='help-inline'><?php echo form_error('stripe_subscription_id'); ?></span>
            </div>
        <div class='form-group'>
            <?php $save_btnName = ($currentMethod == 'edit') ? lang('give_action_edit')  : lang('give_action_create'); ?>
            <input type='submit' name='save' class='btn btn-primary' value="<?php echo $save_btnName; ?>" />
            <?php echo lang('bf_or'); ?>
            <?php echo anchor(SITE_AREA . '/settings/give', lang('give_cancel'), 'class="btn btn-default"'); ?>
            
            <?php if ($this->auth->has_permission('Give.Settings.Delete')) : ?>
                <?php echo lang('bf_or'); ?>
                <button type='submit' name='delete' formnovalidate class='btn btn-danger' id='delete-me' onclick="return confirm('<?php e(js_escape(lang('give_delete_confirm'))); ?>');">
                    <span class='icon-trash icon-white'></span>&nbsp;<?php echo lang('give_delete_record'); ?>
                </button>
            <?php endif; ?>
        </div>
    <?php echo form_close(); ?>
</div>