<!DOCTYPE html>
<html lang='en' class="h-100">
<head>
    <meta charset="UTF-8">
    <title><?php
        echo isset($page_title) ? "{$page_title} : " : '';
        e(class_exists('Settings_lib') ? settings_item('site.title') : 'Bonfire');
        ?></title>
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php e(isset($meta_description) ? $meta_description : ''); ?>">
    <meta name="author" content="<?php e(isset($meta_author) ? $meta_author : ''); ?>">
    <link rel="shortcut icon" href="<?= base_url('favicon.ico'); ?>">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url('themes/default/css/bootstrap.min.css'); ?>">

    <!-- Font Awsome  -->
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/fonts/css/all.min.css'); ?>">

    <!-- Google fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">

	<style type="text/css">
		main > .container {
            padding: 6rem 2rem 0;
		}
        main > .container .home-title {
            padding: 1rem 2rem;
        }
		body {
			background-color: #f0ede9;
			font-family: 'Poppins', sans-serif;
		}
        #main ul{
            list-style: none;
        }
        .nav-item > a { 
            color: #FFF;
            text-decoration: none;
        }
        footer {
            background-color: #ccc;
            width: 100%;
        }
	</style>

</head>
<body class="d-flex flex-column h-100">
        