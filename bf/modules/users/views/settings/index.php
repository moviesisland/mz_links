
<?php echo form_open($this->uri->uri_string(), array('autocomplete' => 'off')); ?>
<div class="box-body user-table-search">
	<div class="row">
		<div class="col-xs-12">
			<h3 class="box-title">
				<?php echo lang('bf_action_search')?>
				<?php if (isset($isFilterSet) && $isFilterSet == 1): ?>
					<span class="badge bg-green"> </span>
				<?php else: ?>
					<span class="badge bg-info"> </span>
				<?php endif ?> 
			</h3>
		</div>
	</div>
	<div class="row">

        <!-- First Name Section -->
        <div class="col-lg-3 col-md-3 col-sm-3 col-sx-3 col-3">
        	<label><?php echo lang('bf_display_name');?></label>
        	<?php

        		$d_nameData = array(
			        'name'  => 'display_name',
			        'id'    => 'display_name',
			        'value' =>	isset($display_name) ? $display_name : '',
			        'class' => 'form-control'
				);

				echo form_input($d_nameData);
			?>
		</div>

        <!-- Last name Section -->
        <div class="col-lg-3 col-md-3 col-sm-3 col-sx-3 col-3">
        	<label><?php echo lang('bf_last_name');?></label>
        	<?php

        		$lastNameData = array(
			        'name'  => 'last_name',
			        'id'    => 'last_name',
			        'value' =>	isset($last_name) ? $last_name : '',
			        'class' => 'form-control'
				);

				echo form_input($lastNameData);
			?>
        </div>

        <!-- Email Section -->
        <div class="col-lg-6 col-md-6 col-sm-6 col-sx-6 col-6">
          	<label><?php echo lang('bf_email');?></label>
          	<?php 

	        	$lastNameData = array(
			        'name'  => 'email',
			        'id'    => 'email',
			        'value' =>	isset($email) ? $email : '',
			        'class' => 'form-control'
				);

				echo form_input($lastNameData);
			?>
        </div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<?php

			echo form_submit('search', lang('bf_action_search') , array('class' => 'btn btn-primary'));
			echo form_submit('reset', lang('bf_reset') , array('class' => 'btn btn-primary ml-10'));

			?>
		</div>
	</div>
</div>
<?php echo form_close(); ?>

<ul class="nav nav-tabs">
	<li <?php echo $filter_type == 'all' ? ' class="active"' : ''; ?> >
		<?php echo anchor($index_url, lang('us_tab_all')); ?>
	</li>
    <li <?php echo $filter_type == 'banned' ? ' class="active"' : ''; ?> >
    	<?php echo anchor("{$index_url}banned/", lang('us_tab_banned')); ?>
	</li>
	<li class="<?php echo $filter_type == 'role_id' ? 'active ' : ''; ?>dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown">
			<?php
			echo lang('us_tab_roles');
            echo isset($filter_role) ? ": {$filter_role}" : '';
			?>
			<span class="caret light-caret"></span>
		</a>
		<ul class="dropdown-menu">
			<?php foreach ($roles as $role) : ?>
            <li><?php echo anchor("{$index_url}role_id-{$role->role_id}", $role->role_name); ?></li>
			<?php endforeach; ?>
		</ul>
	</li>
</ul>
<?php if (empty($users) || ! is_array($users)) : ?>
<div class="callout callout-info mt-20">
	<?php echo lang('us_no_users'); ?>
</div>
<?php
else :
    $numColumns = 8;
    // echo form_open();
?>
	<div class="table-responsive" id="userTable">
		<table class="table table-striped user-table">
			<thead>
				<tr>
					<th class="column-check"><input class="check-all" type="checkbox" /></th>
					<th class='id'><?php echo lang('bf_id'); ?></th>
					<th><?php echo lang('bf_display_name'); ?></th>
					<th><?php echo lang('bf_last_name'); ?></th>
					<th><?php echo lang('bf_email'); ?></th>
					<th><?php echo lang('us_role'); ?></th>
					<th class='last-login'><?php echo lang('us_last_login'); ?></th>
					<th class='status'><?php echo lang('us_status'); ?></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
	                <td colspan="<?php echo $numColumns; ?>">
						<?php echo lang('bf_with_selected'); ?>
						<input type="submit" name="ban" class="btn btn-primary" value="<?php echo lang('bf_action_ban'); ?>" />
						<input type="submit" name="delete" class="btn btn-danger" id="delete-me" value="<?php echo lang('bf_action_delete'); ?>" onclick="return confirm('<?php e(js_escape(lang('us_delete_account_confirm'))); ?>')" />
					</td>
				</tr>
			</tfoot>
			<tbody>
				
	            <?php foreach ($users as $user) : ?>
				<tr>
					<td class="column-check"><input type="checkbox" name="checked[]" value="<?php echo $user->id; ?>" /></td>
					<td><?php echo $user->id; ?></td>
					<td class='id'><?php
	                    echo anchor(site_url(SITE_AREA . "/settings/users/edit/{$user->id}"), $user->display_name);

						if ($user->banned) :
						?>
	                    <span class="label label-warning"><?php echo lang('us_tab_banned'); ?></span>
						<?php endif; ?>
					</td>
					<td><?php e($user->last_name); ?></td>
					<td><?php echo ($user->email ? mailto($user->email) : ''); ?></td>
					<td><?php e($roles[$user->role_id]->role_name); ?></td>
					<td class='last-login'>
						<?php e($user->last_login != '0000-00-00 00:00:00' ? user_time_custom(strtotime($user->last_login), DATE_FORMAT) : '---'); ?></td>
					<td class='status'>
						<?php if ($user->active) : ?>
							<span class="label label-success"><?php echo lang('us_active'); ?></span>
						<?php else : ?>
							<span class="label label-warning"><?php echo lang('us_inactive'); ?></span>
						<?php endif; ?>
					</td>
				</tr>
	            <?php endforeach; ?>
			</tbody>
		</table>
	</div>
<?php
    echo form_close();
    echo $this->pagination->create_links();
endif;
