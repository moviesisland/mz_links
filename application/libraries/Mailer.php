<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('PHPMailer/class.phpmailer.php');

class Mailer extends PHPMailer
{

    var $mail;
    var $errmsg;
	var $is_local = false;
    public function __construct()
    {			
        // the true param means it will throw exceptions on errors, which we need to catch
        $this->mail = new PHPMailer(true);
        
        $this->mail->IsSMTP(); // telling the class to use SMTP

        $this->mail->CharSet = "utf-8";                  // ????? CharSet ????????
        $this->mail->SMTPDebug  = 0;                     // enables SMTP debug information
        $this->mail->mailtype = "html";
        $this->mail->SMTPAuth   = true;                  // enable SMTP authentication  
        //$this->mail->SMTPSecure = SMTP_SECURE;              // sets the prefix to the servier  
        $this->mail->isHTML(true); 
        
        $this->mail->Host       = SMTP_HOST;      // sets GMAIL as the SMTP server    
        $this->mail->Port       = SMTP_PORT;                   // set the SMTP port for the GMAIL server 
        $this->mail->Username   = SMTP_USERNAME;// GMAIL username        
        $this->mail->Password   = SMTP_PASSWORD;       // GMAIL password   
        $this->mail->AddReplyTo(FROM_EMAIL, FROM_NAME);
        $this->mail->SetFrom(FROM_EMAIL, FROM_NAME);
         
    }

    public function sendmail($to, $to_name, $subject, $body){
        try{
            $this->mail->AddAddress($to, $to_name);
            $this->mail->Subject = $subject;
            $this->mail->Body    = $body;
            $this->mail->Send();
            $this->mail->ClearAddresses();
            return true;
        } catch (phpmailerException $e) {
            $this->mail->ClearAddresses();
            $this->errmsg = $e->errorMessage(); //Pretty error messages from PHPMailer

            return false;
        } catch (Exception $e) {
            $this->mail->ClearAddresses();
            $this->errmsg = $e->getMessage(); //Boring error messages from anything else!
            return false;
        }
    }
    
    public function getErr(){
        return $this->errmsg;
    }
    
    public function sendhtmlmail($to, $to_name, $subject, $body){
        $this->mail->IsHTML(true);
        return $this->sendmail($to, $to_name, $subject, $body);
    }
}
 
/* End of file mailer.php */