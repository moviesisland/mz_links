<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Connect controller
 */
class Connect extends Front_Controller
{
    protected $permissionCreate = 'Connect.Connect.Create';
    protected $permissionDelete = 'Connect.Connect.Delete';
    protected $permissionEdit   = 'Connect.Connect.Edit';
    protected $permissionView   = 'Connect.Connect.View';

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('connect/connect_model');
        $this->lang->load('connect');
        
        

        Assets::add_module_js('connect', 'connect.js');
    }

    /**
     * Display a list of connect data.
     *
     * @return void
     */
    public function index($offset = 0)
    {
        
        $pagerUriSegment = 3;
        $pagerBaseUrl = site_url('connect/index') . '/';
        
        $limit  = $this->settings_lib->item('site.list_limit') ?: 15;

        $this->load->library('pagination');
        $this->pager['base_url']    = $pagerBaseUrl;
        $this->pager['total_rows']  = $this->connect_model->count_all();
        $this->pager['per_page']    = $limit;
        $this->pager['uri_segment'] = $pagerUriSegment;

        $this->pagination->initialize($this->pager);
        $this->connect_model->limit($limit, $offset);
        

        // Don't display soft-deleted records
        $this->connect_model->where($this->connect_model->get_deleted_field(), 0);
        $records = $this->connect_model->find_all();

        Template::set('records', $records);
        

        Template::render();
    }
    
}