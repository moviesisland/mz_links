<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['give_manage']      = 'Gerenciar give';
$lang['give_edit']        = 'Editar';
$lang['give_true']        = 'Verdadeiro';
$lang['give_false']       = 'Falso';
$lang['give_create']      = 'Criar';
$lang['give_list']        = 'Listar';
$lang['give_new']       = 'Novo';
$lang['give_edit_text']     = 'Edite isto conforme sua necessidade';
$lang['give_no_records']    = 'Não há give no sistema.';
$lang['give_create_new']    = 'Criar novo(a) give.';
$lang['give_create_success']  = 'give Criado(a) com sucesso.';
$lang['give_create_failure']  = 'Ocorreu um problema criando o(a) give: ';
$lang['give_create_new_button'] = 'Criar novo(a) give';
$lang['give_invalid_id']    = 'ID de give inválida.';
$lang['give_edit_success']    = 'give salvo(a) com sucesso.';
$lang['give_edit_failure']    = 'Ocorreu um problema salvando o(a) give: ';
$lang['give_delete_success']  = 'Registro(s) excluído(s) com sucesso.';
$lang['give_delete_failure']  = 'Não foi possível excluir o registro: ';
$lang['give_delete_error']    = 'Voc6e não selecionou nenhum registro para excluir.';
$lang['give_actions']     = 'Ações';
$lang['give_cancel']      = 'Cancelar';
$lang['give_delete_record']   = 'Excluir este(a) give';
$lang['give_delete_confirm']  = 'Você tem certeza que deseja excluir este(a) give?';
$lang['give_edit_heading']    = 'Editar give';

// Create/Edit Buttons
$lang['give_action_edit']   = 'Salvar give';
$lang['give_action_create']   = 'Criar give';

// Activities
$lang['give_act_create_record'] = 'Criado registro com ID';
$lang['give_act_edit_record'] = 'Atualizado registro com ID';
$lang['give_act_delete_record'] = 'Excluído registro com ID';

//Listing Specifics
$lang['give_records_empty']    = 'Nenhum registro encontrado.';
$lang['give_errors_message']    = 'Por favor corrija os erros a seguir:';

// Column Headings
$lang['give_column_created']  = 'Criado';
$lang['give_column_deleted']  = 'Excluído';
$lang['give_column_modified'] = 'Atualizado';

// Module Details
$lang['give_module_name'] = 'give';
$lang['give_module_description'] = 'Donation Module';
$lang['give_area_title'] = 'give';

// Fields
$lang['give_field_user_id'] = 'fcc account ID';
$lang['give_field_email'] = 'Email';
$lang['give_field_amount'] = 'Amount';
$lang['give_field_status'] = 'Status';
$lang['give_field_donation_type'] = 'Donation type';
$lang['give_field_charge_created_at'] = 'Created at';
$lang['give_field_stripe_customer_id'] = 'Stripe Customer ID';
$lang['give_field_stripe_charge_id'] = 'Stripe Charge ID';
$lang['give_field_stripe_balance_transaction_id'] = 'Stripe Balance Transaction ID';
$lang['give_field_stripe_subscription_id'] = 'Stripe  Subscription ID';
