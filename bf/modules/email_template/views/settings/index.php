<?php

$num_columns	= 7;
$can_delete	= $this->auth->has_permission('Email_template.Settings.Delete');
$can_edit		= $this->auth->has_permission('Email_template.Settings.Edit');
$has_records	= isset($records) && is_array($records) && count($records);

if ($can_delete) {
    $num_columns++;
}
?>
<div class='admin-box'>
	<?php echo form_open($this->uri->uri_string()); ?>
		<table class='table table-striped'>
			<thead>
				<tr>
					<?php if ($can_delete && $has_records) : ?>
					<th class='column-check'><input class='check-all' type='checkbox' /></th>
					<?php endif;?>
					
					<th><?php echo lang('email_template_field_template_name'); ?></th>
					<th><?php echo lang('email_template_field_template_subject'); ?></th>
					<th><?php echo lang('email_template_field_template_description'); ?></th>
					<th><?php echo lang('email_template_field_status'); ?></th>
					<th><?php echo lang('email_template_column_created'); ?></th>
					<th><?php echo lang('email_template_column_modified'); ?></th>
				</tr>
			</thead>
			<?php if ($has_records) : ?>
			<tfoot>
				<?php if ($can_delete) : ?>
				<tr>
					<td colspan='<?php echo $num_columns; ?>'>
						<?php echo lang('bf_with_selected'); ?>
						<input type='submit' name='delete' id='delete-me' class='btn btn-danger' value="<?php echo lang('bf_action_delete'); ?>" onclick="return confirm('<?php e(js_escape(lang('email_template_delete_confirm'))); ?>')" />
					</td>
				</tr>
				<?php endif; ?>
			</tfoot>
			<?php endif; ?>
			<tbody>
				<?php
				if ($has_records) :
					foreach ($records as $record) :
				?>
				<tr>
					<?php if ($can_delete) : ?>
					<td class='column-check'><input type='checkbox' name='checked[]' value='<?php echo $record->id; ?>' /></td>
					<?php endif;?>
					
				<?php if ($can_edit) : ?>
					<td><?php echo anchor(SITE_AREA . '/settings/email_template/edit/' . $record->id, '<span class="icon-pencil"></span> ' .  $record->template_name); ?></td>
				<?php else : ?>
					<td><?php e($record->template_name); ?></td>
				<?php endif; ?>
					<td><?php e($record->template_subject); ?></td>
					<td><?php e($record->template_description); ?></td>
					<td>
						<?php $className = ($record->status == 1) ? 'success' : 'danger'; ?>
						<span class="label label-<?php echo $className;?>">
							<?php e(($record->status == 1) ? lang('bf_status_active') : lang('bf_status_inactive')); ?>
						</span>
					</td>
					<td><?php e(date(DATE_FORMAT,strtotime($record->created_on))); ?></td>
					<td><?php e(date(DATE_FORMAT,strtotime($record->modified_on))); ?></td>
				</tr>
				<?php
					endforeach;
				else:
				?>
				<tr>
					<td colspan='<?php echo $num_columns; ?>'><?php echo lang('email_template_records_empty'); ?></td>
				</tr>
				<?php endif; ?>
			</tbody>
		</table>
	<?php
    echo form_close();
    
    echo $this->pagination->create_links();
    ?>
</div>