<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['hproviders_manage']      = 'Gestionar hproviders';
$lang['hproviders_edit']        = 'Editar';
$lang['hproviders_true']        = 'Verdadero';
$lang['hproviders_false']       = 'Falso';
$lang['hproviders_create']      = 'Crear';
$lang['hproviders_list']        = 'Listar';
$lang['hproviders_new']       = 'Nuevo';
$lang['hproviders_edit_text']     = 'Editar esto para satisfacer sus necesidades';
$lang['hproviders_no_records']    = 'Hay ninguna hproviders en la sistema.';
$lang['hproviders_create_new']    = 'Crear nuevo(a) hproviders.';
$lang['hproviders_create_success']  = 'hproviders creado(a) con éxito.';
$lang['hproviders_create_failure']  = 'Hubo un problema al crear el(la) hproviders: ';
$lang['hproviders_create_new_button'] = 'Crear nuevo(a) hproviders';
$lang['hproviders_invalid_id']    = 'ID de hproviders inválido(a).';
$lang['hproviders_edit_success']    = 'hproviders guardado correctamente.';
$lang['hproviders_edit_failure']    = 'Hubo un problema guardando el(la) hproviders: ';
$lang['hproviders_delete_success']  = 'Registro(s) eliminado con éxito.';
$lang['hproviders_delete_failure']  = 'No hemos podido eliminar el registro: ';
$lang['hproviders_delete_error']    = 'No ha seleccionado ning&#250;n registro que desea eliminar.';
$lang['hproviders_actions']     = 'Açciones';
$lang['hproviders_cancel']      = 'Cancelar';
$lang['hproviders_delete_record']   = 'Eliminar este(a) hproviders';
$lang['hproviders_delete_confirm']  = '¿Esta seguro de que desea eliminar este(a) hproviders?';
$lang['hproviders_edit_heading']    = 'Editar hproviders';

// Create/Edit Buttons
$lang['hproviders_action_edit']   = 'Guardar hproviders';
$lang['hproviders_action_create']   = 'Crear hproviders';

// Activities
$lang['hproviders_act_create_record'] = 'Creado registro con ID';
$lang['hproviders_act_edit_record'] = 'Actualizado registro con ID';
$lang['hproviders_act_delete_record'] = 'Eliminado registro con ID';

//Listing Specifics
$lang['hproviders_records_empty']    = 'No hay registros encontrados para su selección.';
$lang['hproviders_errors_message']    = 'Por favor corrija los siguientes errores:';

// Column Headings
$lang['hproviders_column_created']  = 'Creado';
$lang['hproviders_column_deleted']  = 'Elíminado';
$lang['hproviders_column_modified'] = 'Modificado';

// Module Details
$lang['hproviders_module_name'] = 'hproviders';
$lang['hproviders_module_description'] = 'manage Hosting providers';
$lang['hproviders_area_title'] = 'hproviders';

// Fields
$lang['hproviders_field_name'] = 'Name';
$lang['hproviders_field_website_url'] = 'Website URL';
$lang['hproviders_field_desc'] = 'Description';
