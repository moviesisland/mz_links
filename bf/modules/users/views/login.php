<?php if (validation_errors()) : ?>
		<div class="alert alert-danger" role="alert"><?= validation_errors(); ?></div>
	<?php endif; ?>

<?php echo form_open(LOGIN_URL, array('autocomplete' => 'off', 'class' => 'form-signin')); ?>
	<img class="mb-4" src="<?= base_url('assets\images\mz_link_logo.png'); ?>" alt="" width="300" height="80">
	<!-- <h1 class="h3 mb-3 font-weight-normal"><?php echo lang('us_login'); ?></h1> -->

	<?php echo Template::message(); ?>

	<label class="sr-only">Email address</label>
	<input type="text" name="login" id="login_value" class="form-control" value="<?php echo set_value('login'); ?>" placeholder="<?php echo $this->settings_lib->item('auth.login_type') == 'both' ? lang('bf_username') .'/'. lang('bf_email') : ucwords($this->settings_lib->item('auth.login_type')) ?>" required="" autofocus="" />

	<label for="password" class="sr-only">Password</label>
	<input type="password" name="password" id="password" class="form-control" placeholder="<?php echo lang('bf_password'); ?>" required="">
	
	<?php if ($this->settings_lib->item('auth.allow_remember')) : ?>
		<div class="checkbox mb-3">
			<label>
				<input type="checkbox" name="remember_me" id="remember_me" value="1" />
				<span class="inline-help"><?php echo lang('us_remember_note'); ?></span>
		    </label>
		</div>
	<?php endif; ?>
  	<input class="btn btn-primary btn-block" type="submit" name="log-me-in" id="submit" value="<?php e(lang('us_let_me_in')); ?>" />

<?php echo form_close(); ?>