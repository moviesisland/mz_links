<?php

$hiddenFields = array('id', 'deleted', 'deleted_by',);
?>
<h1 class='page-header'>
    <?php echo lang('give_area_title'); ?>
</h1>
<?php if (isset($records) && is_array($records) && count($records)) : ?>
<table class='table table-striped table-bordered'>
    <thead>
        <tr>
            
            <th>fcc account ID</th>
            <th>Email</th>
            <th>Amount</th>
            <th>Status</th>
            <th>Donation type</th>
            <th>Created at</th>
            <th>Stripe Customer ID</th>
            <th>Stripe Charge ID</th>
            <th>Stripe Balance Transaction ID</th>
            <th>Stripe  Subscription ID</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($records as $record) :
        ?>
        <tr>
            <?php
            foreach($record as $field => $value) :
                if ( ! in_array($field, $hiddenFields)) :
            ?>
            <td>
                <?php
                if ($field == 'deleted') {
                    e(($value > 0) ? lang('give_true') : lang('give_false'));
                } else {
                    e($value);
                }
                ?>
            </td>
            <?php
                endif;
            endforeach;
            ?>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php

    echo $this->pagination->create_links();
endif; ?>