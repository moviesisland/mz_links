<?php
error_reporting(E_ERROR | E_PARSE);
// get contents of composer.json
$composer_json = json_decode(file_get_contents("composer.json"), true);
?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Welcome to CI-BF</title>
        <base target="_blank">
        <link rel="stylesheet" type="text/css" href="public/themes/default/css/bootstrap.min.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="public/themes/default/css/bootstrap-responsive.min.css" media="screen" />
        <style>
            body {
                font-family:sans-serif;
            }
            #intro {
                width:700px;
                margin-left:-390px;
                position:fixed;
                left:50%;
                top:60px;
                padding:10px 30px;
            }
            h1 {
                text-align:center;
            }
            .continue {
                padding:10px 0;
                text-align:center;
            }
        </style>
    </head>
    <body>
        <div id="intro" class="well">
            <h1>Welcome to BASE v<?php echo $composer_json['version']; ?></h1>
            <p>Some things have changed since the last version, specifically pertaining to the installer. Here is the new way <strong>BASE v0.7-dev</strong> handles the installation.</p>
            <p>Before continuing:</p>
            <ol>
                <li>Create your database manually</li>
                <li>Edit your <strong>application/config/database.php</strong> file accordingly</li>
            </ol>
            <div class="alert alert-error">
                <h3>Oops!</h3>
                <p>Your Web Root should be set to the <strong>public</strong> folder, but it's <strong>not</strong>. It's pointing to the <strong>BASE Root</strong> folder.</p>
                <p>See below for an example of how your site should be set up in Apache:</p>
<pre>&lt;VirtualHost *:80&gt;
    DocumentRoot "[...]/htdocs/BASE_Root/public"
    ServerName BASE.Root
    ServerAlias BASE.Root.local
&lt;/VirtualHost&gt;</pre>
            </div>
            <p><em>"Let's make this the best kick-start to any CodeIgniter project."</em> ~ The CI-BASE Team</p>
        </div>
    </body>
</html>