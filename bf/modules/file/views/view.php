
<div class="card main-box">
	<div class="card-header">
		<h5>File Information</h5>
	</div>
    <div class="card-body">
    	<div class="container">
	    	<div class="table-responsive">
		    	<table class="table table-bordered table-hover ">
				  	<tbody>
				    	<tr>
				      		<th scope="row" class="text-center">Name</th>
				      		<td><?= $file->name; ?></td>
				      	</tr>
				      	<tr>
					    	<th scope="row" class="text-center">Size</th>
				      		<td><?= $file->size; ?></td>
				    	</tr>
					</tbody>
				</table>
	    	</div>

	    	<div class="row mt-2">
	    		<small class="mx-auto">If <strong>Direct Download</strong> link is not working you can go for the <strong>Mirror</strong> Links.</small>
	    	</div>

	    	<div class="row mt-5">
	    		<a href="<?= $d_link; ?>" class="btn btn-success mx-auto" target="_blank" rel="noopener noreferrer"><i class="fa fa-download"></i> Direct Download</a>
	    	</div>

	    	<hr>

		    <div class="d-flex justify-content-center mt-3">
		    	<?php foreach ($links_data as $lkey => $lval): ?>
		    		<?php if (!empty($lval->link)): ?>
		    			<a href="<?= $lval->link; ?>" class="btn btn-primary ml-3" target="_blank" rel="noopener noreferrer"><i class="fa fa-cloud"></i> <?php echo $lval->name; ?></a>
		    		<?php endif ?>
		    	<?php endforeach ?>
	    	</div>
    	</div>
    </div>
</div>