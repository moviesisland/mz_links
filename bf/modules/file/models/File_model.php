<?php defined('BASEPATH') || exit('No direct script access allowed');

class File_model extends BF_Model
{
    protected $table_name	= 'files';
	protected $key			= 'id';
	protected $date_format	= 'datetime';

	protected $log_user 	= true;
	protected $set_created	= true;
	protected $set_modified = true;
	protected $soft_deletes	= true;

	protected $created_field     = 'created_on';
    protected $created_by_field  = 'created_by';
	protected $modified_field    = 'modified_on';
    protected $modified_by_field = 'modified_by';
    protected $deleted_field     = 'deleted';
    protected $deleted_by_field  = 'deleted_by';

	// Customize the operations of the model without recreating the insert,
    // update, etc. methods by adding the method names to act as callbacks here.
	protected $before_insert 	= array();
	protected $after_insert 	= array();
	protected $before_update 	= array();
	protected $after_update 	= array();
	protected $before_find 	    = array();
	protected $after_find 		= array();
	protected $before_delete 	= array();
	protected $after_delete 	= array();

	// For performance reasons, you may require your model to NOT return the id
	// of the last inserted row as it is a bit of a slow method. This is
    // primarily helpful when running big loops over data.
	protected $return_insert_id = true;

	// The default type for returned row data.
	protected $return_type = 'object';

	// Items that are always removed from data prior to inserts or updates.
	protected $protected_attributes = array();

	// You may need to move certain rules (like required) into the
	// $insert_validation_rules array and out of the standard validation array.
	// That way it is only required during inserts, not updates which may only
	// be updating a portion of the data.
	protected $validation_rules 		= array(
		array(
			'field' => 'name',
			'label' => 'lang:file_field_name',
			'rules' => 'required|max_length[255]',
		),
		array(
			'field' => 'size',
			'label' => 'lang:file_field_size',
			'rules' => 'required|max_length[100]',
		),
		array(
			'field' => 'slug',
			'label' => 'lang:file_field_slug',
			'rules' => 'max_length[100]',
		),
	);
	protected $insert_validation_rules  = array();
	protected $skip_validation 			= false;

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function checkslug($new_slug) {

	    $this->db->where('slug', $new_slug);
	    $query = $this->db->get($this->table_name);
	    $count_row = $query->num_rows();
	    
	    if ($count_row > 0) {
	      //if count row return any row; that means you have already this email address in the database. so you must set false in this sense.
	        return FALSE; // here I change TRUE to false.
	     } else {
	      // doesn't return any row means database doesn't have this email
	        return TRUE; // And here false to TRUE
	     }
	}

	public function add_record_link_mapping($arr,$fId)
	{
		// Remove All Old Record realated to this link
		$this->db->query("DELETE FROM `bf_links_mapping` WHERE `bf_links_mapping`.`file_id` = " . $fId);
		// echo $this->db->last_query();
		
		foreach ($arr as $hId => $val) {
			$this->db->insert("bf_links_mapping", ['file_id' => $fId, 'host_id' => $hId, 'link' => $val]);
		}
	}

	public function get_link_mapping($fId)
	{
		$result = $this->db->query("SELECT * FROM `bf_links_mapping` WHERE `bf_links_mapping`.`file_id` = " . $fId . " ORDER BY `bf_links_mapping`.`host_id` ASC");

		return $result->result() ?? [];
	}

	public function get_only_links($fId)
	{
		$query = "SELECT `bf_links_mapping`.`file_id`, `bf_links_mapping`.`host_id`, `bf_hosting_provider`.`name` , `bf_links_mapping`.`link` FROM `bf_links_mapping` LEFT JOIN `bf_hosting_provider` on `bf_hosting_provider`.`id` = `bf_links_mapping`.`host_id` WHERE `bf_links_mapping`.`file_id` = " . $fId . "  ORDER BY `bf_links_mapping`.`host_id` ASC";

		$result = $this->db->query( $query );

		return $result->result() ?? [];
	}
}