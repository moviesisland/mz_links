<?php defined('BASEPATH') || exit('No direct script access allowed');


$lang['cms_manage']      = 'Manage CMS';
$lang['cms_edit']        = 'Edit';
$lang['cms_true']        = 'True';
$lang['cms_false']       = 'False';
$lang['cms_create']      = 'Create';
$lang['cms_list']        = 'List';
$lang['cms_new']       = 'New';
$lang['cms_edit_text']     = 'Edit this to suit your needs';
$lang['cms_no_records']    = 'There are no cms in the system.';
$lang['cms_create_new']    = 'Create a new CMS.';
$lang['cms_create_success']  = 'CMS successfully created.';
$lang['cms_create_failure']  = 'There was a problem creating the cms: ';
$lang['cms_create_new_button'] = 'Create New CMS';
$lang['cms_invalid_id']    = 'Invalid CMS ID.';
$lang['cms_edit_success']    = 'CMS successfully saved.';
$lang['cms_edit_failure']    = 'There was a problem saving the cms: ';
$lang['cms_delete_success']  = 'record(s) successfully deleted.';
$lang['cms_delete_failure']  = 'We could not delete the record: ';
$lang['cms_delete_error']    = 'You have not selected any records to delete.';
$lang['cms_actions']     = 'Actions';
$lang['cms_cancel']      = 'Cancel';
$lang['cms_delete_record']   = 'Delete this CMS';
$lang['cms_delete_confirm']  = 'Are you sure you want to delete this cms?';
$lang['cms_edit_heading']    = 'Edit CMS';

// Create/Edit Buttons
$lang['cms_action_edit']   = 'Save CMS';
$lang['cms_action_create']   = 'Create CMS';

// Activities
$lang['cms_act_create_record'] = 'Created record with ID';
$lang['cms_act_edit_record'] = 'Updated record with ID';
$lang['cms_act_delete_record'] = 'Deleted record with ID';

//Listing Specifics
$lang['cms_records_empty']    = 'No records found that match your selection.';
$lang['cms_errors_message']    = 'Please fix the following errors:';

// Column Headings
$lang['cms_column_created']  = 'Created';
$lang['cms_column_deleted']  = 'Deleted';
$lang['cms_column_modified'] = 'Modified';
$lang['cms_column_deleted_by'] = 'Deleted By';
$lang['cms_column_created_by'] = 'Created By';
$lang['cms_column_modified_by'] = 'Modified By';

// Module Details
$lang['cms_module_name'] = 'CMS';
$lang['cms_module_description'] = 'Your module description';
$lang['cms_area_title'] = 'CMS';

// Fields
$lang['cms_field_title'] = 'Page Title';
$lang['cms_field_slug_url'] = 'Page Slug URL';
$lang['cms_field_description'] = 'Page Content';