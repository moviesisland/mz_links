<?php defined('BASEPATH') || exit('No direct script access allowed');


$lang['hproviders_manage']      = 'Manage hproviders';
$lang['hproviders_edit']        = 'Edit';
$lang['hproviders_true']        = 'True';
$lang['hproviders_false']       = 'False';
$lang['hproviders_create']      = 'Create';
$lang['hproviders_list']        = 'List';
$lang['hproviders_new']       = 'New';
$lang['hproviders_edit_text']     = 'Edit this to suit your needs';
$lang['hproviders_no_records']    = 'There are no hproviders in the system.';
$lang['hproviders_create_new']    = 'Create a new hproviders.';
$lang['hproviders_create_success']  = 'hproviders successfully created.';
$lang['hproviders_create_failure']  = 'There was a problem creating the hproviders: ';
$lang['hproviders_create_new_button'] = 'Create New hproviders';
$lang['hproviders_invalid_id']    = 'Invalid hproviders ID.';
$lang['hproviders_edit_success']    = 'hproviders successfully saved.';
$lang['hproviders_edit_failure']    = 'There was a problem saving the hproviders: ';
$lang['hproviders_delete_success']  = 'record(s) successfully deleted.';
$lang['hproviders_delete_failure']  = 'We could not delete the record: ';
$lang['hproviders_delete_error']    = 'You have not selected any records to delete.';
$lang['hproviders_actions']     = 'Actions';
$lang['hproviders_cancel']      = 'Cancel';
$lang['hproviders_delete_record']   = 'Delete this hproviders';
$lang['hproviders_delete_confirm']  = 'Are you sure you want to delete this hproviders?';
$lang['hproviders_edit_heading']    = 'Edit hproviders';

// Create/Edit Buttons
$lang['hproviders_action_edit']   = 'Save hproviders';
$lang['hproviders_action_create']   = 'Create hproviders';

// Activities
$lang['hproviders_act_create_record'] = 'Created record with ID';
$lang['hproviders_act_edit_record'] = 'Updated record with ID';
$lang['hproviders_act_delete_record'] = 'Deleted record with ID';

//Listing Specifics
$lang['hproviders_records_empty']    = 'No records found that match your selection.';
$lang['hproviders_errors_message']    = 'Please fix the following errors:';

// Column Headings
$lang['hproviders_column_created']  = 'Created';
$lang['hproviders_column_deleted']  = 'Deleted';
$lang['hproviders_column_modified'] = 'Modified';
$lang['hproviders_column_deleted_by'] = 'Deleted By';
$lang['hproviders_column_created_by'] = 'Created By';
$lang['hproviders_column_modified_by'] = 'Modified By';

// Module Details
$lang['hproviders_module_name'] = 'hproviders';
$lang['hproviders_module_description'] = 'manage Hosting providers';
$lang['hproviders_area_title'] = 'hproviders';

// Fields
$lang['hproviders_field_name'] = 'Name';
$lang['hproviders_field_website_url'] = 'Website URL';
$lang['hproviders_field_desc'] = 'Description';