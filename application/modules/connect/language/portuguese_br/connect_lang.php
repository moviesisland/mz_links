<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['connect_manage']      = 'Gerenciar connect';
$lang['connect_edit']        = 'Editar';
$lang['connect_true']        = 'Verdadeiro';
$lang['connect_false']       = 'Falso';
$lang['connect_create']      = 'Criar';
$lang['connect_list']        = 'Listar';
$lang['connect_new']       = 'Novo';
$lang['connect_edit_text']     = 'Edite isto conforme sua necessidade';
$lang['connect_no_records']    = 'Não há connect no sistema.';
$lang['connect_create_new']    = 'Criar novo(a) connect.';
$lang['connect_create_success']  = 'connect Criado(a) com sucesso.';
$lang['connect_create_failure']  = 'Ocorreu um problema criando o(a) connect: ';
$lang['connect_create_new_button'] = 'Criar novo(a) connect';
$lang['connect_invalid_id']    = 'ID de connect inválida.';
$lang['connect_edit_success']    = 'connect salvo(a) com sucesso.';
$lang['connect_edit_failure']    = 'Ocorreu um problema salvando o(a) connect: ';
$lang['connect_delete_success']  = 'Registro(s) excluído(s) com sucesso.';
$lang['connect_delete_failure']  = 'Não foi possível excluir o registro: ';
$lang['connect_delete_error']    = 'Voc6e não selecionou nenhum registro para excluir.';
$lang['connect_actions']     = 'Ações';
$lang['connect_cancel']      = 'Cancelar';
$lang['connect_delete_record']   = 'Excluir este(a) connect';
$lang['connect_delete_confirm']  = 'Você tem certeza que deseja excluir este(a) connect?';
$lang['connect_edit_heading']    = 'Editar connect';

// Create/Edit Buttons
$lang['connect_action_edit']   = 'Salvar connect';
$lang['connect_action_create']   = 'Criar connect';

// Activities
$lang['connect_act_create_record'] = 'Criado registro com ID';
$lang['connect_act_edit_record'] = 'Atualizado registro com ID';
$lang['connect_act_delete_record'] = 'Excluído registro com ID';

//Listing Specifics
$lang['connect_records_empty']    = 'Nenhum registro encontrado.';
$lang['connect_errors_message']    = 'Por favor corrija os erros a seguir:';

// Column Headings
$lang['connect_column_created']  = 'Criado';
$lang['connect_column_deleted']  = 'Excluído';
$lang['connect_column_modified'] = 'Atualizado';

// Module Details
$lang['connect_module_name'] = 'connect';
$lang['connect_module_description'] = 'Your module description';
$lang['connect_area_title'] = 'connect';

// Fields
$lang['connect_field_option_title'] = 'Option Title';
$lang['connect_field_email'] = 'Email';
$lang['connect_field_workflow_name'] = 'Workflow';
$lang['connect_field_workflow_id'] = 'Workflow ID';
