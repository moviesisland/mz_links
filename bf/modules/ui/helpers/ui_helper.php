<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * BF
 *
 * An open source project to allow developers to jumpstart their development of
 * CodeIgniter applications
 *
 * @package   BASE
 * @author    BASE Dev Team
 * @copyright Copyright (c) 2011 - 2018, BASE Dev Team
 * @license   http://opensource.org/licenses/MIT The MIT License
  * 
 * @since     Version 1.0
 * @filesource
 */

/**
 * UI Helper
 *
 * @package BF\Modules\UI\Helpers\ui_helper
 * @author  BASE Dev Team
 * /docs
 */

if (! function_exists('renderSearchBox')) {
    /**
     * Display a search box.
     *
     * @return void
     */
    function renderSearchBox($template = '', $searchLabel = '', $searchPlaceholder = '')
    {
        $ci =& get_instance();

        // Handle any empty arguments.
        if (empty($searchLabel) || empty($searchPlaceholder)) {
            $search            = $ci->lang->line('bf_action_search');
            $searchLabel       = empty($searchLabel) ? "{$search}&hellip;" : $searchLabel;
            $searchPlaceholder = empty($searchPlaceholder) ? strtolower($search) . '&hellip;' : $searchPlaceholder;
        }

        if (empty($template)) {
            $template = "<a href='#' class='list-search'>{searchLabel}</a>
<div id='search-form' style='display: none'>
    <input type='search' class='list-search' value='' placeholder='{searchPlaceholder}' />
</div>";
        }

        // Allow use of a localized label/placeholder with the 'lang:' prefix.
        if (strpos($searchLabel, 'lang:') === 0) {
            $searchLabel = $ci->lang->line(str_replace('lang:', '', $searchLabel));
        }

        if (strpos($searchPlaceholder, 'lang:') === 0) {
            $searchPlaceholder = $ci->lang->line(str_replace('lang:', '', $searchPlaceholder));
        }

        echo str_replace(
            array('{searchLabel}', '{searchPlaceholder}'),
            array($searchLabel, $searchPlaceholder),
            $template
        );
    }
}
