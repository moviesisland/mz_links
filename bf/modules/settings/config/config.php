<?php defined('BASEPATH') || exit('No direct script access allowed');

$config['module_config'] = array(
    'description'   => 'Site-Wide Settings',
    'author'        => 'BASE Team',
    'name'          => 'lang:bf_menu_settings',
);