<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['file_manage']      = 'Gerenciar file';
$lang['file_edit']        = 'Editar';
$lang['file_true']        = 'Verdadeiro';
$lang['file_false']       = 'Falso';
$lang['file_create']      = 'Criar';
$lang['file_list']        = 'Listar';
$lang['file_new']       = 'Novo';
$lang['file_edit_text']     = 'Edite isto conforme sua necessidade';
$lang['file_no_records']    = 'Não há file no sistema.';
$lang['file_create_new']    = 'Criar novo(a) file.';
$lang['file_create_success']  = 'file Criado(a) com sucesso.';
$lang['file_create_failure']  = 'Ocorreu um problema criando o(a) file: ';
$lang['file_create_new_button'] = 'Criar novo(a) file';
$lang['file_invalid_id']    = 'ID de file inválida.';
$lang['file_edit_success']    = 'file salvo(a) com sucesso.';
$lang['file_edit_failure']    = 'Ocorreu um problema salvando o(a) file: ';
$lang['file_delete_success']  = 'Registro(s) excluído(s) com sucesso.';
$lang['file_delete_failure']  = 'Não foi possível excluir o registro: ';
$lang['file_delete_error']    = 'Voc6e não selecionou nenhum registro para excluir.';
$lang['file_actions']     = 'Ações';
$lang['file_cancel']      = 'Cancelar';
$lang['file_delete_record']   = 'Excluir este(a) file';
$lang['file_delete_confirm']  = 'Você tem certeza que deseja excluir este(a) file?';
$lang['file_edit_heading']    = 'Editar file';

// Create/Edit Buttons
$lang['file_action_edit']   = 'Salvar file';
$lang['file_action_create']   = 'Criar file';

// Activities
$lang['file_act_create_record'] = 'Criado registro com ID';
$lang['file_act_edit_record'] = 'Atualizado registro com ID';
$lang['file_act_delete_record'] = 'Excluído registro com ID';

//Listing Specifics
$lang['file_records_empty']    = 'Nenhum registro encontrado.';
$lang['file_errors_message']    = 'Por favor corrija os erros a seguir:';

// Column Headings
$lang['file_column_created']  = 'Criado';
$lang['file_column_deleted']  = 'Excluído';
$lang['file_column_modified'] = 'Atualizado';

// Module Details
$lang['file_module_name'] = 'file';
$lang['file_module_description'] = 'Host file information';
$lang['file_area_title'] = 'file';

// Fields
$lang['file_field_name'] = 'File Name';
$lang['file_field_size'] = 'File Size';
$lang['file_field_slug'] = 'URL Slug';
