<?php defined('BASEPATH') || exit('No direct script access allowed');

$config['module_config'] = array(
	'description'   => 'Allows other modules to store user activity information.',
	'author'		=> 'BASE Team',
	'name'			=> 'lang:bf_menu_activities',
);