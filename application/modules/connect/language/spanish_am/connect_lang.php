<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['connect_manage']      = 'Gestionar connect';
$lang['connect_edit']        = 'Editar';
$lang['connect_true']        = 'Verdadero';
$lang['connect_false']       = 'Falso';
$lang['connect_create']      = 'Crear';
$lang['connect_list']        = 'Listar';
$lang['connect_new']       = 'Nuevo';
$lang['connect_edit_text']     = 'Editar esto para satisfacer sus necesidades';
$lang['connect_no_records']    = 'Hay ninguna connect en la sistema.';
$lang['connect_create_new']    = 'Crear nuevo(a) connect.';
$lang['connect_create_success']  = 'connect creado(a) con éxito.';
$lang['connect_create_failure']  = 'Hubo un problema al crear el(la) connect: ';
$lang['connect_create_new_button'] = 'Crear nuevo(a) connect';
$lang['connect_invalid_id']    = 'ID de connect inválido(a).';
$lang['connect_edit_success']    = 'connect guardado correctamente.';
$lang['connect_edit_failure']    = 'Hubo un problema guardando el(la) connect: ';
$lang['connect_delete_success']  = 'Registro(s) eliminado con éxito.';
$lang['connect_delete_failure']  = 'No hemos podido eliminar el registro: ';
$lang['connect_delete_error']    = 'No ha seleccionado ning&#250;n registro que desea eliminar.';
$lang['connect_actions']     = 'Açciones';
$lang['connect_cancel']      = 'Cancelar';
$lang['connect_delete_record']   = 'Eliminar este(a) connect';
$lang['connect_delete_confirm']  = '¿Esta seguro de que desea eliminar este(a) connect?';
$lang['connect_edit_heading']    = 'Editar connect';

// Create/Edit Buttons
$lang['connect_action_edit']   = 'Guardar connect';
$lang['connect_action_create']   = 'Crear connect';

// Activities
$lang['connect_act_create_record'] = 'Creado registro con ID';
$lang['connect_act_edit_record'] = 'Actualizado registro con ID';
$lang['connect_act_delete_record'] = 'Eliminado registro con ID';

//Listing Specifics
$lang['connect_records_empty']    = 'No hay registros encontrados para su selección.';
$lang['connect_errors_message']    = 'Por favor corrija los siguientes errores:';

// Column Headings
$lang['connect_column_created']  = 'Creado';
$lang['connect_column_deleted']  = 'Elíminado';
$lang['connect_column_modified'] = 'Modificado';

// Module Details
$lang['connect_module_name'] = 'connect';
$lang['connect_module_description'] = 'Your module description';
$lang['connect_area_title'] = 'connect';

// Fields
$lang['connect_field_option_title'] = 'Option Title';
$lang['connect_field_email'] = 'Email';
$lang['connect_field_workflow_name'] = 'Workflow';
$lang['connect_field_workflow_id'] = 'Workflow ID';
