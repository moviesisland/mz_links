<?php defined('BASEPATH') || exit('No direct script access allowed');

$config['module_config'] = array(
    'author'      => 'BASE Team',
    'description' => 'Provides helpers for consistent admin UI features.',
    'name'        => 'lang:bf_menu_kb_shortcuts',
    'version'     => '0.7.3',
);
