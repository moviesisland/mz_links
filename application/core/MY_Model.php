<?php defined('BASEPATH') || exit('No direct script access allowed');
/**
 * BF
 *
 * An open source project to allow developers to jumpstart their development of
 * CodeIgniter applications.
 *
 * @package   BASE
 * @author    BASE Dev Team
 * @copyright Copyright (c) 2011 - 2014, BASE Dev Team
 * @license   http://opensource.org/licenses/MIT
 * 
 * @since     Version 1.0
 * @filesource
 */

/**
 * MY_Model extends BF_Model for backwards compatibility, and to provide a
 * placeholder class that your project can customize/extend as needed.
 *
 * @package    BASE\Core\MY_Model
  /docs/developer/bonfire_models
 */
class MY_Model extends BF_Model
{

}
/* End of file ./application/core/MY_Model.php */