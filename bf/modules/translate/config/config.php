<?php defined('BASEPATH') || exit('No direct script access allowed');

$config['module_config'] = array(
	'description'	=> 'Allows users to create translations for any language.',
	'author'		=> 'BASE Team',
    'name'          => 'lang:bf_menu_translate'
);