<?php

if (validation_errors()) :
?>
<div class='alert alert-block alert-error fade in'>
    <a class='close' data-dismiss='alert'>&times;</a>
    <h4 class='alert-heading'>
        <?php echo lang('email_template_errors_message'); ?>
    </h4>
    <?php echo validation_errors(); ?>
</div>
<?php
endif;

$id = isset($email_template->id) ? $email_template->id : '';

?>
<div class='admin-box'>
    <?php echo form_open($this->uri->uri_string()); ?>
        

            <div class="form-group <?php echo form_error('template_name') ? ' error' : ''; ?>">
                <?php echo form_label(lang('email_template_field_template_name') . lang('bf_form_label_required'), 'template_name' ); ?>
                <input id='template_name' type='text' required='required' name='template_name' maxlength='150' value="<?php echo set_value('template_name', isset($email_template->template_name) ? $email_template->template_name : ''); ?>" class='form-control' />
                <span class='help-inline'><?php echo form_error('template_name'); ?></span>
            </div>

            <div class="form-group <?php echo form_error('template_subject') ? ' error' : ''; ?>">
                <?php echo form_label(lang('email_template_field_template_subject') . lang('bf_form_label_required'), 'template_subject' ); ?>
                <input id='template_subject' type='text' required='required' name='template_subject' maxlength='255' value="<?php echo set_value('template_subject', isset($email_template->template_subject) ? $email_template->template_subject : ''); ?>" class='form-control' />
                <span class='help-inline'><?php echo form_error('template_subject'); ?></span>
            </div>

            <div class="form-group <?php echo form_error('template_description') ? ' error' : ''; ?>">
                <?php echo form_label(lang('email_template_field_template_description'), 'template_description'); ?>
                    <?php echo form_textarea(array('name' => 'template_description', 'id' => 'template_description', 'class' => 'form-control', 'rows' => '5', 'cols' => '80', 'value' => set_value('template_description', isset($email_template->template_description) ? $email_template->template_description : ''))); ?>
                    <span class='help-inline'><?php echo form_error('template_description'); ?></span>
            </div>

            <div class="form-group <?php echo form_error('template_body') ? ' error' : ''; ?>">
                <?php echo form_label(lang('email_template_field_template_body') . lang('bf_form_label_required'), 'template_body'); ?>
                    <?php echo form_textarea(array('name' => 'template_body', 'id' => 'template_body', 'class' => 'form-control', 'rows' => '5', 'cols' => '80', 'value' => set_value('template_body', isset($email_template->template_body) ? $email_template->template_body : ''), 'required' => 'required')); ?>
                    <span class='help-inline'><?php echo form_error('template_body'); ?></span>
            </div>

            <div class="form-group">
                <?php // Change the values in this array to populate your dropdown as required
                    $options = array(
                        1 => 1,
                    );
                    echo form_dropdown(array('name' => 'status', 'class'=>'form-control'), $options, set_value('status', isset($email_template->status) ? $email_template->status : ''), lang('email_template_field_status'));
                ?>
            </div>
        <div class='form-group'>
            <input type='submit' name='save' class='btn btn-primary' value="<?php echo lang('email_template_action_edit'); ?>" />
            <?php echo lang('bf_or'); ?>
            <?php echo anchor(SITE_AREA . '/settings/email_template', lang('email_template_cancel'), 'class="btn btn-default"'); ?>
            
            <?php if ($this->auth->has_permission('Email_template.Settings.Delete')) : ?>
                <?php echo lang('bf_or'); ?>
                <button type='submit' name='delete' formnovalidate class='btn btn-danger' id='delete-me' onclick="return confirm('<?php e(js_escape(lang('email_template_delete_confirm'))); ?>');">
                    <span class='icon-trash icon-white'></span>&nbsp;<?php echo lang('email_template_delete_record'); ?>
                </button>
            <?php endif; ?>
        </div>
    <?php echo form_close(); ?>
</div>