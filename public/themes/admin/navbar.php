<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="javascript:void(0);" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b><?php echo lang('bf_activate_admin_small'); ?></b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b><?php echo lang('bf_activate_admin'); ?></b></span>
    </a>
    
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top fixed-top">
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?php echo THEME_URL.'dist/img/user.png' ?>" class="user-image" alt="User Image">
              
              <span class="hidden-xs"> <?php
                          $userDisplayName = isset($current_user->display_name) && ! empty($current_user->display_name) ? $current_user->display_name : ($this->settings_lib->item('auth.use_usernames') ? $current_user->username : $current_user->email);
                          echo $userDisplayName;
                          ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
              
                  <img src="<?php echo THEME_URL.'dist/img/user.png' ?>" class="img-circle" alt="User Image">
                
                <p>
                  <?php
                          $userDisplayName = isset($current_user->display_name) && ! empty($current_user->display_name) ? $current_user->display_name : ($this->settings_lib->item('auth.use_usernames') ? $current_user->username : $current_user->email);
                          echo $userDisplayName;
                          ?>
              </p>
              </li>
              <!-- Menu Body -->
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo site_url(SITE_AREA .'/settings/users/edit') ?>" class="btn btn-default btn-flat"><?php echo lang('bf_users_profile'); ?></a>
                </div>
                <div class="pull-right">
                  <a class="btn btn-default btn-flat" href="<?php echo site_url('users/logout'); ?>"><?php e(lang('bf_action_logout')); ?></a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>