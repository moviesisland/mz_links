<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['hproviders_manage']      = 'Gestisci hproviders';
$lang['hproviders_edit']        = 'Modifica';
$lang['hproviders_true']        = 'Vero';
$lang['hproviders_false']       = 'Falso';
$lang['hproviders_create']      = 'Crea';
$lang['hproviders_list']        = 'Elenca';
$lang['hproviders_new']       = 'Nuovo';
$lang['hproviders_edit_text']     = 'Modifica questo secondo le tue necessità';
$lang['hproviders_no_records']    = 'Non ci sono hproviders nel sistema.';
$lang['hproviders_create_new']    = 'Crea un nuovo hproviders.';
$lang['hproviders_create_success']  = 'hproviders creato con successo.';
$lang['hproviders_create_failure']  = 'C\'è stato un problema nella creazione di hproviders: ';
$lang['hproviders_create_new_button'] = 'Crea nuovo hproviders';
$lang['hproviders_invalid_id']    = 'ID hproviders non valido.';
$lang['hproviders_edit_success']    = 'hproviders creato con successo.';
$lang['hproviders_edit_failure']    = 'C\'è stato un errore nel salvataggio di hproviders: ';
$lang['hproviders_delete_success']  = 'record(s) creati con successo.';
$lang['hproviders_delete_failure']  = 'Non possiamo eliminare il record: ';
$lang['hproviders_delete_error']    = 'Non hai selezionato alcun record da eliminare.';
$lang['hproviders_actions']     = 'Azioni';
$lang['hproviders_cancel']      = 'Cancella';
$lang['hproviders_delete_record']   = 'Elimina questo hproviders';
$lang['hproviders_delete_confirm']  = 'Sei sicuro di voler eliminare questo hproviders?';
$lang['hproviders_edit_heading']    = 'Modifica hproviders';

// Create/Edit Buttons
$lang['hproviders_action_edit']   = 'Salva hproviders';
$lang['hproviders_action_create']   = 'Crea hproviders';

// Activities
$lang['hproviders_act_create_record'] = 'Creato il record con ID';
$lang['hproviders_act_edit_record'] = 'Aggiornato il record con ID';
$lang['hproviders_act_delete_record'] = 'Eliminato il record con ID';

// Listing Specifics
$lang['hproviders_records_empty']    = 'Nessun record trovato che corrisponda alla tua selezione.';
$lang['hproviders_errors_message']    = 'Per favore risolvi i seguenti errori:';

// Column Headings
$lang['hproviders_column_created']  = 'Creato';
$lang['hproviders_column_deleted']  = 'Eliminato';
$lang['hproviders_column_modified'] = 'Modificato';

// Module Details
$lang['hproviders_module_name'] = 'hproviders';
$lang['hproviders_module_description'] = 'manage Hosting providers';
$lang['hproviders_area_title'] = 'hproviders';

// Fields
$lang['hproviders_field_name'] = 'Name';
$lang['hproviders_field_website_url'] = 'Website URL';
$lang['hproviders_field_desc'] = 'Description';
