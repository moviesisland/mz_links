<?php defined('BASEPATH') || exit('No direct script access allowed');

// -----------------------------------------------------------------------------
// Common function file
// -----------------------------------------------------------------------------

/* 
 * Function pr to print array without exit
 */
function pr($printarray) {
    echo "<pre>";
    print_r($printarray);
    echo "</pre>";
}

/**
 * Function pre to print array with exit
 */
function pre($printarray) {
    echo "<pre>";
    print_r($printarray);
    echo "</pre>";
    exit;
}

/**
 * Function get random string
 */
function get_random_string($length = 8) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $string = '';

    for ($i = 0; $i < $length; $i++) {
        $string .= $characters[mt_rand(0, strlen($characters) - 1)];
    }

    return $string;
}

/**
 * Function get random number
 */
function get_random_number($length = 8) {
    $characters = '0123456789';
    $string = '';

    for ($i = 0; $i < $length; $i++) {
        $string .= $characters[mt_rand(0, strlen($characters) - 1)];
    }

    return $string;
}

// -----------------------------------------------------------------------------
// Send Email Common Functions
// -----------------------------------------------------------------------------
function get_template_sections($template_name,$mail_var)
{
    // Get Instance
    $obj =   &get_instance();

    // Initializing Array
    $template_result = array();

    // Initializing variables
    $body = $subj = $template_name; 

    // Prepar Query
    $query = $obj->db->query("SELECT * FROM `bf_email_template` WHERE `status` = 1 AND `template_name` = '".$template_name."' ");

    // Get query Result
    $template_result = $query->result_array();

    // Get Subject And Body
    if (!empty($template_result)) {
        $subj = srepltags_array($mail_var,$template_result[0]['template_subject'],false);
        $body = srepltags_array($mail_var,$template_result[0]['template_body']);
    }

    return array('body' => $body, 'subject' => $subj);
}

/**
 * Replace string in template body
 * @param  array  $arr
 * @param  $str
 * @param  boolean $html
 * @return String
 */
function srepltags_array($arr,$str,$html=true)
{
    // Check Is Array
    if (is_array($arr)) {
        reset($arr);
        $keys = array_keys($arr);
        array_walk($keys, create_function('&$val', '$val = "[$val]";'));
        $vals = array_values($arr);
        if ($html) {
            return preg_replace('/^[0-9a-zA-Z\/_\/s\/-]+/', '', str_replace($keys, $vals, $str));
            // return preg_replace('/^[0-9a-zA-Z\/_\/s\/-]$/', '', str_replace($keys, $vals, $str));
        } else {
            return str_replace($keys, $vals, $str);
        }
    }
    else
    {
        return $str;
    }
}

/**
 * Function send email to user
 * @params $email_id $data for destination email address
 * @params $mail_vars (variable required to template body)
 */

/**
 * Function send email to user
 * @param  array   $mail_vars  [description]
 * @param  string  $template_name
 * @param  integer $email_id   [description]
 * @param  string  $reply_to   [description]
 * @param  string  $reply_name [description]
 * @return boolean  True/False
 */
function send_email($mail_vars = array(), $template_name = NULL, $email_id,$reply_to='',$reply_name='') {          

    // Get CI Instance
    $CI =   &get_instance();

    // Load Mailer Library
    $CI->load->library('mailer');
    
    // Get Final Template Section
    $template_sect = get_template_sections($template_name, $mail_vars);

    // Get Body
    $body = $template_sect['body'];

    // Get Subject
    $subject = $template_sect['subject'];        

    if (trim($body) == "") {
        return false;
    }
    else            
    {
        $CI->mailer->sendmail($email_id, " ", $subject, $body,$reply_to,$reply_name);
        return true;            
    }
}

/* Genrate Random string by given lengeth */
function crypto_rand_secure($min, $max) {
    $range = $max - $min;
    if ($range < 1) return $min; // not so random...
    $log = ceil(log($range, 2));
    $bytes = (int) ($log / 8) + 1; // length in bytes
    $bits = (int) $log + 1; // length in bits
    $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
    do {
        $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
        $rnd = $rnd & $filter; // discard irrelevant bits
    } while ($rnd > $range);
    return $min + $rnd;
}
function random_string_genrater($length) {
    $token = "";
    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
    $codeAlphabet.= "0123456789";
    $max = strlen($codeAlphabet); // edited

    for ($i=0; $i < $length; $i++) {
        $token .= $codeAlphabet[crypto_rand_secure(0, $max-1)];
    }

    return $token;
}

function remove_space_str()
{
    $str = "Wonder Woman 2017 English 1080p 10bit BluRay 8CH x265 Hindi-Eng Moviesisland.in";

    $arr = [' ', '-'];
    
    $returnValue = str_replace($arr, '.', $str , $count);

    echo $returnValue;
}

function sync_gd()
{
    $curl = curl_init();

    // $fileId = '1PnRIuGZZMmwFM2_TdRHxSj51b1-f0mb7';

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://www.googleapis.com/drive/v3/files/",
        // CURLOPT_URL => "https://www.googleapis.com/drive/v3/drives/0ALeCAKrBgKstUk9PVA",
        // CURLOPT_URL => "https://www.googleapis.com/drive/v2/files?q='1f8I70Z_CkFn79YVIFv4Rb1h2q9iqxYj9'",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "post",
        CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer ya29.a0AfH6SMBEJJ8EX4-KLex5QXD9aUcq9efYvGI7vhEEdRZB1ey0pWIkmZCXIYSG9wS2eLfuol5LItgQKk1K_eao_l5LVQ3IG7f1MUtKks2w7rbIDqHntacLhVyghR926tkai1Ad2mDx3-FFqo8xheIB8wrP5RhHMuUwYGg"
        ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    echo "<pre>";
    print_r($response);

}

?>