<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['file_manage']      = 'Gestionar file';
$lang['file_edit']        = 'Editar';
$lang['file_true']        = 'Verdadero';
$lang['file_false']       = 'Falso';
$lang['file_create']      = 'Crear';
$lang['file_list']        = 'Listar';
$lang['file_new']       = 'Nuevo';
$lang['file_edit_text']     = 'Editar esto para satisfacer sus necesidades';
$lang['file_no_records']    = 'Hay ninguna file en la sistema.';
$lang['file_create_new']    = 'Crear nuevo(a) file.';
$lang['file_create_success']  = 'file creado(a) con éxito.';
$lang['file_create_failure']  = 'Hubo un problema al crear el(la) file: ';
$lang['file_create_new_button'] = 'Crear nuevo(a) file';
$lang['file_invalid_id']    = 'ID de file inválido(a).';
$lang['file_edit_success']    = 'file guardado correctamente.';
$lang['file_edit_failure']    = 'Hubo un problema guardando el(la) file: ';
$lang['file_delete_success']  = 'Registro(s) eliminado con éxito.';
$lang['file_delete_failure']  = 'No hemos podido eliminar el registro: ';
$lang['file_delete_error']    = 'No ha seleccionado ning&#250;n registro que desea eliminar.';
$lang['file_actions']     = 'Açciones';
$lang['file_cancel']      = 'Cancelar';
$lang['file_delete_record']   = 'Eliminar este(a) file';
$lang['file_delete_confirm']  = '¿Esta seguro de que desea eliminar este(a) file?';
$lang['file_edit_heading']    = 'Editar file';

// Create/Edit Buttons
$lang['file_action_edit']   = 'Guardar file';
$lang['file_action_create']   = 'Crear file';

// Activities
$lang['file_act_create_record'] = 'Creado registro con ID';
$lang['file_act_edit_record'] = 'Actualizado registro con ID';
$lang['file_act_delete_record'] = 'Eliminado registro con ID';

//Listing Specifics
$lang['file_records_empty']    = 'No hay registros encontrados para su selección.';
$lang['file_errors_message']    = 'Por favor corrija los siguientes errores:';

// Column Headings
$lang['file_column_created']  = 'Creado';
$lang['file_column_deleted']  = 'Elíminado';
$lang['file_column_modified'] = 'Modificado';

// Module Details
$lang['file_module_name'] = 'file';
$lang['file_module_description'] = 'Host file information';
$lang['file_area_title'] = 'file';

// Fields
$lang['file_field_name'] = 'File Name';
$lang['file_field_size'] = 'File Size';
$lang['file_field_slug'] = 'URL Slug';
