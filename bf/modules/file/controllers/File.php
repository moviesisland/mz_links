<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * File controller
 */
class File extends Front_Controller
{
    protected $permissionCreate = 'File.File.Create';
    protected $permissionDelete = 'File.File.Delete';
    protected $permissionEdit   = 'File.File.Edit';
    protected $permissionView   = 'File.File.View';

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('file/file_model');
        $this->lang->load('file');
        
        

        // Assets::add_module_js('file', 'file.js');
    }

    /**
     * Display a list of file data.
     *
     * @return void
     */
    public function index($offset = 0)
    {
        
        $pagerUriSegment = 3;
        $pagerBaseUrl = site_url('file/index') . '/';
        
        $limit  = $this->settings_lib->item('site.list_limit') ?: 15;

        $this->load->library('pagination');
        $this->pager['base_url']    = $pagerBaseUrl;
        $this->pager['total_rows']  = $this->file_model->count_all();
        $this->pager['per_page']    = $limit;
        $this->pager['uri_segment'] = $pagerUriSegment;

        $this->pagination->initialize($this->pager);
        $this->file_model->limit($limit, $offset);
        

        // Don't display soft-deleted records
        $this->file_model->where($this->file_model->get_deleted_field(), 0);
        $records = $this->file_model->find_all();

        Template::set('records', $records);
        

        Template::render();
    }

    function details($slug)
    {
        $result = $this->file_model->find_by('slug',$slug);

        // Get Links Details
        $links = $this->file_model->get_only_links($result->id);
        // pre($links);
        
        Template::set('d_link', $links[0]->link);
        unset($links[0]);
        Template::set('links_data', $links);
        // pre($links_data);

        if (!empty($result))
        {
            // Set Value in view
            Template::set('file', $result);

            // Load View
            Template::set_view('view');
            Template::render();
        } else {
            Template::redirect('/');
        }
    }
    
}