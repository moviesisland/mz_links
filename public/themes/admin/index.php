<?php

Assets::add_css(array(
    'bower_components/bootstrap/dist/css/bootstrap.min.css',
    'bower_components/font-awesome/css/font-awesome.min.css',
    'bower_components/Ionicons/css/ionicons.min.css',
    'dist/css/skins/_all-skins.min.css',
    'dist/css/AdminLTE.min.css',
    'custom/css/style.css',
));

Assets::add_js(array(
    'bower_components/jquery/dist/jquery.min.js',
    'bower_components/bootstrap/dist/js/bootstrap.min.js',
    'bower_components/jquery-slimscroll/jquery.slimscroll.min.js',
    'bower_components/fastclick/lib/fastclick.js',
    'dist/js/adminlte.min.js',
    'dist/js/demo.js',
    'custom/js/custom.js',
), 'external', true);

echo theme_view('header');

echo theme_view('navbar');

echo theme_view('side_panal');

?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box box-default box-solid">
        <div class="box-header with-border">
          <h3 class="box-title">
            <?php if (isset($toolbar_title)) : ?>
                <span><?php echo $toolbar_title; ?></span>
              <?php endif; ?>
          </h3>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-lg-8"></div>
            <div class="col-lg-4">
              <div class="pull-right" id="sub-menu">
                  <?php Template::block('sub_nav', ''); ?>
                </div>
            </div>
          </div>
          <div class="container-fluid">            
              <?php
                echo Template::message();
                echo isset($content) ? $content : Template::content();
              ?>                                      
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php echo theme_view('footer'); ?>