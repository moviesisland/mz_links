<?php /* /users/views/user_fields.php */

$currentMethod = $this->router->method;

$errorClass     = empty($errorClass) ? ' error' : $errorClass;
$controlClass   = empty($controlClass) ? 'form-control' : 'form-control';
$registerClass  = $currentMethod == 'register' ? ' required' : '';
$editSettings   = $currentMethod == 'edit';

$defaultLanguage = isset($user->language) ? $user->language : strtolower(settings_item('language'));
$defaultTimezone = isset($user->timezone) ? $user->timezone : strtoupper(settings_item('site.default_user_timezone'));

?>
<div class="form-group <?php echo form_error('display_name') ? $errorClass : ''; ?>">
    <label for="display_name"><?php echo lang('bf_display_name'); ?></label>
        <input class="<?php echo $controlClass; ?>" type="text" id="display_name" name="display_name" value="<?php echo set_value('display_name', isset($user) ? $user->display_name : ''); ?>" />
        <span class="help-inline"><?php echo form_error('display_name'); ?></span>
</div>
<div class="form-group <?php echo form_error('last_name') ? $errorClass : ''; ?>">
    <label for="last_name"><?php echo lang('bf_last_name'); ?></label>
        <input class="<?php echo $controlClass; ?>" type="text" id="last_name" name="last_name" value="<?php echo set_value('last_name', isset($user) ? $user->last_name : ''); ?>" />
        <span class="help-inline"><?php echo form_error('last_name'); ?></span>
</div>
<div class="form-group <?php echo form_error('address') ? '$errorClass' : ''; ?>">
    <?php echo form_label(lang('bf_address'), 'address'); ?>
    <?php echo form_textarea(array('name' => 'address','class'=>$controlClass, 'id' => 'address', 'rows' => '5', 'cols' => '80', 'value' => set_value('address', isset($user->address) ? $user->address : '',false))); ?>
    <span class='help-block'><?php echo form_error('address'); ?></span>
</div>

<div class="form-group <?php echo form_error('city') ? $errorClass : ''; ?>">
    <label for="city"><?php echo lang('bf_city'); ?></label>
        <input class="<?php echo $controlClass; ?>" type="text" id="city" name="city" value="<?php echo set_value('city', isset($user) ? $user->city : ''); ?>" />
        <span class="help-inline"><?php echo form_error('city'); ?></span>
</div>
<div class="form-group <?php echo form_error("state") ? ' error' : ''; ?>">
    <label for="<?php echo "state" ?>"><?php echo lang('bf_state'); ?></label>
        <?php
            echo state_select(
                set_value("state", isset($user) ? $user->state : ''),
                "NY",//DEFAULT_STATE,
                "US",//DEFAULT_COUNTRY,
                "state",
                'form-control'
            );
        ?>
</div>
<div class="form-group <?php echo form_error('zip') ? $errorClass : ''; ?>">
    <label for="zip"><?php echo lang('bf_zip'); ?></label>
        <input class="<?php echo $controlClass; ?>" type="text" id="zip" name="zip" value="<?php echo set_value('zip', isset($user) ? $user->zip : ''); ?>" />
        <span class="help-inline"><?php echo form_error('zip'); ?></span>
</div>
<div class="form-group <?php echo form_error('phone') ? $errorClass : ''; ?>">
    <label for="phone"><?php echo lang('bf_phone'); ?></label>
        <input class="<?php echo $controlClass; ?>" type="text" id="phone" name="phone" value="<?php echo set_value('phone', isset($user) ? $user->phone : ''); ?>" />
        <span class="help-inline"><?php echo form_error('phone'); ?></span>
</div>
<div class="form-group <?php echo form_error('email') ? $errorClass : ''; ?>">
    <label class="control-label" for="email"><?php echo lang('bf_email') . lang('bf_form_label_required'); ?></label>
        <input class="<?php echo $controlClass; ?>" type="email" id="email" name="email" value="<?php echo set_value('email', isset($user) ? $user->email : ''); ?>" />
    <span class="help-inline text-danger"><?php echo form_error('email'); ?></span>
</div>

<?php if (settings_item('auth.login_type') !== 'email' || settings_item('auth.use_usernames')) : ?>
<div class="form-group <?php echo form_error('username') ? $errorClass : ''; ?>">
    <label class="control-label" for="username"><?php echo lang('bf_username') . lang('bf_form_label_required'); ?></label>
        <input class="<?php echo $controlClass; ?>" type="text" id="username" name="username" value="<?php echo set_value('username', isset($user) ? $user->username : ''); ?>" />
    <span class="help-inline text-danger"><?php echo form_error('username'); ?></span>
</div>
<?php endif; ?>
<div class="form-group <?php echo form_error('password') ? $errorClass : ''; ?>">
    <label  for="password"><?php echo lang('bf_password') . lang('bf_form_label_required'); ?></label>
        <input class="<?php echo $controlClass; ?>" type="password" id="password" name="password" value="" />
    <span class="help-inline text-danger"><?php echo form_error('password'); ?></span>
    <p class="help-block"><?php echo isset($password_hints) ? $password_hints : ''; ?></p>
</div>
<div class="form-group <?php echo form_error('pass_confirm') ? $errorClass : ''; ?>">
    <label  for="pass_confirm"><?php echo lang('bf_password_confirm') . lang('bf_form_label_required'); ?></label>
    <input class="<?php echo $controlClass; ?>" type="password" id="pass_confirm" name="pass_confirm" value="" />
    <span class="help-inline text-danger"><?php echo form_error('pass_confirm'); ?></span>
    <!-- <div class="form-group <?php echo form_error('force_password_reset') ? $errorClass : ''; ?>">
        <label for="force_password_reset">
            <input type="checkbox" id="force_password_reset" name="force_password_reset" value="1"  <?php echo set_checkbox('force_password_reset', empty($user->force_password_reset)); ?> />
            <?php echo lang('us_force_password_reset'); ?>
        </label>
    </div> -->
</div>

<?php
if (! empty($languages) && is_array($languages)) :
    if (count($languages) == 1) :
?>
    <input type="hidden" id="language" name="language" value="<?php echo $languages[0]; ?>" />
<?php
    else :
?>
<div class="form-group <?php echo form_error('language') ? $errorClass : ''; ?>">
    <label for="language"><?php echo lang('bf_language'); ?></label>
        <select name="language" id="language" class="chzn-select <?php echo $controlClass; ?>">
            <?php foreach ($languages as $language) : ?>
            <option value="<?php e($language); ?>" <?php echo set_select('language', $language, $defaultLanguage == $language); ?>>
                <?php e(ucfirst($language)); ?>
            </option>
            <?php endforeach; ?>
        </select>
        <span class="help-inline"><?php echo form_error('language'); ?></span>
</div>
<?php
    endif;
endif;
?>