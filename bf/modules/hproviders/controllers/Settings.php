<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Settings controller
 */
class Settings extends Admin_Controller
{
    protected $permissionCreate = 'Hproviders.Settings.Create';
    protected $permissionDelete = 'Hproviders.Settings.Delete';
    protected $permissionEdit   = 'Hproviders.Settings.Edit';
    protected $permissionView   = 'Hproviders.Settings.View';

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->auth->restrict($this->permissionView);
        $this->load->model('hproviders/hproviders_model');
        $this->lang->load('hproviders');
        
            $this->form_validation->set_error_delimiters("<span class='error'>", "</span>");
        
        Template::set_block('sub_nav', 'settings/_sub_nav');

        Assets::add_module_js('hproviders', 'hproviders.js');
    }

    /**
     * Display a list of hproviders data.
     *
     * @return void
     */
    public function index($offset = 0)
    {
        // Deleting anything?
        if (isset($_POST['delete'])) {
            $this->auth->restrict($this->permissionDelete);
            $checked = $this->input->post('checked');
            if (is_array($checked) && count($checked)) {

                // If any of the deletions fail, set the result to false, so
                // failure message is set if any of the attempts fail, not just
                // the last attempt

                $result = true;
                foreach ($checked as $pid) {
                    $deleted = $this->hproviders_model->delete($pid);
                    if ($deleted == false) {
                        $result = false;
                    }
                }
                if ($result) {
                    Template::set_message(count($checked) . ' ' . lang('hproviders_delete_success'), 'success');
                } else {
                    Template::set_message(lang('hproviders_delete_failure') . $this->hproviders_model->error, 'error');
                }
            }
        }
        $pagerUriSegment = 5;
        $pagerBaseUrl = site_url(SITE_AREA . '/settings/hproviders/index') . '/';
        
        $limit  = $this->settings_lib->item('site.list_limit') ?: 15;

        $this->load->library('pagination');
        $this->pager['base_url']    = $pagerBaseUrl;
        $this->pager['total_rows']  = $this->hproviders_model->count_all();
        $this->pager['per_page']    = $limit;
        $this->pager['uri_segment'] = $pagerUriSegment;

        $this->pagination->initialize($this->pager);
        $this->hproviders_model->limit($limit, $offset);
        
        $records = $this->hproviders_model->find_all();

        Template::set('records', $records);
        
        Template::set('toolbar_title', lang('hproviders_manage'));

        Template::render();
    }
    
    /**
     * Create a hproviders object.
     *
     * @return void
     */
    public function create()
    {
        $this->auth->restrict($this->permissionCreate);
        
        if (isset($_POST['save'])) {
            if ($insert_id = $this->save_hproviders()) {
                log_activity($this->auth->user_id(), lang('hproviders_act_create_record') . ': ' . $insert_id . ' : ' . $this->input->ip_address(), 'hproviders');
                Template::set_message(lang('hproviders_create_success'), 'success');

                redirect(SITE_AREA . '/settings/hproviders');
            }

            // Not validation error
            if ( ! empty($this->hproviders_model->error)) {
                Template::set_message(lang('hproviders_create_failure') . $this->hproviders_model->error, 'error');
            }
        }

        Template::set('toolbar_title', lang('hproviders_action_create'));

        Template::render();
    }
    /**
     * Allows editing of hproviders data.
     *
     * @return void
     */
    public function edit()
    {
        $id = $this->uri->segment(5);
        if (empty($id)) {
            Template::set_message(lang('hproviders_invalid_id'), 'error');

            redirect(SITE_AREA . '/settings/hproviders');
        }
        
        if (isset($_POST['save'])) {
            $this->auth->restrict($this->permissionEdit);

            if ($this->save_hproviders('update', $id)) {
                log_activity($this->auth->user_id(), lang('hproviders_act_edit_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'hproviders');
                Template::set_message(lang('hproviders_edit_success'), 'success');
                redirect(SITE_AREA . '/settings/hproviders');
            }

            // Not validation error
            if ( ! empty($this->hproviders_model->error)) {
                Template::set_message(lang('hproviders_edit_failure') . $this->hproviders_model->error, 'error');
            }
        }
        
        elseif (isset($_POST['delete'])) {
            $this->auth->restrict($this->permissionDelete);

            if ($this->hproviders_model->delete($id)) {
                log_activity($this->auth->user_id(), lang('hproviders_act_delete_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'hproviders');
                Template::set_message(lang('hproviders_delete_success'), 'success');

                redirect(SITE_AREA . '/settings/hproviders');
            }

            Template::set_message(lang('hproviders_delete_failure') . $this->hproviders_model->error, 'error');
        }
        
        Template::set('hproviders', $this->hproviders_model->find($id));

        Template::set_view('create');
        Template::set('toolbar_title', lang('hproviders_edit_heading'));
        Template::render();
    }

    //--------------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------------

    /**
     * Save the data.
     *
     * @param string $type Either 'insert' or 'update'.
     * @param int    $id   The ID of the record to update, ignored on inserts.
     *
     * @return boolean|integer An ID for successful inserts, true for successful
     * updates, else false.
     */
    private function save_hproviders($type = 'insert', $id = 0)
    {
        if ($type == 'update') {
            $_POST['id'] = $id;
        }

        // Validate the data
        $this->form_validation->set_rules($this->hproviders_model->get_validation_rules());
        if ($this->form_validation->run() === false) {
            return false;
        }

        // Make sure we only pass in the fields we want
        
        $data = $this->hproviders_model->prep_data($this->input->post());

        // Additional handling for default values should be added below,
        // or in the model's prep_data() method
        

        $return = false;
        if ($type == 'insert') {
            $id = $this->hproviders_model->insert($data);

            if (is_numeric($id)) {
                $return = $id;
            }
        } elseif ($type == 'update') {
            $return = $this->hproviders_model->update($id, $data);
        }

        return $return;
    }
}