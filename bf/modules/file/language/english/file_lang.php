<?php defined('BASEPATH') || exit('No direct script access allowed');


$lang['file_manage']      = 'Manage file';
$lang['file_edit']        = 'Edit';
$lang['file_true']        = 'True';
$lang['file_false']       = 'False';
$lang['file_create']      = 'Create';
$lang['file_list']        = 'List';
$lang['file_new']       = 'New';
$lang['file_edit_text']     = 'Edit this to suit your needs';
$lang['file_no_records']    = 'There are no file in the system.';
$lang['file_create_new']    = 'Create a new file.';
$lang['file_create_success']  = 'file successfully created.';
$lang['file_create_failure']  = 'There was a problem creating the file: ';
$lang['file_create_new_button'] = 'Create New file';
$lang['file_invalid_id']    = 'Invalid file ID.';
$lang['file_edit_success']    = 'file successfully saved.';
$lang['file_edit_failure']    = 'There was a problem saving the file: ';
$lang['file_delete_success']  = 'record(s) successfully deleted.';
$lang['file_delete_failure']  = 'We could not delete the record: ';
$lang['file_delete_error']    = 'You have not selected any records to delete.';
$lang['file_actions']     = 'Actions';
$lang['file_cancel']      = 'Cancel';
$lang['file_delete_record']   = 'Delete this file';
$lang['file_delete_confirm']  = 'Are you sure you want to delete this file?';
$lang['file_edit_heading']    = 'Edit file';

// Create/Edit Buttons
$lang['file_action_edit']   = 'Save file';
$lang['file_action_create']   = 'Create file';

// Activities
$lang['file_act_create_record'] = 'Created record with ID';
$lang['file_act_edit_record'] = 'Updated record with ID';
$lang['file_act_delete_record'] = 'Deleted record with ID';

//Listing Specifics
$lang['file_records_empty']    = 'No records found that match your selection.';
$lang['file_errors_message']    = 'Please fix the following errors:';

// Column Headings
$lang['file_column_created']  = 'Created';
$lang['file_column_deleted']  = 'Deleted';
$lang['file_column_modified'] = 'Modified';
$lang['file_column_deleted_by'] = 'Deleted By';
$lang['file_column_created_by'] = 'Created By';
$lang['file_column_modified_by'] = 'Modified By';

// Module Details
$lang['file_module_name'] = 'file';
$lang['file_module_description'] = 'Host file information';
$lang['file_area_title'] = 'file';

// Fields
$lang['file_field_name'] = 'File Name';
$lang['file_field_size'] = 'File Size';
$lang['file_field_slug'] = 'URL Slug';