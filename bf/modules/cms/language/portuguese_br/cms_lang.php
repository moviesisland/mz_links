<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['cms_manage']      = 'Gerenciar CMS';
$lang['cms_edit']        = 'Editar';
$lang['cms_true']        = 'Verdadeiro';
$lang['cms_false']       = 'Falso';
$lang['cms_create']      = 'Criar';
$lang['cms_list']        = 'Listar';
$lang['cms_new']       = 'Novo';
$lang['cms_edit_text']     = 'Edite isto conforme sua necessidade';
$lang['cms_no_records']    = 'Não há cms no sistema.';
$lang['cms_create_new']    = 'Criar novo(a) CMS.';
$lang['cms_create_success']  = 'CMS Criado(a) com sucesso.';
$lang['cms_create_failure']  = 'Ocorreu um problema criando o(a) cms: ';
$lang['cms_create_new_button'] = 'Criar novo(a) CMS';
$lang['cms_invalid_id']    = 'ID de CMS inválida.';
$lang['cms_edit_success']    = 'CMS salvo(a) com sucesso.';
$lang['cms_edit_failure']    = 'Ocorreu um problema salvando o(a) cms: ';
$lang['cms_delete_success']  = 'Registro(s) excluído(s) com sucesso.';
$lang['cms_delete_failure']  = 'Não foi possível excluir o registro: ';
$lang['cms_delete_error']    = 'Voc6e não selecionou nenhum registro para excluir.';
$lang['cms_actions']     = 'Ações';
$lang['cms_cancel']      = 'Cancelar';
$lang['cms_delete_record']   = 'Excluir este(a) CMS';
$lang['cms_delete_confirm']  = 'Você tem certeza que deseja excluir este(a) cms?';
$lang['cms_edit_heading']    = 'Editar CMS';

// Create/Edit Buttons
$lang['cms_action_edit']   = 'Salvar CMS';
$lang['cms_action_create']   = 'Criar CMS';

// Activities
$lang['cms_act_create_record'] = 'Criado registro com ID';
$lang['cms_act_edit_record'] = 'Atualizado registro com ID';
$lang['cms_act_delete_record'] = 'Excluído registro com ID';

//Listing Specifics
$lang['cms_records_empty']    = 'Nenhum registro encontrado.';
$lang['cms_errors_message']    = 'Por favor corrija os erros a seguir:';

// Column Headings
$lang['cms_column_created']  = 'Criado';
$lang['cms_column_deleted']  = 'Excluído';
$lang['cms_column_modified'] = 'Atualizado';

// Module Details
$lang['cms_module_name'] = 'CMS';
$lang['cms_module_description'] = 'Your module description';
$lang['cms_area_title'] = 'CMS';

// Fields
$lang['cms_field_title'] = 'Page Title';
$lang['cms_field_slug_url'] = 'Page Slug URL';
$lang['cms_field_description'] = 'Page Content';
