<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Settings controller
 */
class Settings extends Admin_Controller
{
    protected $permissionCreate = 'Cms.Settings.Create';
    protected $permissionDelete = 'Cms.Settings.Delete';
    protected $permissionEdit   = 'Cms.Settings.Edit';
    protected $permissionView   = 'Cms.Settings.View';

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->auth->restrict($this->permissionView);
        $this->load->model('cms/cms_model');
        $this->lang->load('cms');
        
            $this->form_validation->set_error_delimiters("<span class='error'>", "</span>");
        
        Template::set_block('sub_nav', 'settings/_sub_nav');

        Assets::add_module_js('cms', 'cms.js');
    }

    /**
     * Display a list of CMS data.
     *
     * @return void
     */
    public function index($offset = 0)
    {        
        // Deleting anything?
        if (isset($_POST['delete'])) {
            $this->auth->restrict($this->permissionDelete);
            $checked = $this->input->post('checked');
            if (is_array($checked) && count($checked)) {

                // If any of the deletions fail, set the result to false, so
                // failure message is set if any of the attempts fail, not just
                // the last attempt

                $result = true;
                foreach ($checked as $pid) {
                    $deleted = $this->cms_model->delete($pid);
                    if ($deleted == false) {
                        $result = false;
                    }
                }
                if ($result) {
                    Template::set_message(count($checked) . ' ' . lang('cms_delete_success'), 'success');
                } else {
                    Template::set_message(lang('cms_delete_failure') . $this->cms_model->error, 'error');
                }
            }
        }
        $pagerUriSegment = 5;
        $pagerBaseUrl = site_url(SITE_AREA . '/settings/cms/index') . '/';
        
        $limit  = $this->settings_lib->item('site.list_limit') ?: 15;

        $this->load->library('pagination');
        $this->pager['base_url']    = $pagerBaseUrl;
        $this->pager['total_rows']  = $this->cms_model->count_all();
        $this->pager['per_page']    = $limit;
        $this->pager['uri_segment'] = $pagerUriSegment;

        $this->pagination->initialize($this->pager);
        $this->cms_model->limit($limit, $offset);
        
        $records = $this->cms_model->find_all();

        Template::set('records', $records);
        
        Template::set('toolbar_title', lang('cms_manage'));

        Template::render();
    }
    
    /**
     * Create a CMS object.
     *
     * @return void
     */
    public function create()
    {
        $this->auth->restrict($this->permissionCreate);
        
        if (isset($_POST['save'])) {
            if ($insert_id = $this->save_cms()) {
                log_activity($this->auth->user_id(), lang('cms_act_create_record') . ': ' . $insert_id . ' : ' . $this->input->ip_address(), 'cms');
                Template::set_message(lang('cms_create_success'), 'success');

                redirect(SITE_AREA . '/settings/cms');
            }

            // Not validation error
            if ( ! empty($this->cms_model->error)) {
                Template::set_message(lang('cms_create_failure') . $this->cms_model->error, 'error');
            }
        }

        Template::set('toolbar_title', lang('cms_action_create'));

        Template::render();
    }
    /**
     * Allows editing of CMS data.
     *
     * @return void
     */
    public function edit()
    {
        $id = $this->uri->segment(5);
        if (empty($id)) {
            Template::set_message(lang('cms_invalid_id'), 'error');

            redirect(SITE_AREA . '/settings/cms');
        }
        
        if (isset($_POST['save'])) {
            $this->auth->restrict($this->permissionEdit);

            if ($this->save_cms('update', $id)) {
                log_activity($this->auth->user_id(), lang('cms_act_edit_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'cms');
                Template::set_message(lang('cms_edit_success'), 'success');
                redirect(SITE_AREA . '/settings/cms');
            }

            // Not validation error
            if ( ! empty($this->cms_model->error)) {
                Template::set_message(lang('cms_edit_failure') . $this->cms_model->error, 'error');
            }
        }
        
        elseif (isset($_POST['delete'])) {
            $this->auth->restrict($this->permissionDelete);

            if ($this->cms_model->delete($id)) {
                log_activity($this->auth->user_id(), lang('cms_act_delete_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'cms');
                Template::set_message(lang('cms_delete_success'), 'success');

                redirect(SITE_AREA . '/settings/cms');
            }

            Template::set_message(lang('cms_delete_failure') . $this->cms_model->error, 'error');
        }
        
        Template::set('cms', $this->cms_model->find($id));

        Template::set_view('create');
        Template::set('toolbar_title', lang('cms_edit_heading'));
        Template::render();
    }

    //--------------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------------

    /**
     * Save the data.
     *
     * @param string $type Either 'insert' or 'update'.
     * @param int    $id   The ID of the record to update, ignored on inserts.
     *
     * @return boolean|integer An ID for successful inserts, true for successful
     * updates, else false.
     */
    private function save_cms($type = 'insert', $id = 0)
    {
        if ($type == 'update') {
            $_POST['id'] = $id;
        }

        // Validate the data
        $this->form_validation->set_rules($this->cms_model->get_validation_rules());
        if ($this->form_validation->run() === false) {
            return false;
        }

        // Make sure we only pass in the fields we want
        if ($type == 'update') {
            $_POST['slug_url'] = $this->generate_slug($this->input->post('title'), $id);
        } else {
            $_POST['slug_url'] = $this->generate_slug($this->input->post('title'));
        }
        $data = $this->cms_model->prep_data($this->input->post());

        // Additional handling for default values should be added below,
        // or in the model's prep_data() method
        

        $return = false;
        if ($type == 'insert') {
            $id = $this->cms_model->insert($data);
            $this->cms_model->saveRoutes($this->cms_model->find_all());

            if (is_numeric($id)) {
                $return = $id;
            }
        } elseif ($type == 'update') {
            $return = $this->cms_model->update($id, $data);
        }

        return $return;
    }

    /**
     * Generate New slug by title
     * @param  string $title Page title
     * @return String generated Page slug
     */
    private function generate_slug($title,$id = 0) {

        // initializing empty array 
        $page_slug = '';

        // Configer Slug
        $config = array(
            'table' => 'bf_cms',
            'id' => 'id',
            'field' => 'slug_url',
            'replacement' => 'dash' // Either dash or underscore
        );

        // Load Slug Library
        $this->load->library('slug', $config);

        // Get slug string
        $slug_name = array('title' => $this->input->post('title'));

        // Create Slug
        $page_slug = ($id > 0 ) ? $this->slug->create_uri($slug_name , $id) : $this->slug->create_uri($slug_name);

        return $page_slug;
    }
}