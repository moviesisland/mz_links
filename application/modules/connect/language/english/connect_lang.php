<?php defined('BASEPATH') || exit('No direct script access allowed');


$lang['connect_manage']      = 'Manage connect';
$lang['connect_edit']        = 'Edit';
$lang['connect_true']        = 'True';
$lang['connect_false']       = 'False';
$lang['connect_create']      = 'Create';
$lang['connect_list']        = 'List';
$lang['connect_new']       = 'New';
$lang['connect_edit_text']     = 'Edit this to suit your needs';
$lang['connect_no_records']    = 'There are no connect in the system.';
$lang['connect_create_new']    = 'Create a new connect.';
$lang['connect_create_success']  = 'connect successfully created.';
$lang['connect_create_failure']  = 'There was a problem creating the connect: ';
$lang['connect_create_new_button'] = 'Create New connect';
$lang['connect_invalid_id']    = 'Invalid connect ID.';
$lang['connect_edit_success']    = 'connect successfully saved.';
$lang['connect_edit_failure']    = 'There was a problem saving the connect: ';
$lang['connect_delete_success']  = 'record(s) successfully deleted.';
$lang['connect_delete_failure']  = 'We could not delete the record: ';
$lang['connect_delete_error']    = 'You have not selected any records to delete.';
$lang['connect_actions']     = 'Actions';
$lang['connect_cancel']      = 'Cancel';
$lang['connect_delete_record']   = 'Delete this connect';
$lang['connect_delete_confirm']  = 'Are you sure you want to delete this connect?';
$lang['connect_edit_heading']    = 'Edit connect';

// Create/Edit Buttons
$lang['connect_action_edit']   = 'Save connect';
$lang['connect_action_create']   = 'Create connect';

// Activities
$lang['connect_act_create_record'] = 'Created record with ID';
$lang['connect_act_edit_record'] = 'Updated record with ID';
$lang['connect_act_delete_record'] = 'Deleted record with ID';

//Listing Specifics
$lang['connect_records_empty']    = 'No records found that match your selection.';
$lang['connect_errors_message']    = 'Please fix the following errors:';

// Column Headings
$lang['connect_column_created']  = 'Created';
$lang['connect_column_deleted']  = 'Deleted';
$lang['connect_column_modified'] = 'Modified';
$lang['connect_column_deleted_by'] = 'Deleted By';
$lang['connect_column_created_by'] = 'Created By';
$lang['connect_column_modified_by'] = 'Modified By';

// Module Details
$lang['connect_module_name'] = 'connect';
$lang['connect_module_description'] = 'Your module description';
$lang['connect_area_title'] = 'connect';

// Fields
$lang['connect_field_option_title'] = 'Option Title';
$lang['connect_field_email'] = 'Email';
$lang['connect_field_workflow_name'] = 'Workflow';
$lang['connect_field_workflow_id'] = 'Workflow ID';